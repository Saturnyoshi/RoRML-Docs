***********
Sound Class
***********

The ``Sound`` class is used to control and play pieces of audio.

For a full list of sounds added by the base game, see :doc:`this page </misc/vanillaSounds>`.

.. note:: Please keep in mind when using this class that audio playback is not synced to the game speed and will not do anything if the volume level is at 0%.
	As such, the ``isPlaying`` method should not be used to check the timing of events.

.. contents:: Table of Contents
   :local:

Static Methods
==============
	
``Sound.load([name], fname)``
-----------------------------

	Alias: ``Sound([name], fname)``

	Loads a sound from the path ``fname`` relative to the mod's directory. Only ``.wav`` and ``.ogg`` files are supported.

	If a name is not provided then the filename will be used.
	
	**Parameters:**
		
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      *Optional:* The name to give the sound within the current namespace.
		``fname``       string      The path to the file, relative to the mod's base path. The file extension is optional.
		===========     ======      ===========
	
	**Returns:**
		
		Always returns a new Sound object.
	
	**Examples:**
		::
			
			local smash = Sound.load("Smash", "sounds/glass.ogg")
		
		Loads the file ``glass.ogg`` from the mod's ``sounds`` folder and assign it to the variable ``smash``.

		::
			
			local bang = Sound.load("sounds/bang.ogg")
		
		This would load the file ``bang.ogg`` from the mod's ``sounds`` folder.
		As no name is provided, the sound will automatically be assigned the name ``"bang"``.

``Sound.find(name, [namespace])``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Sound.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Sound.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Sounds in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
		
``Sound.fromID(id)``
--------------------------------------------------------------
	
	Used to fetch a Sound object from its GameMaker resource ID.

	Useful for getting sounds from instance variables which contain a sound ID. 
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``id``          number      The GameMaker ID of the sound.
		===========     ======      ===========

	**Returns:**
	
		Returns the Sound if found, nil otherwise.
	
``Sound.getMusic()``
---------------------------------
	
	Returns the currently playing music as a ``Sound`` object.
		
	**Returns:**
	
		The currently playing music, or nil if no music is currently playing.
	
``Sound.setMusic(music)``
---------------------------------
	
	Sets the music currently playing music.
		
	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``music``       Sound or nil                       The sound object to use as the new music. If this is nil, music will be stopped.
		============    ===============================    ===========
	
Fields
======

===============    ========    ===========
Field              Type        Description
===============    ========    ===========
``id``             number      (Alias: ``ID``) The sound's GameMaker resource ID.
===============    ========    ===========

Methods
=======

.. _sound-play:

``sound:play([pitch], [volume])`` 
---------------------------------

	Plays the sound.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``pitch``       number      *Optional:* The pitch to play the sound at, defaults to 1. A higher pitch means a higher and faster sound, a lower pitch means a lower and slower sound.
		``volume``      number      *Optional:* The volume to play the sound at, defaults to 1.
		===========     ======      ===========
	
	**Examples:**
		::
			
			smash:play()
			
		This would play the ``smash`` sound at the default pitch and volume.

		::
			
			smash:play(0.8, 1.5)
			
		This would play the ``smash`` sound at 80% pitch and 150% volume.
		
.. _sound-isplaying:

``sound:isPlaying()`` 
---------------------

	Returns whether any instance of the sound is currently playing.
	
	**Returns:**
		
		Returns ``true`` if any instances of the sound are playing, otherwise returns ``false``.
	
	**Example:**
		::
			
			if smash:isPlaying() then 
				-- Some Code
			end
			
		This would check if the sound ``smash`` is currently playing, and if it is, execute the code.

.. _sound-loop:
		
``sound:loop()``
----------------
	
	Loops the sound, playing it repeatedly until the :ref:`stop <sound-stop>` method is called.
	
	**Example:**
		::
			
			smash:loop()
			
		This would play the ``smash`` sound on loop until ``smash:stop()`` is called.
	
.. _sound-stop:

``sound:stop()``
----------------
	
	Stops all playing instances of the sound.
	
	**Example:**
		::
			
			smash:stop()
			
		This would stop any currently playing instanecs of the ``smash`` sound.

``sound:setRemap(remap)``
-------------------------

	Used to effectively replace the sound when it is referenced by the base game.
	
	Note that this does not affect what sound plays when mods reference the sound.

	**Parameters:**
	
		===========     ============      ===========
		Parameter       Type              Description
		===========     ============      ===========
		``remap``       Sound or nil      The sound to be replaced with. If this is nil, the remap will be removed.
		===========     ============      ===========

``sound:getRemap(remap)``
-------------------------

	Used to check whether a sound has been remapped.

	**Returns:**

		Returns the remapped sound if applicable, otherwise returns nil.

.. _sound-getorigin:

``sound:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the sound.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``sound:getName()``
-------------------------------
	
	Returns the name of the Sound within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
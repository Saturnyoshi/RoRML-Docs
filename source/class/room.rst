**********
Room Class
**********

A ``Room`` represents a space where instances can be spawned. Rooms should not be confused with :doc:`Stages </class/stage>`.

Rooms are used for menus or specific stage variants.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Room.new(name)``
-------------------------------
	
	Alias: ``Room(name)``

	Creates a new room.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the room within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Room object.
	
	**Example:**
		::
			
			local coolRoom = Room.new("CoolRoom")
			
		Creates a new room called ``"CoolRoom"``.
	
``Room.getCurrentRoom()``
-------------------------------

	**Returns:**
	
		Returns the currently loaded Room.

``Room.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Room.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Room.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Rooms in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Methods
=======

``room:createInstance(object, x, y)``
-------------------------------------

	Creates an instance of the given object at the given coordinates, and returns the created instance's ID.
    
	Note that the instance isn't actually created until the room is loaded.
	
	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``object``      GMObject      The object of the instance to be created.
		``x``           number        The horizontal coordinate of the position to create the instance in.
		``y``           number        The vertical coordinate of the position to create the instance in.
		===========     ========      ===========
	
	**Example:**
		::
			
			local golem = Object.find("Golem")
			local golemInstID = coolRoom:createInstance(golem, 10, 10)
			
		Creates a ``Golem`` instance in the ``coolRoom`` room, and stores the instance's ID.
	
``room:resize(width, height)``
---------------------------------------------
	
	Resizes the room to match the given width and height.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``width``       number      The room's new width.
		``height``      number      The room's new height.
		===========     ======      ===========
	
	**Example:**
		::
			
			coolRoom:resize(420, 69)
			
		Resizes the ``coolRoom`` room to be 420x69.
	
``room:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the room.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``room:getName()``
-------------------------------
	
	Returns the name of the room.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
*******************
ActorInstance Class
*******************

The ``ActorInstance`` class is used to manipulate instances of characters in the game world.

ActorInstance inherits all functionality from :doc:`Instance </class/instance>`.

It provides no additional fields, but a variety of useful methods:

.. contents:: Table of Contents
   :local:

Methods (General)
=================

.. _actorinstance-bullet:

``actor:fireBullet(x, y, direction, distance, damage, [hitSprite], [properties])``
----------------------------------------------------------------------------------
	
	Creates a hitscan bullet.
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``x``              number      Horizontal origin of the bullet.
		``y``              number      Vertical origin of the bullet.
		``direction``      number      Direction the bullet "travels" in.
		``distance``       number      Max distance of the bullet.
		``damage``         number      Damage multiplier of the bullet. The final damage dealt will be the actor's damage multiplied by this number.
		``hitSprite``      Sprite      *Optional:* The sprite of the sparks spawned when the damager hits something, if left nil then sparks won't be created.
		``properties``     number      *Optional:* A combination of damager property constants, as seen below, added together.
		==============     ======      ===========
		
	**Damager Property Constants:**
		
		======================    ======
		Constant                  Effect
		======================    ======
		DAMAGER_NO_PROC           The bullet won’t proc on-hit item effects when this flag is set; Such as Sticky Bomb or Ifrit’s Horn.
		DAMAGER_NO_RECALC         When this flag is set, the bullet‘s displayed damage will not be randomized and the bullet cannot be a critical hit.
		DAMAGER_BULLET_PIERCE     Allows the bullet to pierce through any number of enemies, like the Commando’s FMJ.
		======================    ======
		
	**Returns:**
	
		Returns the created :doc:`DamagerInstance </class/instance>`.
	
	**Example:**
		::
			
			player:fireBullet(player.x, player.y, player:getFacingDirection(), 100, 2, nil, DAMAGER_BULLET_PIERCE + DAMAGER_NO_PROC)
			
		Fires a bullet originating from instance ``player`` with 100 range, dealing 200% damage. It has no hit sprite, pierces, and doesn't proc on-hit effects.

.. _actorinstance-explosion:

``actor:fireExplosion(x, y, width, height, damage, [explosionSprite], [hitSprite], [properties])``
--------------------------------------------------------------------------------------------------
	
	Creates an explosion.
	
	**Parameters:**
	
		===================     ======      ===========
		Parameter               Type        Description
		===================     ======      ===========
		``x``                   number      Horizontal origin of the explosion.
		``y``                   number      Vertical origin of the explosion.
		``width``               number      The width of the explosion, as a multiple of 19.
		``height``              number      The height of the explosion, as a multiple of 4.
		``damage``              number      Damage multiplier of the explosion. The final damage dealt will be the actor's damage multiplied by this number.
		``explosionSprite``     Sprite      *Optional:* The animation used by the explosion itself, if left nil then the explosion won't be visible
		``hitSprite``           Sprite      *Optional:* The sprite of the sparks spawned when the damager hits something, if left nil then sparks won't be created.
		``properties``          number      *Optional:* A combination of damager property constants, as seen below, added together.
		===================     ======      ===========
		
	**Damager Property Constants:**
		
		======================    ======
		Constant                  Effect
		======================    ======
		DAMAGER_NO_PROC           The explosion won't proc on-hit item effects when this flag is set; Such as Sticky Bomb or Ifrit's Horn.
		DAMAGER_NO_RECALCULATE    When this flag is set, the explosion's displayed damage will not be randomized and the bullet cannot be a critical hit.
		======================    ======
		
	**Returns:**
	
		Returns the created :doc:`DamagerInstance </class/instance>`.
	
	**Example:**
		::
			
			inst:fireExplosion(inst.x, inst.y, 100/19, 100/4, 1.5, detonationSprite, nil, BULLET_NO_RECALCULATE)
			
		Fires an explosion on the instance ``inst``, with a size of 100x100, dealing 150% damage, using ``detonationSprite`` as the explosion sprite but no hit sprite.

``actor:getFacingDirection()``
-------------------------------
	
	Gets the direction the instance is facing.
	
	Usually used alongside the ``fireBullet`` method.
	
	**Returns:**
	
		Returns a number; 0 if facing right, 180 if facing left.
		
``actor:kill()``
-------------------------------
	
	Kills the actor.
	
	**Example:**
		::
			
			lemurian:kill()
			
		Kills the instance ``lemurian``.
		
``actor:isClassic()``
-------------------------------
	
	**Returns:**
	
		Whether the actor is a "classic" type actor.
		
		Classic actors are those which can walk across the map and be knocked down ledges, such as Lemurians, Golems, Players, and Imp Overlords.
		
``actor:isBoss()``
-------------------------------
	
	**Returns:**
	
		Whether the actor is a boss.
		
Methods (Buffs)
===============

``actor:applyBuff(buff, duration)``
-----------------------------------
	
	Applies a :doc:`Buff </class/buff>` to the instance.
	
	Applying a buff already active on the instance will extend the timer.
	
	**Parameters:**
	
		============     ======      ===========
		Parameter        Type        Description
		============     ======      ===========
		``buff``         Buff        The buff to apply.
		``duration``     number      The duration of the buff in frames.
		============     ======      ===========
	
	**Example:**
		::
			
			player:applyBuff(someBuff, 60 * 3)
			
		Applies the buff ``someBuff`` to the instance ``player`` for 3 seconds.
		
``actor:removeBuff(buff)``
-------------------------------
	
	Removes a :doc:`Buff </class/buff>` from the instance.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``buff``        Buff        The type of buff to remove.
		===========     ======      ===========
	
	**Example:**
		::
			
			player:removeBuff(someBuff)
			
		Removes the buff ``someBuff`` from the instance ``player``.
		
``actor:hasBuff(buff)``
-------------------------------
	
	Checks whether an instance is affected by a :doc:`Buff </class/buff>`.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``buff``        Buff        The type of buff to check for.
		===========     ======      ===========

	**Returns:**
	
		Returns true if the instance is affected by the buff, false if not.
	
	**Example:**
		::
			
			if actor:hasBuff(someBuff) then
			    -- Do something
			end
			
		Does something if the instance ``actor`` has the buff ``someBuff``.

``actor:getBuffs()``
-------------------------------
	
	Gets all the :doc:`Buffs </class/buff>` affecting the instance.

	**Returns:**
	
		Returns a list of Buff objects.
	
	**Example:**
		::
			
			for _, buff in ipairs(player:getBuffs()) do
			    -- Do something
			end
			
		Loops over the buffs a player has and does something for each.
		
Methods (Sprites)
=================

.. _spritekeys:

Common Sprite Keys
------------------

	====================    ===========
	Key                     Description
	====================    ===========
	idle                    The sprite displayed when the actor is idle
	walk                    The sprite displayed when the actor is walking.
	jump                    The sprite displayed when the actor is jumping.
	climb                   Only applies to players. Sprite used when climbing ropes.
	death                   The animation played when the actor dies. The first frame is also used when the actor takes knockback.
	shoot[1...4]            Most vanilla skill use sprites. (Example: ``shoot1`` or ``shoot2``)
	shoot[1...4]_[1...3]    Used by vanilla skills that have multiple sprites. (Example: ``shoot1_2``)
	====================    ===========

``actor:getAnimation(key)``
-------------------------------
	
	Gets one of the actor's animations.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``key``         string      The key to check for. See :ref:`above <spritekeys>` for a list of examples.
		===========     ======      ===========

	**Returns:**
	
		Returns a :doc:`Sprite </class/sprite>` object.
	
	**Example:**
		::
			
			local walk = lemurian:getAnimation("walk")
			
		Gets the walk sprite of instance ``lemurian``.
		
``actor:setAnimation(key, sprite)``
----------------------------------------
	
	Sets one of the actor's animations.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``key``         string      The key to set the sprite for. See :ref:`above <spritekeys>` for a list of examples.
		``sprite``      Sprite      The sprite to set the key to.
		===========     ======      ===========
	
	**Example:**
		::
			
			lemurian:setAnimation("walk", customWalk)
			
		Sets the walk sprite of instance ``lemurian`` to the sprite stored in ``customWalk``.
		
``actor:setAnimations{...}``
-------------------------------
	
	Sets multiple of the actor's animations at the same time.
	
	**Parameters:**
	
		A table with string keys and sprite values.
	
	**Example:**
		::
			
			lemurian:setAnimations{walk = customWalk, idle = customIdle}
			
		Sets both the walk and idle animations of the instance ``lemurian`` to those stored in the variables ``customWalk`` and ``customIdle``.

Methods (Elites)
================
  
``actor:makeElite([type])``
-----------------------------------
	
	Applies an :doc:`EliteType </class/eliteType>` to the instance.

	If the enemy is already elite or blighted then this method won't do anything.

	For the elite type to be applied, it must be available in the actor's spawn elites.
	
	**Parameters:**
	
		============     =========      ===========
		Parameter        Type           Description
		============     =========      ===========
		``type``         EliteType      *Optional:* The elite type to apply to the enemy. If none is provided, the enemy will be assigned a random type from its available types.
		============     =========      ===========

	**Returns:**

		Returns true if the actor was changed, otherwise false.

	**Examples:**
		::
			
			golem:makeElite()

		Makes the ``golem`` a random elite from its list of available elite types.

		::
			
			local overloading = EliteType.find("Overloading", "Vanilla")
			golem:makeElite(overloading)

		Makes the ``golem`` an overloading elite, provided it's allowed to be one normally.
  
``actor:getElite()``
-----------------------------------
	
	**Returns:**

		Returns the enemy's :doc:`EliteType </class/eliteType>` if one is applied, otherwise returns nil.
  
``actor:makeBlighted([type])``
-----------------------------------
	
	Transforms an enemy into a blighted version.

	If the enemy is already elite or blighted then this method won't do anything.

	For the enemy to become blighted, it must be able to spawn blighted under normal circumstances.
	
	| The blighted enemy's type is represented by a single number generated by multiplying primes together to define its traits.
	| Each prime and associated effect is as follows:

	=====     =========
	Prime     Effect
	=====     =========
	3         (Blazing) The enemy gains a fire trail.
	5         (Frenzied) The enemy gains 30% attack speed and 25% movement speed.
	7         (Leeching) The enemy gains 50 lifesteal.
	11        (Overloading) The enemy gains on-hit lightning.
	13        (Volatile) The enemy gains explosive attacks.
	=====     =========

	**Parameters:**
	
		============     =========      ===========
		Parameter        Type           Description
		============     =========      ===========
		``type``         number         *Optional:* The blight type as explained above. If none is provided, a random type will be generated from 2 of the primes.
		============     =========      ===========

	**Returns:**

		Returns true if the actor was changed, otherwise false.

	**Examples:**
		::
			
			wisp:makeBlighted()

		Makes the ``wisp`` a random blighted enemy type.

		::
			
			golem:makeElite(3 * 5 * 7 * 11 * 13)

		Makes the ``wisp`` a blighted enemy with all effects. Normally blighted enemies only spawn with 2, but there's no limit to how many can be provided.

``actor:getBlighted()``
-----------------------------------
	
	**Returns:**

		Returns the blighted enemy's type if one is applied, otherwise returns nil.

PlayerInstance
==============
		
``PlayerInstance`` is a subclass of ActorInstance and is used to represent players in the game world.

It also has a page on its own; see :doc:`PlayerInstance Class Reference </class/playerInstance>`.
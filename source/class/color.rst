***********
Color Class
***********

Alias: ``Colour``

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Color.fromHex(value)``
-----------------------------------
	
	Alias: ``Color(value)``

	Produces a new Color object taking a single number as input.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       number      The color as a single RRGGBB formated number.
		===========     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local blurple = Color.fromHex(0x7289DA)
			
		Creates a new color, assigning it to the variable ``blurple``.

``Color.fromRGB(red, green, blue)``
-----------------------------------
	
	Alias: ``Color(red, green, blue)``

	Produces a new Color object taking RGB values as input.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``red``         number      The red value of the color, ranging from 0 to 255.
		``green``       number      The green value of the color, ranging from 0 to 255.
		``blue``        number      The blue value of the color, ranging from 0 to 255.
		===========     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local blurple = Color.fromRGB(114, 137, 218)
			
		Creates a new color, assigning it to the variable ``blurple``.
		
``Color.fromHSV(hue, saturation, value)``
-----------------------------------------
	
	Produces a new Color object taking HSV values as input.
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``hue``            number      The hue of the color, ranging from 0 to 255.
		``saturation``     number      The saturation of the color, ranging from 0 to 255.
		``value``          number      The value of the color, ranging from 0 to 255.
		==============     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local blurple = Color.fromHSV(226, 120, 217)
			
		Creates a new color, assigning it to the variable ``blurple``.
		
``Color.fromGML(value)``
-----------------------------------------
	
	Produces a new Color object taking a GameMaker colour as input.

	You'll typically expect to find these values stored in instance variables used to represent colours.
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``value``          number      The color in GameMaker format.
		==============     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local blend = Color.fromGML(inst:get("image_blend"))
			
		Gets the blend color of the instance ``inst``, this is identical to accessing the instance's ``blendColor`` field.
		
``Color.mix(color1, color2, amount)``
-----------------------------------------
	
	Produces a new Color object by mixing together 2 existing colors a specified amount.
	
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``color1``         Color       The base color.
		``color2``         Color       The color to mix with.
		``amount``         number      The amount to mix as a decimal value from 0 to 1.
		==============     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Examples:**
		::
			
			local yellorange = Color.mix(Color.YELLOW, Color.ORANGE, 0.5)
			
		Creates a new color which is 50% yellow and 50% orange.

		::
		
			local bluish_green = Color.mix(Color.GREEN, Color.BLUE, 0.25)
			
		Creates a new color which is 75% green and 25% blue.

		
``Color.darken(color, amount)``
-----------------------------------------
	
	Produces a darkened version of the provided Color object.
	
	Works the same as merging the provided color with ``Color.BLACK``.
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``color``          Color       The base color.
		``amount``         number      The amount to darken as a decimal value from 0 to 1.
		==============     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local dark_yellow = Color.darken(Color.YELLOW, 0.25)
			
		Produces a yellow color 25% darker than the original.
			
``Color.lighten(color, amount)``
-----------------------------------------
	
	Produces a lightened version of the provided Color object.
	
	Works the same as merging the provided color with ``Color.WHITE``.
	
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``color``          Color       The base color.
		``amount``         number      The amount to darken as a decimal value from 0 to 1.
		==============     ======      ===========

	**Returns:**
	
		Returns a new Color object.
	
	**Example:**
		::
			
			local light_yellow = Color.lighten(Color.YELLOW, 0.75)
			
		Produces a yellow color 75% lighter than the original.

``Color.equals(color1, color2)``
-----------------------------------------
	
	Compares 2 Color objects to see if they contain is the same color.
		
	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``color1``         Color       The first color to compare.
		``color2``         Color       The second color to compare.
		==============     ======      ===========

	**Returns:**
	
		Returns a boolean indicating whether the objects match.
	
Constants
=========

Colors starting with ``ROR_`` are commonly used for UI or instance blending.

Colors starting with ``DAMAGE_`` are commonly used as damage indicator colors.

	========================    === === ===
	Name                        R   G   B
	========================    === === ===
	``Color.WHITE``             255 255 255
	``Color.LIGHT_GRAY``        192 192 192
	``Color.LIGHT_GREY``        192 192 192
	``Color.GRAY``              128 128 128
	``Color.GREY``              128 128 128
	``Color.DARK_GRAY``         64  64  64
	``Color.DARK_GREY``         64  64  64
	``Color.BLACK``             0   0   0
	``Color.RED``               255 0   0
	``Color.GREEN``             0   255 0
	``Color.BLUE``              0   0   255
	``Color.LIGHT_RED``         255 128 128
	``Color.LIGHT_GREEN``       128 255 128
	``Color.LIGHT_BLUE``        128 128 255
	``Color.DARK_RED``          128 0   0
	``Color.DARK_GREEN``        0   128 0
	``Color.DARK_BLUE``         0   0   128
	``Color.AQUA``              128 255 255
	``Color.FUCHSIA``           255 0   255
	``Color.YELLOW``            255 255 0
	``Color.ORANGE``            255 128 0
	``Color.LIME``              128 255 0
	``Color.PURPLE``            128 0   255
	``Color.PINK``              255 0   128
	``Color.CORAL``             255 128 128
	``Color.ROR_RED``           207 102 102
	``Color.ROR_GREEN``         126 182 134
	``Color.ROR_BLUE``          124 136 184
	``Color.ROR_YELLOW``        239 210 123
	``Color.ROR_ORANGE``        243 165 86
	``Color.DAMAGE_HEAL``       132 215 104
	``Color.DAMAGE_POISON``     201 242 77
	``Color.DAMAGE_ENEMY``      124 97  169
	``Color.DAMAGE_NEUTRAL``    124 136 184
	========================    === === ===
	
Fields
======
	
.. note:: All fields are read only and all fields other than ``hex`` and ``gml`` are from 0 to 255.

==============    ======    ===========
Field             Type      Description
==============    ======    ===========
``red``           number    (Alias: ``r``, ``R``) The red component of the color in RGB.
``green``         number    (Alias: ``g``, ``G``) The green component of the color in RGB.
``blue``          number    (Alias: ``b``, ``B``) The blue component of the color in RGB.
``hue``           number    (Alias: ``h``, ``H``) The hue component of the color in HSV.
``saturation``    number    (Alias: ``s``, ``S``) The saturation component of the color in HSV.
``value``         number    (Alias: ``v``, ``V``) The value component of the color in HSV.
``hex``           number    The color as a single RRGGBB hex value.
``gml``           number    The color as a GameMaker (BBGGRR) color value.
==============    ======    ===========

*******************
DynamicSprite Class
*******************

The ``DynamicSprite`` class is used to create :doc:`Sprites </class/sprite>` at runtime.

DynamicSprite inherits all functionality from :doc:`BaseSprite </class/baseSprite>`.

A ``DynamicSprite`` is created using the :ref:`Surface:createSprite <surface-createsprite>` method.

| Much like a :doc:`Surface </class/surface>`, a ``DynamicSprite`` can be created at any time and drawn to the screen.
| Unlike a :doc:`Surface </class/surface>`, a ``DynamicSprite`` will never stop being valid unless it is explicitly deleted, but does require a :doc:`Surface </class/surface>` to be created from.
| This class can also be used to generate regular :doc:`Sprites </class/sprite>` which can be used to dynamically generate items or other content.

.. contents:: Table of Contents
   :local:

Methods
=======

``sprite:delete()``
-----------------------------------
	
	| Deletes the sprite, causing it to become invalid.
	| All future attempts to access the sprite will result in an error.

``sprite:finalise(name)``
-------------------------------
	
	Alias: ``DynamicSprite:finalize(name)``

	| Used to retrieve a final :doc:`Sprite </class/sprite>`.
	| Also causes the ``DynamicSprite`` to become invalid.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the sprite within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Returns a new :doc:`Sprite </class/sprite>` with the same contents.

``sprite:addFrame(source, x, y, w, h)``
----------------------------------------

	| Adds a new frame to the sprite, using the provided :doc:`Surface </class/surface>`.
	| The frame is appended at the end of the current animation sequence.

	**Parameters:**
	
		===========     =======      ===========
		Parameter       Type         Description
		===========     =======      ===========
		``source``      Surface      The :doc:`Surface </class/surface>` to pull the image from.
		``x``           number       *Optional:* The left x coordinate of the region of the surface to copy. Defaults to 0.
		``y``           number       *Optional:* The top y coordinate of the region of the surface to copy. Default to 0.
		``w``           number       *Optional:* The width of the region of the surface to copy. Defaults to the width of the sprite.
		``h``           number       *Optional:* The height of the region of the surface to copy. Defaults to the height of the sprite.
		===========     =======      ===========

``sprite:isValid()``
-------------------------------
	
	| Checks whether the sprite still exists.
	| This will only return false after the sprite's ``delete`` method has been called.

	**Returns:**
	
		Returns true if sprite exists, false if it doesn't.

************
Camera Class
************

The ``camera`` class lets you interact with GameMaker's camera.

To update the camera's position, use the ``onCameraUpdate`` :doc:`callback </global/registerCallback>`.

.. contents:: Table of Contents
   :local:

Fields
======

=================    ==================    ===========
Field                Type                  Description
=================    ==================    ===========
``width``            number                The camera view's width. Must be between ``10`` and ``10000``.
``height``           number                The camera view's height. Must be between ``10`` and ``10000``.
``x``                number                The camera's ``x`` position. *This can only be changed inside the ``onCameraUpdate`` callback.*
``y``                number                The camera's ``x`` position. *This can only be changed inside the ``onCameraUpdate`` callback.*
``angle``            number                The camera's angle, in degrees.
=================    ==================    ===========

**********
Item Class
**********

The ``Item`` class is used to create and modify items which can be found in the game.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Item.new(name)``
-------------------------------
	
	Alias: ``Item(name)``

	Creates a new item.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the item within the current namespace..
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Item object.
	
	**Example:**
		::
			
			local peach = Item.new("Peach")
			
		Creates a new item called ``"Peach"``.
	
``Item.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Item.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Item.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Items in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

``Item.fromObject(object)``
---------------------------

	Searches for an item which uses the specified doc:`GMObject </class/gmObject>`.
	
	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``object``      GMObject      The object to use to find the item.
		===========     ========      ===========

	**Returns:**
	
		Returns the item if found.
	
Fields
======

===============    ===============    ===========
Field              Type               Description
===============    ===============    ===========
``displayName``    string             The name of the item as displayed in-game.
``pickupText``     string             The text displayed under the name of the item when it's picked up. Usually a short descriptor of the item's functionality.
``sprite``         Sprite             The sprite of the item. Use items expect a 2 frame sprite, the first one with the ``USE`` label on it. Other item sprites will only display the first frame.
``color``          string or Color    | (Alias: ``colour``) The color of the item's tier in :doc:`colored text formatting </misc/coloredTextFormatting>`.
                                      | For custom item tiers, using a :doc:`Color </class/color>` object is also supported.
                                      | Usually ``"w"``, ``"g"``, ``"r"``, ``"or"``, and ``"p"`` for common, uncommon, rare, use, and special items respectively.
``isUseItem``      boolean            Whether the item is a use item or not.
``useCooldown``    number             *Only applies to use items.* This is the cooldown of the item, in seconds. Default is 45, as used by most use items in the base game.
===============    ===============    ===========

Methods
=======

``item:setTier(tier)``
------------------------------------
	
	Sets the ``color`` of the item and adds it to the appropriate :doc:`ItemPool </class/itemPool>`.
	If the item is already in the common, uncommon, rare, or use pool then it will be removed.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``tier``         string        The string ``"common"``, ``"uncommon"``, ``"rare"``, or ``"use"``.
		============     ========      ===========
	
	**Example:**
		::
			
			customItem:setTier("common")
			
		::
			
			ItemPool.find("common"):add(customItem)
			customItem.color = "w"
			
		Both of the above code examples achieve the same effect.
		However, the example utilizing ``setTier`` is much more concise.

``item:addCallback(callback, bind)``
------------------------------------
	
	Binds a function to be called when a specific callback is fired.
	
	The :doc:`PlayerInstance </class/playerInstance>` which triggered the callback is always passed to the function.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``callback``     string        The name of the callback to bind a function to, see below for a list of callback names.
		``bind``         function      The function to be run when the callback is fired. The function is always passed the PlayerInstance which triggered the callback.
		============     ========      ===========
		
	**Callbacks:**
		
		==========    ===========
		Name          Description
		==========    ===========
		``pickup``    Fired when a player picks up the item.
		``use``       *Use items only.* Fired when the item is used.
		``drop``      *Use items only.* Fired when the item is dropped as a result of being switched out for another.
		==========    ===========
	
	**Example:**
		::
			
			local function itemPickup(player)
			    player.y = player.y - 100
			end
			customItem:addCallback("pickup", itemPickup)
			
		::
			
			customItem:addCallback("pickup", function(player)
			    player.y = player.y - 100
			end)
			
		Two different ways of achieving the same effect; an item that moves the player 100 pixels upward when picked up.
		
``item:setLog{...}``
-------------------------------
	
	Creates an entry for the item in the Item Log, alternatively modifies the existing one.
	
	**Parameters:**
		
		This method can be provided with any number of these parameters at a time to set or edit specific parts of the item log. See below for a usage example.
	
		===============     ======      ===========
		Parameter           Type        Description
		===============     ======      ===========
		``group``           string      Decides where in the item log your items will appear. Mod items will always appear after base game items of the same rarity. A list of available group name can be found below.
		``description``     string      A description of the items functionality, usually more detailed than the in-game description. Supports :doc:`colored text formatting </misc/coloredTextFormatting>`.
		``priority``        string      A lore-friendly description of rarity, see the in-game descriptions of items for examples. Not setting this will set it automatically to a default based on what group the Log is set to. Supports :doc:`colored text formatting </misc/coloredTextFormatting>`.
		``destination``     string      Shipping destination for the item.
		``date``            string      Estimated delivery date for the item.
		``story``           string      A flavorful story or description relating to the item.
		===============     ======      ===========
	
	**Groups:**
		
		============    =====
		``start``        
		``common``      ``common_locked``
		``uncommon``    ``uncommon_locked``
		``rare``        ``rare_locked``
		``use``         ``use_locked``
		``boss``        ``boss_locked``
		``end``
		============    =====
	
	**Example:**
		::
			
			customItem:setLog{
			    group = "uncommon",
			    description = "This is what the item does",
			    destination = "City,\nCountry,\nPlanet",
			    date = "mm/DD/YYYY",
			    story = "This is some flavortext for the item."
			}
			
		| Creates a fully formed item log for the custom item.
		| Priority has been omitted so that it's set to the default for uncommon items.
		
		::
			
			Item.find("infusion", "vanilla"):setLog{
			    description = "Killing an enemy increases your &r&health permanently by 0.25.&!&"
			}
			
		In this example someone has made a mod that nerfs Infusion, and they want to edit the item log description to match the changes.
	
``item:getObject()``
--------------------

	Returns the :doc:`GMObject </class/gmObject>` used to represent the item in the game world.

	**Returns:**
	
		Always returns a :doc:`GMObject </class/gmObject>`.
	
	**Example:**
		::
			
			exampleItem:getObject():create(xpos, ypos)
			
		Creates an instance of the item ``exampleItem`` at a specific position.

``item:create(x, y)``
---------------------

	Creates a new instance of the item. Can be used as a shortcut to ``item:getObject():create(x, y)``.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal coordinate to create an instance of the item at.
		``y``           number      The vertical coordinate to create an instance of the item at.
		===========     ======      ===========

	**Returns:**
	
		Returns the newly created instance.
	
	**Example:**
		::
			
			exampleItem:getObject():create(xpos, ypos)
			
		Creates an instance of the item ``exampleItem`` at a specific position.
	
	
``item:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the item.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``item:getName()``
-------------------------------
	
	Returns the name of the item.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
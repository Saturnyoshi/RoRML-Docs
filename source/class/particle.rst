******************
ParticleType Class
******************

The ``ParticleType`` class is used to define the behavior of particles.
This class is primarily an interface for `GML's own particle types <https://docs.yoyogames.com/source/dadiospice/002_reference/particles/particle%20types/index.html>`_.


A ParticleType object has no accessible fields, instead all of its properties must be altered through methods.

For a full list of particles added by the base game, see :doc:`this page </misc/vanillaParticles>`.

.. contents:: Table of Contents
   :local:

Static Methods
============== 
  
``ParticleType.new([name])``
-------------------------------
	
	Alias: ``ParticleType([name])``
	
	Creates a new ParticleType.

	If a name is not provided then an automatically generated name will be used.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      *Optional:* The name to give the particle type within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new ParticleType object.
	
	**Example:**
		::
			
			local eggplant = Particles.new("Eggplant")
			
		Creates a new particle type called ``"Eggplant"``.
   
``ParticleType.find(name, [namespace])``
-----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing :doc:`ParticleType </class/particle>`.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``ParticleType.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing ParticleTypes in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

Methods (Appearance)
========================

``particletype:sprite(sprite, animate, stretch, random)``
---------------------------------------------------------
	
	Sets the sprite and animation settings of the particle type.
	
	Replaces any ``shape`` that the particle might already have. (see below)
	
	**Parameters:**
	
		===========     =======      ===========
		Parameter       Type         Description
		===========     =======      ===========
		``sprite``      Sprite       The sprite that the particle will have.
		``animate``     boolean      Whether the sprite should animate or not.
		``stretch``     boolean      Whether to stretch the sprite's animation to match the particle's lifespan.
		``random``      boolean      Whether to choose a random sub-image.
		===========     =======      ===========

	**Example:**
		::
			
			eggplant:sprite(eggplantSprite, true, true, false)
			
		This sets the sprite of the particle ``eggplant`` to the sprite ``eggplantSprite``, with its animation stretched to match the life of the particle.
		
``particletype:shape(shape)``
-------------------------------
	
	Sets the shape of the particle type from a predetermined set of shapes.
	
	Replaces any ``sprite`` that the particle might already have. (see above)
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``shape``       string      The name of the shape to set the particle as.
		===========     ======      ===========
		
		The shape can be any of the following 14 built in types (not case sensitive):
		
		.. image:: /images/particletypes.png   
		
		*(Image taken from the GML documentation page for particles.)*

	**Example:**
		::
			
			circleParticle:shape("circle")
			
		Sets the shape of ``circleParticle`` to ``"circle"``.
		
``particletype:color(color, [color2], [color3])``
-------------------------------------------------
	
	Sets the :doc:`color </class/color>` of the particle type. Using the optional arguments you can make particles fade between different colors during their lifespan.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``color``       Color       The :doc:`color </class/color>` that the particle will have.
		``color2``      Color       *Optional:* If set, the particle's color will fade to this color over its lifespan.
		``color3``      Color       *Optional:* If set, the particle's color will fade first to ``color2`` during the first half of its lifespan and then fade from ``color2`` to this color during the second half.
		===========     ======      ===========

	**Example:**
		::
			
			stoplight:color(Color.RED, Color.YELLOW, Color.GREEN)
			
		Makes the particle ``stoplight`` fade from red to yellow to green over the course of its lifespan.
		
``particletype:alpha(alpha, [alpha1], [alpha2])``
-------------------------------------------------
	
	Sets the transparency of the particle type. Using the optional arguments you can make particles fade between different transparency levels during their lifespan.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``alpha``       number      The alpha value that the particle will have, 0 is fully transparent and 1 is fully opaque.
		``alpha2``      number      *Optional:* If set, the particle's transparency will fade to this transparency over its lifespan.
		``alpha3``      number      *Optional:* If set, the particle's transparency will fade first to ``alpha2`` during the first half of its lifespan and then fade from ``alpha2`` to this transparency during the second half.
		===========     ======      ===========

	**Example:**
		::
			
			pulseLight:alpha(0, 1, 0)
			
		Makes the particle ``pulseLight`` fade into full opacity and then out again over the course of its lifespan.

``particletype:additive(additive)``
-----------------------------------
	
	Sets whether particles of the type should use additive blend mode or not.
	
	This is useful for example in fire effects or things that have a glow to them. This is set to false by default.
	
	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``additive``     boolean      Whether the particle type should use the additive blend mode.
		============     =======      ===========

	**Example:**
		::
			
			fairydust:additive(true)
			
		Makes the particle ``fairydust`` be drawn additively.
		
``particletype:scale(xscale, yscale)``
--------------------------------------
	
	Scales the sprite or shape of the particle type before any other size changes are applied.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``xscale``      number      The horizontal scaling of the image. Multiplicative, meaning a scale of 0.5 is 50% and a scale of 2 is 200%.
		``yscale``      number      The vertical scaling of the image. Multiplicative, meaning a scale of 0.5 is 50% and a scale of 2 is 200%.
		===========     ======      ===========

	**Example:**
		::
			
			eggplant:scale(1.4, 0.7)
			
		Makes the particle type ``eggplant`` wider and shorter.
		
``particletype:size(min, max, add, wiggle)``
--------------------------------------------
	
	Determines the size range of the particle type.
	
	| Size is multiplicative, meaning a size of 0.5 is half and a size of 2 is twice as big.
	| When spawned, a particle's size will be selected randomly between the minimum and the maximum.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``min``         number      The minimum size a particle can start at.
		``max``         number      The maximum size a particle can start at.
		``add``         number      How much a particle should grow or shrink per step, negative values indicate shrinking.
		``wiggle``      number      Randomly added or subtracted from a particle's size each step.
		===========     ======      ===========

	**Example:**
		::
			
			eggplant:size(0.8, 1.2, 0.02, 0.01)
			
		Makes the particle ``eggplant`` start at a random scale between 0.8 and 1.2 times the base size, and grow by 1 to 3 percent each step.
		
``particletype:angle(min, max, add, wiggle, relative)``
-------------------------------------------------------
	
	Determines the rotation of a particle's sprite or shape.
	
	| An angle of 0 is straight right, an angle of 90 is straight up, and so on.
	| When spawned, a particle's angle will be selected randomly between the minimum and the maximum.

	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``min``          number       The minimum angle a particle can start at, in degrees.
		``max``          number       The maximum angle a particle can start at, in degrees.
		``add``          number       How much a particle should rotate each step, in degrees.
		``wiggle``       number       Randomly added or subtracted from a particle's angle each step.
		``relative``     boolean      Whether the particle should rotate in the direction it's moving.
		============     =======      ===========
	
	**Example:**
		::
			
			eggplant:angle(0, 360, 6, 0, true)
			
		Makes the particle ``eggplant`` start rotated at a random angle and turn continuously counterclockwise.

Methods (Behavior)
==================
		
``particletype:speed(min, max, add, wiggle)``
---------------------------------------------
	
	Determines the speed at which a particle will move.
	
	When spawned, a particle's speed will be selected randomly between the minimum and the maximum.

	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``min``          number       The minimum speed the particle can start at.
		``max``          number       The maximum speed the particle can start at.
		``add``          number       How much the particle should accelerate or decelerate each step. Can be both negative and positive.
		``wiggle``       number       Randomly added or subtracted from the particle's speed each step.
		============     =======      ===========
	
	**Example:**
		::
			
			boomerang:speed(2, 2, -0.02, 0)
			
		Makes the particle ``boomerang`` spawn with a speed of 2, which will slowly decrease over its lifespan.
		
``particletype:direction(min, max, add, wiggle)``
-------------------------------------------------
	
	Determines the direction a particle will move.
	
	| An angle of 0 is straight right, an angle of 90 is straight up, and so on.
	| When spawned, a particle's direction will be selected randomly between the minimum and the maximum.

	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``min``          number       The minimum angle the particle can start out heading towards, in degrees.
		``max``          number       The maximum angle the particle can start out heading towards, in degrees.
		``add``          number       How much the particle should turn each step, in degrees.
		``wiggle``       number       Randomly added or subtracted from the particle's direction each step.
		============     =======      ===========

	**Example:**
		::
			
			firework:direction(85, 95, 0, 1)
			
		Makes the particle ``firework`` spawn pointing up, with 10 degrees of variance, and rotate slightly as it travels.
		
``particletype:gravity(amount, direction)``
-------------------------------------------
	
	Determines the direction and speed of gravity on a particle.
	
	**Parameters:**
	
		=============     =======      ===========
		Parameter         Type         Description
		=============     =======      ===========
		``amount``        number       How much the gravity will affect a particle, in pixels per step squared.
		``direction``     number       The direction the gravity will pull the particle towards.
		=============     =======      ===========

	**Example:**
		::
			
			rock:gravity(0.5, 270)
			
		Gives the particle ``rock`` a downwards gravity of 0.5 pixels per frame.
		
``particletype:life(min, max)``
-------------------------------
	
	Determines the lifespan of the particle.
	
	When spawned, a particle's lifespan will be selected randomly between the minimum and the maximum.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``min``         number      The minimum lifespan of a particle, in steps.
		``max``         number      The maximum lifespan of a particle, in steps.
		===========     ======      ===========
	
	**Example:**
		::
			
			fog:life(60 * 5, 60 * 7)
			
		Gives each particle ``fog`` a random lifespan between 5 and 7 seconds.

``particletype:createOnStep(child, amount)``
--------------------------------------------
	
	Used to make all particles of the type create another particle type each step.

	.. note::
		Be very careful when using this as it can greatly increase the number of particles on-screen at a time and cause the game to slow down noticeably.
	
	**Parameters:**
	
		===========     ============      ===========
		Parameter       Type              Description
		===========     ============      ===========
		``child``       ParticleType      The child particle to spawn. (can not be the same particle type)
		``amount``      number            The number of particles to spawn. If this number is a negative, it will be used as a chance to spawn another particle instead.
		===========     ============      ===========
	
	**Examples:**
		::
			
			rocket:createOnStep(smoke, 2)
			
		Makes the particle ``rocket`` spawn two of the particle ``smoke`` each step.

		::
			
			firework:createOnStep(smoke, -3)
			
		Gives the particle ``firework`` a chance to spawn the particle ``smoke`` an average of once every 3 steps.

``particletype:createOnDeath(child, amount)``
---------------------------------------------
	
	Determines what child particles a particle of this type should spawn at the end of its lifespan.
	
	.. note::
		Be very careful when using this as it can greatly increase the number of particles on-screen at a time and cause the game to slow down noticeably.
	
	**Parameters:**
	
		===========     ============      ===========
		Parameter       Type              Description
		===========     ============      ===========
		``child``       ParticleType      The child particle to spawn. (can not be the same particle type)
		``amount``      number            The amount of particles to spawn. If this number is a negative, it will be used as a chance to spawn another particle instead.
		===========     ============      ===========
	
	**Example:**
		::
			
			firework:createOnDeath(ember, 7)
			
		Makes particle ``firework`` spawn 7 of the particle ``ember`` at the end of its life.
		
Methods (Other)
===============

``particletype:burst(depth, x, y, amount, [color])``
------------------------------------------------------------
	
	Creates a burst of particles at a location.
	
	**Parameters:**
	
		============     ============      ===========
		Parameter        Type              Description
		============     ============      ===========
		``depth``        string            The layer to spawn the particles on, can be either ``"above"``, ``"below"``, or ``"middle"``.
		``x``            number            The horizontal coordinate to spawn the particles at.
		``y``            number            The vertical coordinate to spawn the particles at.
		``amount``       number            The amount of particles to spawn.
		``color``        Color             *Optional:* The :doc:`color </class/color>` that the particles will have. Defaults to white, meaning no added coloration.
		============     ============      ===========
	
	**Example:**
		::
			
			eggplant:burst("above", x, y, 5)
			
		Creates 5 of the particle ``eggplant`` on the layer ``"above"``.

``particletype:reset()``
-------------------------------
	
	Clears all defined properties of the particle type, reverting it back to when it was created.
	
	**Example:**
		::
			
			eggplant:reset()
			
		Resets the particle ``eggplant`` back to an empty shell.
		
``particletype:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the particle type.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``particletype:getName()``
-------------------------------
	
	Returns the name of the particle type within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
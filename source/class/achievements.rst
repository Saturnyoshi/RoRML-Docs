*****************
Achievement Class
*****************

An ``Achievement`` is an unlockable that triggers upon doing some sort of objective. It can unlock new items and survivors.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Achievement.new(name)``
-------------------------------
	
	Alias: ``Achievement(name)``

	Creates a new achievement.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the achievement within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Achievement object.
	
	**Example:**
		::
			
			local harvestAchievement = Achievement.new("Harvester")
			
		Creates a new achievement called ``"Harvester"``.
	
``Achievement.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Achievement.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Achievement.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Achievements in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Fields
======

=================    ==================    ===========
Field                Type                  Description
=================    ==================    ===========
``requirement``      number                The target required to hit to unlock the achievement.
``sprite``           Sprite                The sprite of the achievement, displayed upon unlocking. Same size as an item sprite.
``unlockText``       string                The text that is displayed on the death screen / end screen when you unlock the achievement. Usually "This item will now drop." for items and "This character is now playable." for survivors.
``deathReset``       boolean               Sets whether the achievement progress should reset upon death.
``description``      string                The description of the achievement, namely the information on what to do to unlock it. Displayed on the achievement unlock popup and on Scores and Unlockables page, under Achievements.
``highscoreText``    string                The text that indicates what the achievement unlocked. Usually the item / survivor's ``displayName`` in quotes, followed by ``" Unlocked"``.
``parent``           Achievement or nil    The achievement's parent. The achievement can only be unlocked if the parent achievement is also unlocked. If set to ``nil`` it will not have any parent achievement and thus can be unlocked anytime.
=================    ==================    ===========


Methods
=======

``achievement:increment(inc)``
------------------------------------

	Increments the achievement's progress by ``inc``.
	
	If the achievement's ``requirement`` field is hit or surpassed, the achievement is unlocked.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``inc``         number      The amount to increase the achievement's progress by.
		===========     ======      ===========
	
	**Example:**
		::
			
			harvestAchievement:increment(1)
			
		Increments ``harvestAchievement``'s progress by one.
	
``achievement:assignUnlockable(thing)``
---------------------------------------------
	
	Assigns an :doc:`Item </class/item>` or :doc:`Survivor </class/survivor>` to the achievement, unlocking it when the achievement is unlocked.
	
	This automatically sets the ``highscoreText`` field to the item / survivor's ``displayName`` in quotes, followed by ``" Unlocked"``, the ``sprite`` field to the item / survivor's sprite and the ``unlockText`` to "This character is now playable." for survivors and "This item will now drop." for items.
	
	**Parameters:**
	
		===========     ================      ===========
		Parameter       Type                  Description
		===========     ================      ===========
		``thing``       Item or Survivor      The unlockable this achievement should unlock.
		===========     ================      ===========
	
	**Example:**
		::
			
			local scythe = Item.new("Scythe")
			harvestAchievement:assignUnlockable(scythe)
			
		Assigns the Item ``scythe`` to the ``harvestAchievement`` achievement, setting its ``highscoreText``, ``sprite`` and ``unlockText`` accordingly.
	
``achievement:isComplete()``
----------------------------------
	
	Checks if the achievement has already been unlocked.
	
	**Returns:**
	
		``true`` if the achievement has been unlocked, ``false`` otherwise.
	
	**Example:**
		::
			
			if harvestAchievement:isComplete() then
				player.blendColor = Color.GREEN
			end
			
		Checks if the ``harvestAchievement`` has been unlocked, and if so, sets the player's ``blendColor`` to green.
	
``achievement:addCallback(callback, bind)``
--------------------------------------------
	
	Binds a function to be called whenever the specified callback is fired.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``callback``     string        The name of the callback to bind a function to, see below for a list of callback names.
		``bind``         function      The function to be run when the callback is fired.
		============     ========      ===========
    
	*Callbacks:*
	
		===============    ===================================================
		Name               Description                                        
		===============    ===================================================
		``onIncrement``    Called each time the achievement is incremented.   
		``onComplete``     Called when the achievement is unlocked.           
		===============    ===================================================
    
	**Example:**
		::
			
			harvestAchievement:addCallback("onComplete", function()
				ParticleType.find("FireworkFlare", "vanilla"):burst("above", player.x, player.y, 10, Color.GREEN)
			end)
			
		This creates a burst of ``FireworkFlare`` particles spawn when the ``harvestAchievement`` achievement is unlocked.

``achievement:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the achievement.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``achievement:getName()``
-------------------------------
	
	Returns the name of the achievement.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
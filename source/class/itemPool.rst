**************
ItemPool Class
**************

An ``ItemPool`` is a container for multiple :doc:`Items </class/item>` objects used to randomly select which items are dropped.

For a complete list of pools in the base game, see :doc:`this page </misc/vanillaItemPools>`.

.. contents:: Table of Contents
   :local:

Static Methods
==============

.. _new-pool:

``ItemPool.new(name)``
-------------------------------
	
	Alias: ``ItemPool(name)``
	
	Creates a new ItemPool.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the itemPool. This is mostly used to distinguish the ItemPool from others in searches.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new ItemPool object.
	
	**Example:**
		::
			
			local blueItems = ItemPool.new("blue")
			
		Creates a new ItemPool called ``"blue"``.
	
``ItemPool.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing ItemPool.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``ItemPool.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing ItemPools in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
   
Fields
======

================    =======    ===========
Field               Type       Description
================    =======    ===========
``ignoreLocks``     boolean    If set to true all items in the pool will be able to drop even if the player hasn't unlocked them.
``ignoreEnigma``    boolean    | By default, use items will not be rolled if the Artifact of Enigma is enabled.
                               | Setting this flag to true allows the item pool to roll use items even when Enigma is active.
``weighted``        boolean    | Allows assigning different weights to items in the item pool.
                               | Setting this to false clears all existing weight values in the pool.
							   | The only default item pool where this is true is the ``gunchest`` pool.
================    =======    ===========

Methods
=======

``pool:add(item)``
-------------------------------
	
	Adds an :doc:`Item </class/item>` to the pool.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The Item to be added to the pool.
		===========     ======      ===========
	
	**Example:**
		::
			
			coolPool:add(Item.find("Infusion"))
			
		Adds Infusion to the coolPool pool.
		
``pool:remove(item)``
-------------------------------
	
	Removes an :doc:`Item </class/item>` from the pool.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The Item to be removed from the pool.
		===========     ======      ===========
	
	**Example:**
		::
			
			coolPool:remove(Item.find("Infusion"))
			
		Removes Infusion from the coolPool pool.
		
``pool:contains(item)``
-------------------------------
	
	Checks if an :doc:`Item </class/item>` is in the pool.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The Item to checked for.
		===========     ======      ===========
	
	**Returns:**
		
		Returns true if the item is in the pool, otherwise returns false.
	
	**Example:**
		::
			
			if coolPool:contains(Item.find("Infusion")) then
			    print("uncool")
			end
			
		Prints "uncool" if Infusion is in coolPool.
		
``pool:roll()``
-------------------------------
	
	**Returns:**
	
		Returns one :doc:`Item </class/item>` randomly selected from the pool.
	
	**Example:**
		::
			
			coolPool:roll().getObject().create(x, y)
			
		Creates an instance of a random object from the coolPool pool at x, y.
		
``pool:setWeight(item, weight)``
--------------------------------
	
	Has no effect if ``pool.weighted`` is not set to true.

	Sets the weight of the item within the pool.

	Should be called after the item is *already* added to the pool.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to set the weight of.
		``weight``      number      The new weight of the item.
		===========     ======      ===========
		
``pool:getWeight(item)``
--------------------------------
		
	Returns the weight of an item in the pool. 

	Defaults to 1 and always returns 1 if ``pool.weighted`` is not set to true. 
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to set the weight of.
		``weight``      number      The new weight of the item.
		===========     ======      ===========
	
	**Returns:**

		The weight of the item in the pool.
	
``pool:getCrate()``
-------------------------------
	
	**Returns:**
	
		Returns the command crate :doc:`GMObject </class/gmObject>` for the pool.
	
	**Example:**
		::
			
			coolPool:getCrate().create(x, y)
			
		Creates a command crate containing the coolPool items.

``pool:toList()``
------------------------
	
	**Returns:**
	
		Returns an array table containing all the :doc:`Items </class/item>` within the pool.
	
	**Example:**
		::
		
			local items = pool:toList()
			for _, v in ipairs(items) do
			    print(v:getName())
			end
		
		Prints the internal name of all items in ``pool``.
		
``pool:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the ItemPool.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``pool:getName()``
-------------------------------
	
	Returns the name of the ItemPool within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
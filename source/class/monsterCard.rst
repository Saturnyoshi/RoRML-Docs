*****************
MonsterCard Class
*****************

A ``MonsterCard`` defines the rules which allow a type of enemy to spawn in a level.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``MonsterCard.new(name, object)``
---------------------------------
	
	Alias: ``MonsterCard(name, object)``

	Creates a new monster card, binding it to the passed object.
	
	| Depending on the object, some properties of the resulting card are automatically set:
	| - Classic enemies are initialized with all vanilla elite types.
	| - Bosses automatically have ``isBoss`` set to true.
	| - If the enemy is classic, spawn type is set to ``"classic"``, otherwise it's set to ``"offscreen"``.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``name``        string        The name to give the monster card within the current namespace.
		``object``      GMObject      The actual object representing the enemy.
		===========     ========      ===========

	**Returns:**
	
		Always returns a new MonsterCard object.
	
	**Example:**
		::
			
			local enemy_obj = Object.base("EnemyClassic", "myEnemy")
			local enemy_card = MonsterCard.new("myEnemyCard", enemy_obj)
			
		Creates a new enemy and its spawn card.

``MonsterCard.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing monster card.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``MonsterCard.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing monster cards in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Fields
======

============================    ==================    ===========
Field                           Type                  Description
============================    ==================    ===========
``type``                        string                Determines where the enemy is spawned. Applicable values are listed below.
``cost``                        number                The number of spawn points the director needs to spawn this enemy. Some base game values are listed below as a reference.
``sound``                       Sound                 *(Only applicable if the spawn type is set to "classic")* The sound played when the enemy appears.
``sprite``                      Sprite                *(Only applicable if the spawn type is set to "classic")* The spawn animation used when the enemy appears.
``isBoss``                      boolean               Whether the enemy should be spawned by teleporter activation. Automatically set based on whether the spawned object is a boss, but may be overridden.
``eliteTypes``                  List<EliteType>       A :doc:`List </class/list>` of all possible :doc:`EliteTypes </class/eliteType>` the enemy can spawn with.
``canBlight``                   boolean               Whether it's possible for a blighted version of this enemy to spawn.
``object``                      GMObject              (Read only) The object the card was created with. This field is the same as the card's ``getObject`` method.
============================    ==================    ===========

The following is a list of possible values for the ``type`` field:

===============    ==================
Value              Description
===============    ==================
``"classic"``      Spawns on the ground with an animation and sound. This is the typical spawn type for classic enemies.
``"offscreen"``    Spawns at the edge of the game view, slightly offscreen. This is the typical spawn type for flying enemies.
``"player"``       Spawns the enemy at the exact same coordinates as the player. This is used by a few bosses to find an appropriate spawn location.
``"origin"``       Spawns the enemy at the coordinates 0,0. This is used by the Magma Worm because the spawned object is a controller and doesn't have a hitbox.
===============    ==================

For reference, the spawn cost of some enemies are as follows:

========================================          ==================
Enemy                                             Cost
========================================          ==================
Lemurian, Child                                   8
Clay Man                                          20
Stone Golem                                       29
Sand Crab                                         33
Jellyfish                                         37
Wisp, Evolved Lemurian                            40
Parent                                            45
Mushrum                                           50
Whorl, Imp                                        100
Greater Wisp                                      160
Elder Lemurian                                    180
Archaic Wisp                                      420
Young Vagrant                                     460
Colossus, Magma Worm, Wandering Vagrant           760
Ancient Wisp                                      860
Cremator, Toxic Beast                             1220
Ifrit                                             1240
Scavenger                                         1320
========================================          ==================

Methods
=======

``MonsterCard:getObject()``
------------------------------------

	**Returns:**

		Returns the object represented by this monster card.


``MonsterCard:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the monster card.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``MonsterCard:getName()``
-------------------------------
	
	Returns the name of the monster card.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
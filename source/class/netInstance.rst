*****************
NetInstance Class
*****************

The ``NetInstance`` class is used to transfer instances which are already synced by the vanilla game.

.. contents:: Table of Contents
   :local:

Fields
======

===============    ========    ===========
Field              Type        Description
===============    ========    ===========
``id``             number      (Read only) (Alias: ``ID``) The actual instance's network ID.
``object``         GMObject    (Read only) The actual instance's object.
===============    ========    ===========

Methods
=======

``netinstance:resolve()``
-------------------------------

	| Used to find the actual instance.
	| It is possible that the instance no longer exists and this will fail, in which case ``nil`` will be returned instead.

	**Returns:**
	
		Returns the instance if found, ``nil`` otherwise.

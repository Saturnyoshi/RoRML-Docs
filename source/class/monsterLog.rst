****************
MonsterLog Class
****************

A ``MonsterLog`` is the unlockable data entry associated with an enemy.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``MonsterLog.new(name)``
---------------------------------
	
	Alias: ``MonsterLog(name)``

	Creates and returns a new monster log.
	
	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``name``        string        The name to give the monster log within the current namespace.
		===========     ========      ===========

	**Returns:**
	
		Always returns a new MonsterLog object.
	
	**Example:**
		::
			
			local enemy_obj = Object.base("EnemyClassic", "myEnemy")
			local enemy_log = MonsterLog.new("My Enemy")
			MonsterLog.map[enemy_obj] = enemy_log
			
		Creates a new enemy and its log, assigning the log to the enemy.

``MonsterLog.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing monster log.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``MonsterLog.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing monster logs in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

Static Fields
=============

``MonsterLog.map``
------------------

	A :doc:`Map </class/map>` associating an enemy's :doc:`GMObject </class/gmObject>` to its log entry.

	Assigning a log to an object in this map will cause that object to drop the log when killed.

Fields
======

============================    ======    ===========
Field                           Type      Description
============================    ======    ===========
``displayName``                 string    The name of the log as displayed in-game. 
``story``                       string    The main lore of the enemy.
``statHP``                      number    The enemy's displayed HP stat.
``statDamage``                  number    The enemy's displayed damage stat.
``statSpeed``                   number    The enemy's displayed speed stat.
``sprite``                      Sprite    The sprite of the enemy used as the log icon.
``portrait``                    Sprite    The big sprite of the enemy seen when viewing the log.
``portraitSubimage``            number    The subimage of the portrait sprite to display. Note that this is 0-indexed rather than the usual 1-indexing.
============================    ======    ===========

Methods
=======

``monsterLog:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the monster log.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``monsterLog:getName()``
-------------------------------
	
	Returns the name of the monster log.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
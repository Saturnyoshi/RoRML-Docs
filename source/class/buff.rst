**********
Buff Class
**********

A ``Buff`` is a positive or negative status effect which can be applied to most actors.

Applying and removing buffs is handled in the :doc:`ActorInstance </class/actorInstance>` class.

For a full list of buffs added by the base game, see :doc:`this page </misc/vanillaBuffs>`.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Buff.new([name])``
-------------------------------
	
	Alias: ``Buff([name])``

	Creates a new Buff.

	If a name is not provided then an automatically generated name will be used.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      *Optional:* The name to give the buff within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Buff object.
	
	**Example:**
		::
			
			local jumpboost = Buff.new("Jump Boost")
			
		Creates a new buff called ``"Jump Boost"``.


``Buff.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Buff.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Buff.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Buffs in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

Fields
======

===============    =======    ===========
Field              Type       Description
===============    =======    ===========
``sprite``         Sprite     The sprite of the buff. This will appear above the affected character.
``subimage``       number     The frame of the sprite to use. Defaults to 1.
``frameSpeed``     number     How fast the buff sprite should animate. Defaults to 0 for no animation.
===============    =======    ===========

Methods
=======

``buff:addCallback(callback, bind)``
------------------------------------
	
	Binds a function to be called when a specific callback is fired.
	
	The :doc:`ActorInstance </class/actorInstance>` affected by the buff is always passed to the function.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``callback``     string        The name of the callback to bind a function to, see below for a list of callback names.
		``bind``         function      The function to be run when the callback is fired. The function is always passed the ActorInstance affected by the buff.
		============     ========      ===========
		
	**Callbacks:**
		
		==========    ===========
		Name          Description
		==========    ===========
		``start``     Fired when the buff is first applied.
		``step``      | Fired every step that the buff is applied to the ActorInstance.
		              | Additionally, the remaining time for the buff is passed as the second argument.
		``end``       Fired when the buff ends.
		==========    ===========
	
	**Example:**
		::
			
			jumpboost:addCallback("start", function(player)
			    player:set("pVmax", player:get("pVmax") + 1)
			end)
			
			jumpboost:addCallback("end", function(player)
			    player:set("pVmax", player:get("pVmax") - 1)
			end)
			
		When the ``jumpboost`` buff is applied to an actor it gives them the ability to jump a bit higher, which is reverted once the buff wears off.

``buff:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the Buff.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``buff:getName()``
-------------------------------
	
	Returns the name of the Buff within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
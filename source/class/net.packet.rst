****************
net.Packet Class
****************

The ``net.Packet`` class is used to transfer information between clients in online co-op.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``net.Packet.new(name, handler)``
----------------------------------
	
	Alias: ``net.Packet(name, handler)``

	Defines a new packet type.
	Takes a function which is called when the packet is received.
	
	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``name``        string        The name of the packet. Used to identify the packet between clients.
		``handler``     function      | The function called when the packet is received.
		                              | The first argument passed is the ``PlayerInstance`` of the sender followed by arguments the packet was sent with.
		===========     ========      ===========

	**Returns:**
	
		Returns the new packet type.

Supported Argument Types
========================

.. warning:: As of the current version, passing nil as an argument to a packet will cut off any following arguments.

When passing RoRML types (except :doc:`NetInstance </class/netInstance>`) the resource must exist on both the sender and the receiver.
If you cannot be sure, send the name of the resource over as a string and ``find`` it on the receiving end.
 
Below is a list of types which can be passed to packets. Passing anything else will cause an error.

========    ===========    ========
number      string         boolean
Sprite      Sound          Item
GMObject    NetInstance
========    ===========    ========

Methods
=======

``packet:sendAsClient(...)``
-------------------------------
	
	Sends the packet to the game host.
	Does nothing if in singleplayer or if the local player is the host.
	
	**Parameters:**

		Any of the above supported types.

``packet:sendAsHost(target, player, ...)``
------------------------------------------
	
	Sends the packet to clients.
	Does nothing if in singleplayer or if the local player is not the host.
	
	The different supported values of ``target`` are as follows:

	===========    =============    ====
	Name           String Alias     Description
	===========    =============    ====
	net.ALL        ``"all"``        | Sends the packet to all connected clients.
	                                | ``player`` can be nil when using this.
	net.EXCLUDE    ``"exclude"``    Sends the packet to all players *except for* the specified player.
	net.DIRECT     ``"direct"``     Only sends the packet to the specified player.
	===========    =============    ====

	**Parameters:**

		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``target``      string              Any of the above 3 sending types.
		``player``      PlayerInstance      | The player to send to or exclude when using the ``DIRECT`` or ``EXCLUDE`` settings respectively.
		                                    | This can be ``nil`` when using the ``ALL`` setting.
		...                                 Any number of arguments of the above supported types.
		===========     ==============      ===========
	
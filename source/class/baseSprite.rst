****************
BaseSprite Class
****************

The ``BaseSprite`` class provides shared methods to the :doc:`Sprite </class/sprite>` and :doc:`DynamicSprite </class/dynamicSprite>` classes.

.. contents:: Table of Contents
   :local:

Fields
======

.. note:: All sprite fields are read only and cannot be modified.

=====================    ======    ===========
Field                    Type      Description
=====================    ======    ===========
``xorigin``              number    The horizontal origin [#f1]_ of the sprite.
``yorigin``              number    The vertical origin [#f1]_ of the sprite.
``width``                number    The width of the sprite, in pixels.
``height``               number    The height of the sprite, in pixels.
``frames``               number    The number of frames in the sprite.
``boundingBoxLeft``      number    The relative position of the left of the sprite bounding box [#f2]_\ .
``boundingBoxRight``     number    The relative position of the right of the sprite bounding box [#f2]_\ .
``boundingBoxTop``       number    The relative position of the top of the sprite bounding box [#f2]_\ .
``boundingBoxBottom``    number    The relative position of the bottom of the sprite bounding box [#f2]_\ .
=====================    ======    ===========

.. [#f1] The origin of the sprite is used as the sprite's pivot point.

.. [#f2] The sprite bounding box is the area of the sprite used for collision detection.

Methods
=======

``sprite:draw(x, y, subimage)``
-------------------------------
	
	Draws the sprite to the current drawing surface.
	
	**Parameters:**
	
		============     ======      ===========
		Parameter        Type        Description
		============     ======      ===========
		``x``            number      The horizontal position to draw the sprite at.
		``y``            number      The vertical position to draw the sprite at.
		``subimage``     number      The subimage of the sprite to draw.
		============     ======      ===========
	
	**Example:**
		::
			
			banana:draw(player.x, player.y, 1)
			
		Draws the banana sprite to the screen at the player position.
		
``sprite:replace(new)``
-------------------------------
	
	Replaces a sprite with another sprite. Useful for visual mods.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``new``         Sprite      The sprite that will replace the old sprite.
		===========     ======      ===========
	
	**Example:**
		::
			
			for _, v in ipairs(Graphics.findAllSprites("vanilla")) do
			    v:replace(eggplant)
			end
			
		Replaces every single sprite in the vanilla game with the sprite ``eggplant``.

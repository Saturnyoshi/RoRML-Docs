***********
Stage Class
***********

The ``Stage`` class is used to get and change information for the in-game levels.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Stage.getCurrentStage()``
-------------------------------
	
	**Returns:**
	
		Returns the currently loaded stage.
	
	**Example:**
		::
			
			print(Stage.getCurrentStage().displayName)
			
		Prints the name of the current stage.

``Stage.progressionLimit(value)``
------------------------------------------------
	
	Gets or sets the number of stages until the last stage.

	The last set of stages in the progression is used as the final level accessed through the divine teleporter.

	Normally the limit is 6.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``value``       number        *Optional:* The new value if setting, or ``nil`` if getting.
		===========     ========      ===========

	**Returns:**
	
		The current stage limit if ``value`` is ``nil``, otherwise returns ``value``.

	**Example:**
		::
			
			Stage.progressionLimit(3)
			
		Change sthe number of stages in a run to 3.

.. _stage-getProgression:

``Stage.getProgression(index)``
------------------------------------------------
	
	Gets the :doc:`list </class/list>` of stages that can be selected for the ``index``-nth level.

	Unless you need to access level indexes after the default 6, using the built-in :ref:`Stage.progression <stage-progression>` table is usually preferable.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``index``       number        The level index to get the stages for.
		===========     ========      ===========

	**Returns:**
	
		A :doc:`List\<Stage\> </class/list>` containing the stages that can be selected for level ``index``.

	**Example:**
		::
			
			Stage.progressionLimit(7)
			local list = Stage.getProgression(7)
		
		Changes the maximum stage to 1 higher than the base game and then gets the stages at the newly added stage index.

``Stage.transport(stage)``
------------------------------------------------
	
	Changes the current stage to `stage`.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``stage``       Stage         The stage to teleport to.
		===========     ========      ===========

	**Example:**
		::
			
			local boarBeach = Stage.find("Boar Beach")
			Stage.transport(boarBeach)
		
		Immediately teleports every player to the stage "Boar Beach".

``Stage.getDimensions()``
-------------------------------
	
	**Returns:**
	
		Returns the width and height of the current stage, in pixels.
	
	**Example:**
		::
			
			local width, height = Stage.getDimensions()
			
		Stores the size of the current stage in the ``width`` and ``height`` variables.

``Stage.collidesPoint(x, y)``
-------------------------------
	
	Used to check whether the map is solid at a specific point.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``x``           number        The x coordinate to check at.
		``y``           number        The y coordinate to check at.
		===========     ========      ===========

	**Returns:**
	
		A boolean indicating whether there was a collision.

``Stage.collidesRectangle(x1, y1, x2, y2)``
------------------------------------------------
	
	Used to check for map solidity within a rectangle

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``x1``          number        The left x coordinate to check at.
		``y1``          number        The top y coordinate to check at.
		``x2``          number        The right x coordinate to check at.
		``y2``          number        The bottom y coordinate to check at.
		===========     ========      ===========

	**Returns:**
	
		A boolean indicating whether there was a collision.

``Stage.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Stage.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Stage.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Stages in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

Static Fields
=============
		
.. _stage-progression:
		
``Stage.progression``
---------------------------

	Contains a numerically indexed table of all :doc:`lists </class/list>` of stages normally found in the game.

	If trying to access level indexes after the default 6, :ref:`Stage.getProgression <stage-getProgression>` must be used.
	
	**Example:**
		::
		
			local stage = Stage.progression[2][1]
			
		Stores the first stage available for selection at level index 2 in the local variable ``stage``.

Fields
======
   
======================    =========================    ===========
Field                     Type                         Description
======================    =========================    ===========
``displayName``           string                       The name of the stage displayed on the screen when you enter it. Note that the internal name of the stage will still be the same if this is altered.
``subname``               string                       The text displayed under the name of the stage when it's entered.
``disabled``              boolean                      Removes the stage from stage selection. Be sure not to disable all stages of a certain set otherwise the game will crash.
``music``                 Sound                        The music used by this stage.
``enemies``               List<monsterCard>            The list of :doc:`enemy cards </class/monsterCard>` that can spawn in this stage.
``interactables``         List<Interactable>           The list of :doc:`interactables </class/interactable>` that can spawn in this stage.
``interactableRarity``    Map<Interactable, number>    A map containing each interactable's spawn rarity. Spawn rarity modifies how far away an object has to be from the stage entrance to spawn. This is assumed 1 by default.
``rooms``                 List<Room>                   The list of :doc:`rooms </class/room>` present in this stage.
``teleporterIndex``       number                       The subimage used by the teleporter the player enters through. Can be used to select stage specific enterance teleporters.
======================    =========================    ===========

Methods
=======

``stage:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the Stage.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``stage:getName()``
-------------------------------
	
	Returns the name of the Stage within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
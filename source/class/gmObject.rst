**************
GMObject Class
**************

A ``GMObject`` is a specific type of :doc:`Instance </class/instance>`.

GMObject inherits all functionality from :doc:`GMObjectBase </class/gmObjectBase>`.

A list of most usable objects added by the base game can be found on :doc:`this page </misc/objects>`.

.. contents:: Table of Contents
   :local:
   
Static Methods
==============

``Object.new([name])``
-------------------------------
	
	Alias: ``Object([name])``

	Creates a new GMObject.

	If a name is not provided then an automatically generated name will be used.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      *Optional:* The name to give the object within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new GMObject.
	
	**Example:**
		::
			
			local fireball = Object.new("fireball")
			
		Creates a new GMObject.
		
.. _object-base:

``Object.base(kind, [name])``
-------------------------------
	
	Creates a new GMObject, inheriting properties from a base object type.

	If a name is not provided then an automatically generated name will be used.

	This method is used to generate objects for custom enemies, interactables, and other features with built-in behavior.

	See :doc:`here </misc/baseObjects>` for information on each base object type.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``kind``        string      The base object type to inherit from.
		``name``        string      *Optional:* The name to give the object within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new GMObject.
	
	**Example:**
		::
			
			local fireball = Object.new("fireball")
			
		Creates a new GMObject.
		
``Object.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing GMObject.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Object.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing GMObjects in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

``Object.fromID(id)``
--------------------------------------------------------------
	
	Used to fetch a GMObject from its GameMaker resource ID.

	Useful for getting objects from instance variables which contain an object ID. 
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``id``          number      The GameMaker ID of the object.
		===========     ======      ===========

	**Returns:**
	
		Returns the GMObject if found, nil otherwise.

``Object.findInstance(id)``
--------------------------------
	
	Finds the :doc:`Instance </class/instance>` with a certain ID.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``id``          number      The ID of the instance to find.
		===========     ======      ===========

	**Returns:**
	
		Returns an :doc:`Instance </class/instance>` when found, otherwise returns nil.
	
	**Example:**
		::
			
			local inst = Object.findInstanceByID(someID)
			
		Finds the instance with the ID stored in ``someID`` and stores it in the variable ``inst``.
	
Fields
======

===============    =======    ===========
Field              Type       Description
===============    =======    ===========
``id``             number     (Alias: ``ID``) The object's GameMaker resource ID.
``sprite``         Sprite     The default sprite of the object.
``depth``          number     The depth the object will be rendered at. A higher depth means the object will be rendered further back. Defaults to 0.
===============    =======    ===========

Methods
=======

``object:create(x, y)``
-------------------------------
	
	Creates an :doc:`Instance </class/instance>` of the object at the specified position in the game world.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal coordinate to create the instance at.
		``y``           number      The vertical coordinate to create the instance at.
		===========     ======      ===========

	**Returns:**
	
		Returns the newly created instance.
	
	**Example:**
		::
			
			eggplant:create(200, 200)
			
		Creates an instance of the object ``eggplant`` at coordinates 200, 200.
		
``object:addCallback(callback, bind)``
------------------------------------------
	
	| Binds a function to be called by :doc:`Instances </class/instance>` of the object alongside a specific event.
	| The :doc:`Instance </class/instance>` calling the function is passed along to it.
	
	Note that some events are not currently supported by built-in objects.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``callback``     string        The name of the callback to bind a function to, see below for a list of callback names.
		``bind``         function      The function to be run when the callback is fired. The function is always passed the :doc:`Instance </class/instance>` which fired the callback.
		============     ========      ===========
		
	*Callbacks:*
	
		===========    ===================================================    ===========================
		Name           Description                                            Works with built-in objects
		===========    ===================================================    ===========================
		``create``     Called each time a new instance is created.            Yes
		``destroy``    Called when an instance is destroyed.                  Yes
		``step``       Called each frame when instance logic is handled.      No
		``draw``       Called when the instance is rendered to the screen.    No
		===========    ===================================================    ===========================
	
	**Example:**
		::
			
			leftMover:addCallback("step", function(self)
				self.x = self.x - 2
			end)
			
		This makes instances of the object ``leftMover`` move to the left by 2 pixels each frame.
		
``obj:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the GMObject.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``obj:getName()``
-------------------------------
	
	Returns the name of the GMObject within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
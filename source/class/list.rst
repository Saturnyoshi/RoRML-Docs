*****************
List<T> Class
*****************

A ``List<T>`` is a collection that can hold several unique ``T`` elements. Lists cannot hold more than one type of element.

To get a list element, you can treat it as a regular numerically-indexed Lua table:

	::
		
		local elem = list[1]
		
	Stores the first element in the list ``list`` to the variable ``elem``, or ``nil`` if there's no element.

Please note that you **cannot** use this to add/remove values. You must use the methods defined below.

Lists cannot be instanciated; they are instead used by some classes, such as :doc:`Stages </class/stage>`'s enemies and rooms.

.. contents:: Table of Contents
   :local:

Methods
=======

``list:len()``
------------------------------------

	Gets the total amount of elements inside the list.
	
	**Returns:**
	
		Number of elements in list.
	
	
``list:contains(value)``
---------------------------------------------
	
	Checks if a given value exists in the list.
	
	**Parameters:**
	
		===========	 ================	  ===========
		Parameter	   Type				  Description
		===========	 ================	  ===========
		``value``	   T					 The element to find in the table.
		===========	 ================	  ===========
	
	**Returns:**
	
		``true`` if the table contains the given value, ``false`` otherwise.
	
	**Example:**
		::
			
			local happy = memeList:contains("dank")
			
		Gets whether the List<string> ``memeList`` contains the value ``dank``.
	
``list:add(value)``
----------------------------------
	
	Inserts the given value to the end of the list, if it doesn't already exist.
	
	Keep in mind that list elements have to be unique (no duplicates).
	
	**Returns:**
	
		``true`` if the value was added successfully, ``false`` if the value already exists in the list.
	
	**Example:**
		::
			
			alphabetList:add("E")
			
		Adds the string ``"E"`` to the List<string> ``alphabetList``.
	
``list:remove(value)``
----------------------------------
	
	Removes the given value from the list, if it exists.
	
	**Returns:**
	
		``true`` if the value was removed successfully, ``false`` if the value does not exist in the list.
	
	**Example:**
		::
			
			favoriteNumberList:remove(8)
			
		Removes the number ``8`` from the List<number> ``favoriteNumberList``.
	
``list:toTable()``
----------------------------------
	
	Creates a new table containing every value in the list.
	
	**Returns:**
	
		A new table that is an exact copy of the list.
	
	**Example:**
		::
			
			local stage = Stage.getCurrentStage()
			local enemyTable = stage.enemies:toTable()
			
		Creates a new table containing all enemies the current stage can spawn, and stores it in ``enemyTable``.
	
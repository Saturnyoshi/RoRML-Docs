*************
Surface Class
*************

A ``Surface`` is an image which can be drawn to.

.. note:: Surfaces become invalid when the game is minimized, so you should always make sure a surface is valid before accessing it.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Surface.new(width, height)``
--------------------------------------
	
	Alias: ``Surface(width, height)``
	
	Creates a new Surface.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``width``       number      The width of the surface, in pixels.
		``height``      number      The height of the surface, in pixels.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Surface object.
	
	**Example:**
		::
			
			local vine = Surface.new(8, 256)
			
		Creates a new surface with a width of 8 and a height of 256.

``Surface.isValid(value)``
---------------------------

	Checks if a value is a valid surface.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       any         The value to check.
		===========     ======      ===========
		
	**Returns:**
	
		Returns true if the value is a surface, and ``value:isValid()`` is true.

Fields
======

==============    ======    ===========
Field             Type      Description
==============    ======    ===========
``width``         number    The width of the surface, in pixels.
``height``        number    The height of the surface, in pixels.
==============    ======    ===========

Methods
=======

``surface:draw(x, y)``
-------------------------------
	
	Draws the surface at the specified coordinates.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal position of the top left corner of where to draw the surface, relative to the current drawing surface.
		``y``           number      The vertical position of the top left corner of where to draw the surface, relative to the current drawing surface. 
		===========     ======      ===========
	
	**Example:**
		::
			
			bubble:draw(player.x - 10, player.y - 10)
			
		Draws the surface ``bubble`` relative to the position of ``player``.

``surface:isValid()``
-------------------------------
	
	Checks whether the surface still exists.
	
	In some cases using ``Surface.isValid`` may be preferable.

	**Returns:**
	
		Returns true if surface exists, false if it doesn't.
	
	**Example:**
		::
			
			if not vine:isValid() then
			    vine = constructVineSurface()
			end
			vine:draw(vineposx, vineposy)
			
		Checks if the surface ``vine`` is valid before drawing it.
		If the surface is not valid, then a function is called to create a new surface.
		
``surface:clear()``
-------------------------------
	
	Fills the surface with full transparency.
	
	**Example:**
		::
			
			vine:clear()
			
		Removes anything drawn to the surface ``vine`` so it can be drawn to without overlapping the previous contents.

``surface:free()``
-------------------------------
	
	Manually deletes the surface. 
	
	Surfaces take up graphical memory which is a limited resource, so it is important to free them when you're done with them.
	
	**Example:**
		::
			
			local tempsurf = createTempSurface()
			tempsurf:draw(xpos, ypos)
			tempsurf:free()
			
		In this code we create a temporary surface and then draw it, then since we're not going to need it for next frame, we free it from memory.

``surface:createSprite(xorigin, yorigin, x, y, w, h)``
------------------------------------------------------------

.. _surface-createsprite:

	Creates a new :doc:`DynamicSprite </class/dynamicSprite>`, adding the surface as its only subimage. 

	**Parameters:**
	
		===========     =======      ===========
		Parameter       Type         Description
		===========     =======      ===========
		``xorigin``     number       The x coordinate of the sprite's origin.
		``yorigin``     number       The y coordinate of the sprite's origin.
		``x``           number       *Optional:* The left x coordinate of the region of the surface to copy. Defaults to 0.
		``y``           number       *Optional:* The top y coordinate of the region of the surface to copy. Default to 0.
		``w``           number       *Optional:* The width of the surface. Defaults to the width of the surface.
		``h``           number       *Optional:* The height of the surface. Defaults to the height of the surface.
		===========     =======      ===========

	**Returns:**

		The newly created :doc:`DynamicSprite </class/dynamicSprite>` object.
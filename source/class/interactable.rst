******************
Interactable Class
******************

An ``Interactable`` represents an object randomly spawned in a :doc:`Stage </class/stage>`, such as chests, broken drones, or shrines.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Interactable.new(obj, name)``
-------------------------------
	
	Alias: ``Interactable(obj, name)``

	Creates a new interactable from an existing object, with the specified name.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``obj``         string      The object represented by this interactable.
		``name``        string      The name to give the interactable within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Interactable.
	
	**Example:**
		::
			
			local toastObject = Object.new("Toast_Object")
			local toastInteractable = Interactable.new(toastObject, "Toast_Interactable")
		
		Creates a new interactable called ``"Toast_Interactable"``.

``Interactable.fromObject(obj)``
--------------------------------
	
	Finds an existing interactable given its object.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``obj``         string      The object represented by this interactable.
		===========     ======      ===========

	**Returns:**
	
		Returns an Interactable if one exists for the given object, ``nil`` otherwise.
	
	**Example:**
		::
		
			local chestObject = Object.find("Chest", "vanilla")
			local chestInteractable = Interactable.fromObject(chestObject)
		
		Gets the existing interactable for the Chest object.
	
``Interactable.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Interactable.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Interactable.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Interactables in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Fields
======

=================    ==================    ===========
Field                Type                  Description
=================    ==================    ===========
``spawnCost``        number                | This number of spawn points the object costs to spawn. Defaults to 75, the same amount as item shops.
                                           | For reference, small chests take 50, large chests 110, golden chests 250, small barrels 7, and chance shrines 65.
=================    ==================    ===========


Methods
=======

``interactable:getObject()``
------------------------------------

	**Returns:**

		Returns the object represented by this interactable.

``interactable:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the interactable.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``interactable:getName()``
-------------------------------
	
	Returns the name of the interactable.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
******************
GMObjectBase Class
******************

The ``BaseSprite`` class provides shared methods to the :doc:`GMObject </class/gmObject>` and :doc:`ParentObject </class/parentObject>` classes.

.. contents:: Table of Contents
   :local:
   
Methods
=======

.. note:: Methods which return an :doc:`Instance </class/instance>` may return any :doc:`Instance </class/instance>` child class depending on the calling object.

``object:find(n)``
-------------------------------
	
	Finds the ``n`` th :doc:`Instance </class/instance>` of the object.
	Instances are sorted by the order they were created.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``n``           number      The number of the instance to find.
		===========     ======      ===========

	**Returns:**
	
		Returns the :doc:`Instance </class/instance>` if found, otherwise returns nil.
	
	**Example:**
		::
			
			for i = 1, 5 do
				local instance = eggplant:find(i)
				print(instance.ID)
			end
			
		Prints the IDs of the first 5 instances of the ``eggplant`` object.
		
``object:findAll()``
-------------------------------
	
	Finds all :doc:`Instances </class/instance>` of the object.

	**Returns:**
	
		Returns a numerically indexed table containing all the found :doc:`Instances </class/instance>`.
	
	**Example:**
		::
			
			for _, instance in ipairs(eggplant:findAll()) do
			    instance:destroy()
			end
			
		Destroys all instances of the object ``eggplant``.
		
``object:findNearest(x, y)``
-------------------------------
	
	Finds the :doc:`Instance </class/instance>` of the object closest to a point.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal coordinate of the point to check from.
		``y``           number      The vertical coordinate of the point to check from.
		===========     ======      ===========

	**Returns:**
	
		Returns the :doc:`Instance </class/instance>` if found, otherwise returns nil.
	
	**Example:**
		::
			
			local target = eggplant:findNearest(player.x, player.y)
			
		Finds the instance of the object ``eggplant`` closest to the instance ``player``.
		
``obj:findFurthest(x, y)``
-------------------------------
	
	Finds the :doc:`Instance </class/instance>` of the object furthest from a point.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal coordinate of the point to check from.
		``y``           number      The vertical coordinate of the point to check from.
		===========     ======      ===========

	**Returns:**
	
		Returns the :doc:`Instance </class/instance>` if found, otherwise returns nil.
	
	**Example:**
		::
			
			local target = eggplant:findFurthest(player.x, player.y)
			
		Finds the instance of the object ``eggplant`` farthest from the instance ``player``.
		
``obj:findMatching(key, value, ...)``
---------------------------------------
	
	Finds all :doc:`Instances </class/instance>` of the object with matching key and value variable pairs.
	
	**Parameters:**
	
		===========     =====================       ===========
		Parameter       Type                        Description
		===========     =====================       ===========
		``key``         string                      The name of the first variable to match.
		``value``       string, number or nil       The value of the first variable to match.
		...                                         Any number of additional key and value pairs may be specified.
		===========     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table containing all the matching :doc:`Instances </class/instance>`.
	
	**Example:**
		::
			
			local lizard = Object.find("lizard", "vanilla")
			for _, v in ipairs(lizard:findMatching("boost", nil) do
			    v:set("hp", v:get("hp") + 50)
			    v:set("boost", 1)
			end
			
		Heals all Lemurians where ``boost`` is nil by 50 health, and then sets ``boost`` to 1 so they're only healed once.
		
``obj:findMatchingOp(key, operator, value, ...)``
-----------------------------------------------------
	
	Finds all :doc:`Instances </class/instance>` of the object with matching key and value variable pairs.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``key``          string                      The name of the first variable to compare against.
		``operator``     string                      | The operator to compare with.
		                                             | Valid operators are ``"=="``, ``"~="``,  ``"<"``,  ``">"``,  ``"<="``, and ``">="``.
		``value``        string, number or nil       The value of the first variable to compare against.
		...                                          Any number of additional key, operator and value sets may be specified.
		============     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table containing all the matching :doc:`Instances </class/instance>`.
	
	**Example:**
		::
			
			local lizard = Object.find("lizard", "vanilla")
			for _, v in ipairs(lizard:findMatchingOp("hp", "<", 30) do
			    v:kill()
			end
			
		Kills all Lemurians with less than 30 remaining health.
		
``obj:findRectangle(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds a single :doc:`Instance </class/instance>` inside of the specified rectangle.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      First x coordinates of the rectangle to check.
		``y1``           number                      First y coordinates of the rectangle to check.
		``x2``           number                      Second x coordinates of the rectangle to check.
		``y2``           number                      Second y coordinates of the rectangle to check.
		============     =====================       ===========

	**Returns:**
	
		Returns the first :doc:`Instance </class/instance>` found within the rectangle.

``obj:findAllRectangle(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds all :doc:`Instances </class/instance>` inside of the specified rectangle.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      First x coordinates of the rectangle to check.
		``y1``           number                      First y coordinates of the rectangle to check.
		``x2``           number                      Second x coordinates of the rectangle to check.
		``y2``           number                      Second y coordinates of the rectangle to check.
		============     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table of :doc:`Instances </class/instance>` found within the rectangle.
		
``obj:findEllipse(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds a single :doc:`Instance </class/instance>` inside of the specified ellipse.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      First x coordinates of the ellipse to check.
		``y1``           number                      First y coordinates of the ellipse to check.
		``x2``           number                      Second x coordinates of the ellipse to check.
		``y2``           number                      Second y coordinates of the ellipse to check.
		============     =====================       ===========

	**Returns:**
	
		Returns the first :doc:`Instance </class/instance>` found within the ellipse.

``obj:findAllEllipse(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds all :doc:`Instances </class/instance>` inside of the specified ellipse.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      First x coordinates of the ellipse to check.
		``y1``           number                      First y coordinates of the ellipse to check.
		``x2``           number                      Second x coordinates of the ellipse to check.
		``y2``           number                      Second y coordinates of the ellipse to check.
		============     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table of :doc:`Instances </class/instance>` found within the ellipse.

``obj:findLine(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds a single :doc:`Instance </class/instance>` colliding with the specified line.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      The x coordinate of the first end of the line.
		``y1``           number                      The y coordinate of the first end of the line.
		``x2``           number                      The x coordinate of the second end of the line.
		``y2``           number                      The y coordinate of the second end of the line.
		============     =====================       ===========

	**Returns:**
	
		Returns the first :doc:`Instance </class/instance>` colliding with the line.

``obj:findAllLine(x1, y1, x2, y2)``
-----------------------------------------------------
	
	Finds all :doc:`Instances </class/instance>` colliding with the specified line.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x1``           number                      The x coordinate of the first end of the line.
		``y1``           number                      The y coordinate of the first end of the line.
		``x2``           number                      The x coordinate of the second end of the line.
		``y2``           number                      The y coordinate of the second end of the line.
		============     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table of :doc:`Instances </class/instance>` colliding with the line.
		
``obj:findPoint(x, y)``
-----------------------------------------------------
	
	Finds a single :doc:`Instance </class/instance>` colliding with the specified point.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x``            number                      The x coordinate of the point to check.
		``y``            number                      The y coordinate of the point to check.
		============     =====================       ===========

	**Returns:**
	
		Returns the first :doc:`Instance </class/instance>` colliding with the point.

``obj:findAllPoint(x, y)``
-----------------------------------------------------
	
	Finds all :doc:`Instances </class/instance>` colliding with the specified point.
	
	**Parameters:**
	
		============     =====================       ===========
		Parameter        Type                        Description
		============     =====================       ===========
		``x``            number                      The x coordinate of the point to check.
		``y``            number                      The y coordinate of the point to check.
		============     =====================       ===========

	**Returns:**
	
		Returns a numerically indexed table of :doc:`Instances </class/instance>` colliding with the point.
		

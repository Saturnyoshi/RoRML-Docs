**************
Survivor Class
**************

A ``Survivor`` is any one of the playable characters found in the game.

.. contents:: Table of Contents
   :local:

Static Methods
==============
   
``Survivor.new(name)``
-------------------------------
	
	Alias: ``Survivor(name)``

	Creates a new survivor.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the survivor within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Survivor object.
	
	**Example:**
		::
			
			local cyborg = Survivor.new("Cyborg")
			
		Creates a new survivor called ``"Cyborg"``.
		
``Survivor.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Survivor.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Survivor.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Survivors in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Fields
======

=================    =======    ===========
Field                Type       Description
=================    =======    ===========
``displayName``      string     The name of the survivor as displayed in-game.
``titleSprite``      Sprite     The sprite displayed on the title screen when the survivor was previously selected, this will typically be the same as the survivor's walk animation.
``idleSprite``       Sprite     | The survivor's idle sprite.
                                | Used by the tab menu and character select palette in online co-op.
                                | Only appears in menus and is not used in-game.
``loadoutSprite``    Sprite     | The sprite that shows up in the character select screen. It must have at least 4 frames. The animation speed for the loadout sprite is always 0.2, meaning it animates at 12 frames per second.
                                | The first frame is for when the character is yet to be unlocked.
                                | The second frame is the unselected sprite.
                                | The third frame is for when the character is hovered over.
                                | The fourth frame and onward make up the animation that plays when the character is selected, which stops on the last frame.
``loadoutColor``     Color      (Alias: ``loadoutColour``) The color of the survivor's skill names in the character select screen.
``loadoutWide``      boolean    When set to true the survivor will take up 2 slots on the character select screen, like HAN-D does. 
``endingQuote``      string     The message displayed when the game is beaten as this character. Defaults to ``"..and so they left, with everything but their humanity."``
``disabled``         boolean    When set to true the survivor will be hidden from all menus and be unable to be selected by the player.
=================    =======    ===========

Methods
=======

``survivor:addCallback(callback, bind)``
----------------------------------------
	
	Binds a function to be called when a specific callback is fired.
	
	The arguments passed to the function vary by callback, but the first argument is always the player.
	
	**Parameters:**
	
		============     ========      ===========
		Parameter        Type          Description
		============     ========      ===========
		``callback``     string        The name of the callback to bind a function to, see below for a list of callback names.
		``bind``         function      The function to be run when the callback is fired. The function is always passed the relevant :doc:`PlayerInstance </class/playerInstance>`.
		============     ========      ===========
		
	**Callbacks:**
		
		============    ===========
		Name            Description
		============    ===========
		``init``        Fired when the player first spawns. Note that this is executed before the first level is fully generated.
		``step``        Fired every step.
		``draw``        | Fired whenever the player is drawn.
		                | The second and third arguments are the X and Y coordinates the player is being drawn at.
		``useSkill``    | Fired when the player attempts to use any skill which is off cooldown. 
		                | Provides the skill index as the second argument.
		``onSkill``     | Fired once each step the player is using a skill. 
		                | Provides the skill index as the second argument.
		                | The third argument is the frame of the animation when it's incremented but is otherwise 0.
		``levelUp``     Fired when the player levels up.
		``scepter``     Fired when the player picks up Ancient Scepter.
		============    ===========
	
	**Example:**
		::
			
			commando:addCallback("levelUp", function(player)
			    player:set("pHmax", player:get("pHmax") + 0.1)
			end)
			
		Increases the player's running speed when they level up.
		
``lookup:setLoadoutInfo(description, skills)``
----------------------------------------------
	
	Sets the description of the survivor on the character select screen.
	
	**Parameters:**
	
		===============     ======      ===========
		Parameter           Type        Description
		===============     ======      ===========
		``description``     string      A short description of the character's play style and functionality. Supports :doc:`colored text formatting </misc/coloredTextFormatting>`. Note that line breaks must be put in manually. There's space for 6 lines vertically.
		``skills``          Sprite      A sprite containing 4 frames which will be displayed as the character's skill icons in the character select screen.
		===============     ======      ===========
	
	**Example:**
		::
			
			commando:setLoadoutInfo("The &y&Commando&!& is characterized by long range and mobility.\n" ..
			                        "Effective use of his &y&Tactical Dive&!& will grant increased survivability,\n" ..
			                        "while &y&suppressive fire&!& deals massive damage.\n" .. 
			                        "&y&FMJ&!& can then be used to dispose of large mobs.", skillsprite)
			
		Sets the description and skill icons for ``commando``.
		
``lookup:setLoadoutSkill(index, name, description)``
----------------------------------------------------
	
	Sets the description and name of one of the survivor's skills on the character select screen.
	
	**Parameters:**
	
		===============     ======      ===========
		Parameter           Type        Description
		===============     ======      ===========
		``index``           number      The number of the skill to set the information of, from 1 to 4.
		``name``            string      The name of the skill.
		``description``     string      A short description of the skill's functionality. Supports :doc:`colored text formatting </misc/coloredTextFormatting>`. Note that line breaks must be put in manually. There's space for 2 lines vertically, except on the 4th skill.
		===============     ======      ===========
	
	**Example:**
		::
			
			commando:setLoadoutSkill(3, "Tactical Dive",
			                         "&y&Roll forward&!& a small distance.\n" ..
			                         "You &b&cannot&!& be hit while rolling.")
			
		Sets the description and name of ``commando``'s third skill: Tactical Dive.
		
``survivor:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the Survivor.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``survivor:getName()``
-------------------------------
	
	Returns the name of the Survivor within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
*****************
Difficulty Class
*****************

A ``Difficulty`` is a setting you can pick before starting a new game.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Difficulty.new(name)``
-------------------------------
	
	Alias: ``Difficulty(name)``

	Creates a new difficulty.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the difficulty within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Difficulty object.
	
	**Example:**
		::
			
			local droughtDiff = Difficulty.new("Drought")
			
		Creates a new difficulty called ``"Drought"``.

.. _difficulty-getScaling:

``Difficulty.getScaling(kind)``
-------------------------------
	
	Returns one of the game's different stat multipliers described below. These are used to increase difficulty as time goes on.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``kind``        string      The stat to get the multiplier for.
		===========     ======      ===========

	**Types of stats available:**
	
		===========     ======================
		Stats           Multiplier description
		===========     ======================
		``hp``          Used to scale actors' base HP over time.
		``damage``      Used to scale actors' base damage over time.
		``cost``        Used to increase chests and other :doc:`interactables' </class/interactable>` cost over time.
		===========     ======================

	**Returns:**
	
		Number with the multiplier used for the given stat.
		
``Difficulty.getActive()``
----------------------------------------
	
	Gets the currently active difficulty.

	**Returns:**
	
		The current difficulty.
		
``Difficulty.setActive(diff)``
----------------------------------------
	
	Sets the game's current difficulty to the given one.
	
	**Parameters:**
	
		===========     ==========      ===========
		Parameter       Type            Description
		===========     ==========      ===========
		``diff``        Difficulty      The difficulty to set as active.
		===========     ==========      ===========

	**Example:**
		::
			
			Difficulty.setActive(droughtDiff)
			
		Sets the game's difficulty to ``droughtDiff``.

``Difficulty.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Difficulty.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Difficulty.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Difficulties in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
Fields
======

============================    ==================    ===========
Field                           Type                  Description
============================    ==================    ===========
``displayName``                 string                The difficulty's display name.
``icon``                        Sprite                The icon sprite used by the difficulty.
``scale``                       number                The scaling used by the difficulty, to get harder over time. This is applied every minute.
``scaleOnline``                 number                Same as ``scale``, only for online use.
``description``                 string                The description of the difficulty, displayed when hovered over on survivor selection screen.
``enableMissileIndicators``     boolean               If ``true``, missiles will have a drawn circle over their target.
``forceHardElites``             boolean               If ``true``, forces elites to spawn.
``enableBlightedEnemies``       boolean               If ``true``, blighted enemies will be able to spawn.
============================    ==================    ===========


Methods
=======

``difficulty:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the difficulty.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``difficulty:getName()``
-------------------------------
	
	Returns the name of the difficulty.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
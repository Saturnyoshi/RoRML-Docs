********************
PlayerInstance Class
********************

The ``PlayerInstance`` class is a subclass of the ``ActorInstance`` class used to manipulate players.

PlayerInstance inherits all functionality from :doc:`ActorInstance </class/actorInstance>` and :doc:`Instance </class/instance>`.

.. contents:: Table of Contents
   :local:
   
Fields
======

===============    =======    ===========
Field              Type       Description
===============    =======    ===========
``playerIndex``    number     (Read only) The slot of the player.
``useItem``        Item       The player's current use item.
===============    =======    ===========

Methods (Misc)
==============

``player:getSurvivor()``
-------------------------------
		
	**Returns:**
	
		Returns the :doc:`Survivor </class/survivor>` object of the player's selected character.
	
	**Example:**
		::
			
			print(player:getSurvivor().displayName)
			
		Prints the display name of ``player``'s selected character.

.. _player-instance-control:

``player:control(name)``
-------------------------------
	
	Used to check the press status of one of the player's inputs.
	
	Functionally the same as :ref:`input.checkControl <input-check-control>`.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``control``     string      The control to check. Case insensitive.
		===========     ======      ===========

		The following is a list of all control names:
		
		========    ========    ========
		left        right       up
		down        jump        ability1
		ability2    ability3    ability4
		use         enter       swap
		========    ========    ========
		
	**Returns:**
	
		Returns the press status of the control. See the :ref:`input module <keypressstatus>` page for press statuses.
	
	**Example:**
		::
			
			if player:control("left") == input.HELD then
			    -- Do something
			end
			
		Does something as long as ``player`` is holding their left key.

Methods (Items)
===============
		
``player:activateUseItem([noCooldown], [item])``
------------------------------------------------
	
	Activates either the player's use item or a specified usable :doc:`Item </class/item>`.
	
	Optionally the use cooldown will not be applied to the player.
	
	
	**Parameters:**
	
		==============     =======      ===========
		Parameter          Type         Description
		==============     =======      ===========
		``noCooldown``     boolean      *Optional:* When set to true the use item's cooldown will not be activated. Defaults to false.
		``item``           Item         *Optional:* The item to activate. Defaults to the player's use item.
		==============     =======      ===========
		
``player:giveItem(item, [count])``
----------------------------------
	
	Gives the player a certain amount of a specified :doc:`Item </class/item>`.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to give the player.
		``count``       number      *Optional:* The amount of the item to give the player. Defaults to 1.
		===========     ======      ===========
	
	**Example:**
		::
			
			player:giveItem(infusion, 3)
			
		Gives ``player`` 3 of the item ``infusion``
		
``player:countItem(item)``
-------------------------------
	
	Used to get the player's current stack of a specific :doc:`Item </class/item>`.
	
	Useful for simple item effects.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to get the stack of.
		===========     ======      ===========

	**Returns:**
	
		The stack of the specified item.
	
	**Example:**
		::
			
			if player:countItem(infusion) >= 3 then
			    -- Do something
			end
			
		Does something if ``player`` has at least 3 of the item ``infusion``.
		
``player:removeItem(item, [count])``
------------------------------------
	
	Removes or takes from the stack of an :doc:`Item </class/item>` on the player's HUD.
	
	Keep in mind that this function does not actually reverse the item's effects and that needs to be handled manually.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to remove from the HUD.
		``count``       number      *Optional:* The stack to take. Defaults to 1.
		===========     ======      ===========

	**Example:**
		::
			
			if player:countItem(infusion) >= 1 then
			    -- Remove the item from the player's HUD
			    player:removeItem(infusion)
			    -- Subtract from the player's "hp_after_kill" variable to remove the effect
			    player:set("hp_after_kill", player:get("hp_after_kill") - 1)
			end
			
		Checks if ``player`` has any of the item ``infusion`` and if they do takes one away.
		
		The ``removeItem`` method doesn't remove the effects itself, so the player's ``hp_after_kill`` variable is subtracted to reverse the effects of the item.
		
``player:setItemSprite(item, sprite)``
--------------------------------------
	
	| Sets the sprite of an :doc:`Item </class/item>` on the player's HUD.
	| If the player does not already have the item, this method will do nothing.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to set the sprite of.
		``sprite``      Sprite      The new sprite for the item to use.
		===========     ======      ===========
	
``player:setItemText(item, text)``
----------------------------------
	
	| Sets the text which is displayed when the cursor is hovered over an :doc:`Item </class/item>` on the player's HUD.
	| If the player does not already have the item, this method will do nothing.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``item``        Item        The item to set the sprite of.
		``text``        string      The new text for the item to use.
		===========     ======      ===========

	**Example:**
		::
			
			player:setItemSprite(someItem, itemCharging)
			player:setItemText(someItem, "This item is recharging.")
			
		Sets both the sprite and text of the item ``someItem`` on the player's HUD.
	
Methods (Custom Survivors)
==========================
		
``player:survivorSetInitialStats(health, damage, regen)``
---------------------------------------------------------
	
	Used to set the starting stats of custom survivors.
	
	Automatically adjusts values for the glass artifact and for extra health and regen in Drizzle mode.
	
	Called in the :doc:`Survivor's </class/survivor>` ``"init"`` callback.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``health``      number      The value to set the player's health to.
		``damage``      number      The value to set the player's damage to.
		``regen``       number      The value to set the player's health regeneration to.
		===========     ======      ===========
	
	**Example:**
		::
			
			player:survivorSetInitialStats(110, 12, 0.01)
			
		Sets the player's health, damage, and regen stats to 110, 12, and 0.01 respectively, matching the Commando.
		
``player:survivorLevelUpStats(health, damage, regen, armor)``
-------------------------------------------------------------
	
	Used to add to the player's stats when they level up.
	
	Automatically adjusts values for the glass artifact.
	
	Called in the :doc:`Survivor's </class/survivor>` ``"levelUp"`` callback.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``health``      number      The value to increase the player's health by.
		``damage``      number      The value to increase the player's damage by.
		``regen``       number      The value to increase the player's health regeneration by.
		``armor``       number      The value to increase the player's armor by.
		===========     ======      ===========
	
	**Example:**
		::
			
			player:survivorLevelUpStats(32, 3, 0.002, 2)
			
		Increases the player's stats, matching the level up stats of the Commando.

``player:survivorActivityState(index, sprite, speed, scaleSpeed, resetHSpeed)``
-------------------------------------------------------------------------------
	
	Used to make custom skills, putting the player into a state which automatically ends when the animation is finished.
	
	Called in the :doc:`Survivor's </class/survivor>` ``"useSkill"`` callback.
	
	**Parameters:**
	
		===============     =======      ===========
		Parameter           Type         Description
		===============     =======      ===========
		``index``           number       The activity state ID. Any number more than or equal to 1 and less than 5.
		``sprite``          Sprite       The player's animation used in the attack.
		``speed``           number       The animation speed of the sprite. Usually around 0.25 for 15 frames per second.
		``scaleSpeed``      boolean      | Whether to multiply the animation speed by the player's attack speed.
		                                 | Most attacks use this but some abilities like the Commando's roll do not.
		``resetHSpeed``     boolean      | Whether to automatically set the player's movement speed to 0 when they're on the ground.
		                                 | Used by almost any ability where the player is unable to control the character during the animation.
		===============     =======      ===========
		
``player:survivorFireHeavenCracker([damage])``
----------------------------------------------
	
	Used to control when the Heaven Cracker item is triggered.
	
	Fires a bullet after being called a specific number of times, provided the player has the Heaven Cracker item.
	The number of calls required to fire a bullet is 5 minus the item's stack.
	
	If the player doesn't have the Heaven Cracker item then this method always returns nil.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``damage``      number      *Optional:* The damage multiplier of a successful attack. Defaults to 1.
		===========     ======      ===========

	**Returns:**
	
		Returns the fired bullet when successful, otherwise nil.
	
		
``player:activateSkillCooldown(index)``
---------------------------------------
	
	Activates one of the player's skill cooldowns as if the ability was just used.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``index``       number      The index of the skill to put on cooldown, 1 to 4 inclusive.
		===========     ======      ===========
	
	**Example:**
		::
			
			for i = 1, 4 do
				player:activateSkillCooldown(i)
			end
			
		Puts all 4 of ``player``'s skills on cooldown simultaneously.
		
``player:setSkill(index, name, desc, sprite, subimage, cooldown)``
-------------------------------------------------------------------
	
	Used to set all the information about a skill, including its name, description, icon, and cooldown.
	
	Usually only used when creating a custom :doc:`Survivor </class/survivor>`, but may be used for any purpose outside of that.
	
	**Parameters:**
	
		============     ======      ===========
		Parameter        Type        Description
		============     ======      ===========
		``index``        number      The index of the skill to set the information of, 1 to 4 inclusive.
		``name``         string      The name to give the skill.
		``desc``         string      The description to give the skill. Visible when the mouse is hovered over the skill on the HUD.
		``sprite``       Sprite      The icon sprite to give the skill. Each icon is 18x18 with the origin placed at the top left of the sprite.
		``subimage``     number      The subimage of the icon sprite to use.
		``cooldown``     number      The cooldown of the skill, in frames.
		============     ======      ===========
	
	**Example:**
		::
			
			player:setSkill(2,
			    "Full Metal Jacket",
			    "Shoot a bullet that passes through enemies for 230% damage, knocking them back.",
			    Sprite.find("GManSkills", "Vanilla"), 1,
			    3 * 60
			)
			
		Sets the information for ``player``'s second ability to match the Commando's second ability.
		
``player:setSkillIcon(index, sprite, subimage)``
------------------------------------------------
	
	Used to set the icon of a skill.
	
	A stripped down version of the main ``setSkill`` method.
	Useful for changing the icon of a skill to reflect its state, like the Mercenary's multiple third skill uses, or the Engineer's rechargable mines.
	
	**Parameters:**
	
		============     ======      ===========
		Parameter        Type        Description
		============     ======      ===========
		``index``        number      The index of the skill to set the information of, 1 to 4 inclusive.
		``sprite``       Sprite      The new icon sprite to give the skill. Each icon is 18x18 with the origin placed at the top left of the sprite.
		``subimage``     number      The new subimage of the icon sprite to use.
		============     ======      ===========
	
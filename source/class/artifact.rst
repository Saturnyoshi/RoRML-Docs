**************
Artifact Class
**************

An ``Artifact`` is a togglable gameplay modifier which can have any range of effects, from simplifying the game to making it harder.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``Artifact.new(name)``
-------------------------------
	
	Alias: ``Artifact(name)``

	Creates a new artifact.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the artifact within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Artifact object.
	
	**Example:**
		::
			
			local someArtifact = Artifact.new("Something")
			
		Creates a new artifact called ``"Something"``.
	
``Artifact.find(name, [namespace])``
-------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Artifact.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Artifact.findAll(namespace)``
---------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Artifacts in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

``Artifact.fromObject(object)``
-------------------------------

	Searches for an artifact which uses the specified doc:`GMObject </class/gmObject>`.
	
	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``object``      GMObject      The object to use to find the artifact.
		===========     ========      ===========

	**Returns:**
	
		Returns the artifact if found.
	
Fields
======

=================    ===============    ===========
Field                Type               Description
=================    ===============    ===========
``displayName``      string             The name of the artifact as displayed in-game.
``active``           boolean            Whether the artifact is enabled. This can be modified at any time, but will be reset to false at the end of the run if the artifact is not unlocked.
``unlocked``         boolean            Whether the artifact is unlocked and selectable. Saved to the save file. Set to true when a player picks up the artifact.
``loadoutSprite``    Sprite             The sprite of the artifact in the character select menu. Usually a 2 frame image with the origin at the center. The first frame of the sprite is the icon in solid white. The second frame is used when the artifact is enabled.
``loadoutText``      string             The description of the artifact in the character select screen. A short description of the artifact's effect.
``pickupName``       string             The name displayed when the artifact is picked up. Usually the item's ``displayName`` with ``"Artifact of "`` added to the start. Automatically updated when ``displayName`` is changed.
``pickupSprite``     Sprite             The sprite of the artifact's pickup object.
``disabled``         boolean            When set to true the artifact will be hidden from all menus and be unable to be toggled by the player.
=================    ===============    ===========


Methods
=======

``artifact:getObject()``
------------------------

	Returns the :doc:`GMObject </class/gmObject>` used as the artifact's pickup.

	**Returns:**
	
		Always returns a :doc:`GMObject </class/gmObject>`.
	
	**Example:**
		::
			
			someArtifact:getObject():create(xpos, ypos)
			
		Creates an instance of the artifact ``someArtifact`` at a specific position.

``artifact:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the artifact.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``artifact:getName()``
-------------------------------
	
	Returns the name of the artifact.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
******************
ParentObject Class
******************

A ``ParentObject`` represents a specific group of :doc:`GMObjects </class/gmObject>`.

ParentObject inherits all functionality from :doc:`GMObjectBase </class/gmObjectBase>`.

.. contents:: Table of Contents
   :local:
   
Static Methods
==============

``ParentObject.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing ParentObject.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``ParentObject.findAll(namespace)``
-----------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing ParentObjects in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.

	
List of ParentObjects
=====================

Below is a complete list of all ``ParentObjects``.

==============    ===========
Name              Description
==============    ===========
actors            All actors, including players, enemies, drones, etc.
enemies           All enemies and bosses.
classicEnemies    All standard physics affected enemies.
flyingEnemies     All flying enemies.
bosses            All bosses.
drones            All drones.
mapObjects        All chests, shrines, etc.
droneItems        All drone map objects.
chests            All chest map objects.
items             All items.
artifacts         All artifact pickups.
commandCrates     All crates from the artifact of command.
==============    ===========

Methods
=======

``obj:getOrigin()``
-----------------------------------
	
	| Returns the namespace containing the ParentObject.
	| As custom parents are not supported this will always return ``"vanilla"``.	

	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``obj:getName()``
-------------------------------
	
	Returns the name of the ParentObject within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
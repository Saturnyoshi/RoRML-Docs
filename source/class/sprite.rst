************
Sprite Class
************

A ``Sprite`` is any image which can be rendered to the screen but is not modified during runtime.

Sprite inherits all functionality from :doc:`BaseSprite </class/baseSprite>`.

.. contents:: Table of Contents
   :local:


Static Methods
==============
		
``Sprite.load([name], fname, frames, xorigin, yorigin)``
--------------------------------------------------------------
	
	Alias: ``Sprite([name], fname, frames, xorigin, yorigin)``

	Loads a sprite from the path ``fname`` relative to the mod's directory. Only ``.png`` files are supported.

	If a name is not provided then the filename will be used.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      *Optional:* The name to give the sprite within the current namespace.
		``fname``       string      This is the path to the file, relative to the mod's base path. The file extension is optional.
		``frames``      number      The amount of frames in the sprite. When the sprite is imported into the game, the PNG image will be cut into this many equally sized vertical slices.
		``xorigin``     number      The X position of the sprite's origin. When a sprite is being drawn to the screen, this is the point that will end up at the specified coordinates.
		``yorigin``     number      The Y position of the sprite's origin.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new Sprite object.
	
	**Example:**
		::
			
			ls = Sprite.load("Light Switch", "sprites/light switch.png", 2, 12, 14)
			
		This code loads the sprite called ``"Light Switch"``. It has two frames and its origin is at (12, 14).

``Sprite.find(name, [namespace])``
------------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing Sprite.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``Sprite.findAll(namespace)``
--------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing Sprites in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
		
``Sprite.fromID(id)``
--------------------------------------------------------------
	
	Used to fetch a Sprite object from its GameMaker resource ID.

	Useful for getting sprites from instance variables which contain a sprite ID. 
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``id``          number      The GameMaker ID of the sprite.
		===========     ======      ===========

	**Returns:**
	
		Returns the Sprite if found, nil otherwise.
	
Fields
======

===============    ========    ===========
Field              Type        Description
===============    ========    ===========
``id``             number      (Alias: ``ID``) The sprite's GameMaker resource ID.
===============    ========    ===========

Methods
=======

``sprite:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the Sprite.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``sprite:getName()``
-------------------------------
	
	Returns the name of the Sprite within its parent namespace.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
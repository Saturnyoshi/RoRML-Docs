***************
EliteType Class
***************

An ``EliteType`` is a kind of eltie enemy that can spawn.

.. contents:: Table of Contents
   :local:

Static Methods
==============

``EliteType.new(name)``
-------------------------------
	
	Alias: ``EliteType(name)``

	Creates a new elite type.

	Note that this elite type will not show up in-game until it is manually added to an enemy's elite list.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name to give the elite within the current namespace.
		===========     ======      ===========

	**Returns:**
	
		Always returns a new EliteType object.

``EliteType.find(name, [namespace])``
----------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find an existing elite type.
	
	See the page on :ref:`namespace searching <context-find>` for more information.
	
``EliteType.findAll(namespace)``
------------------------------------
	
	Executes a :doc:`namespace search </misc/contextSearch>` to find all existing elite types in a specific namespace.
	
	See the page on :ref:`namespace searching <context-find-all>` for more information.
	
``EliteType.refreshPalettes()``
---------------------------------

	Forces all elite palettes to be refreshed.
	
	Only needed if creating a new elite type after loading is finished.

``EliteType.registerPalette(palette, object)``
-------------------------------------------------
	
	Used to setup an elite palette for a new enemy. 
	
	The sprite will automatically be overwritten to include all other elite palettes.
	
	The sprite should be a single frame 1x6 pixel image containing the enemy's source colors which will be mapped to the elite palette's colors like seen below.

	**Parameters:**
	
		===========     ========      ===========
		Parameter       Type          Description
		===========     ========      ===========
		``palette``     Sprite        The new palette being set up. Should be a single frame 1x6 pixel sprite like seen below.
		``object``      GMObject      The enemy the palette is being assigned to. The palette must still be manually set as the enemy's ``"palette"`` sprite on spawn.
		===========     ========      ===========

Fields
======

============================    ==================    ===========
Field                           Type                  Description
============================    ==================    ===========
``displayName``                 string                The name of the elite, like ``"frenzied"``, or ``"overloading"``.
``color``                       Color                 (Alias: ``Cclour``) The primary color of the elite, used to tint the enemy when the elite palette is not available.
``colorHard``                   Color                 (Alias: ``colourHard``) The color of the elite when hard elites are active. You can usually ignore this.
``palette``                     Sprite                A single frame 1x6 pixel sprite containing the palette used for recoloring enemies of this elite. Vanilla palettes are visible below upscaled for reference.
============================    ==================    ===========

	.. image:: /images/elitepalettes.png   

Methods
=======

``eliteType:getOrigin()``
-----------------------------------
	
	Returns the namespace containing the elite type.
	
	See the page on :ref:`namespace searching <context-origin>` for more information.
	
``eliteType:getName()``
-------------------------------
	
	Returns the name of the elite type.
	
	See the page on :ref:`namespace searching <context-name>` for more information.
	
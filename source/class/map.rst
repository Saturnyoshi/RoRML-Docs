*****************
Map<T1, T2> Class
*****************

A ``Map<T1, T2>`` is a collection that maps several unique ``T1`` keys to ``T2`` elements. Maps cannot hold more than one type of key and/or element.
To get a map element, you can treat it as a regular Lua table:

	::
			
		local elem = map["key"]
		
	Stores the ``T`` element associated with the string key ``"key"`` in the Map<string, T> ``map`` to the variable ``elem``, or ``nil`` if there's no element.

To add, change or remove map elements, you can also use this notation:

	::
			
		map["lettuce"] = "green"
		map["cringe"] = nil
		
	Stores the element ``"green"`` with the key ``"lettuce"`` in the Map<string, string> ``map`` (replacing any already existing element),
	and removes any element associated with the key ``"cringe"``.

Maps cannot be instanciated; they are instead used by some classes, such as :doc:`Stages </class/stage>`'s interactables' rarity.

.. contents:: Table of Contents
   :local:

Methods
=======

``map:toTable()``
----------------------------------
	
	Creates a new table containing every value in the map, stored with their specific keys.
	
	**Returns:**
	
		A new table that is an exact copy of the map.
	
	**Example:**
		::
			
			local stage = Stage.getCurrentStage()
			local rarityTable = stage.interactableRarity:toTable()
			
		Creates a new table containing every interactable spawned by the stage and their respective rarities, and stores it in ``rarityTable``.
	
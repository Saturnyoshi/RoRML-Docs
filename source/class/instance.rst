**************
Instance Class
**************

An ``Instance`` is anything in the game world which can be interacted with or interacts with other instances.

Instances can be created and accessed via their respective :doc:`GMObject </class/gmObject>`.

There are multiple subclasses to the Instance class. Their differences are documented at the bottom of this page.

.. note:: Attempting to access instances which have already been destroyed will result in an error; the ``isValid`` method can be used to ensure an instance still exists before accessing it.

.. warning:: Attempting to access an instance during game load is prohibited and will result in an error.

.. contents:: Table of Contents
   :local:

Fields
======

===============    =======    ===========
Field              Type       Description
===============    =======    ===========
``x``              number     The horizontal position of the instance in the room.
``y``              number     The vertical position of the instance in the room.
``id``             number     (Alias: ``ID``) The internal ID of the instance.
``sprite``         Sprite     The sprite the instance is currently displaying.
``spriteSpeed``    number     | The speed the instance's sprite will be played at.
                              | A value of 1 is 60 FPS, 0.5 is 30, etc.
``subimage``       number     The current displayed frame of the instance's sprite. Starts at 1.
``mask``           Sprite     | The sprite used by the instance for collision checking.
                              | Appearance modifying fields (like ``xscale`` or ``angle``) also apply to the mask.
``blendColor``     Color      (Alias: ``blendColour``) Value the sprite's colour is multiplied by when rendering.
``xscale``         number     Multiplier for the sprite's horiontal size.
``yscale``         number     Multiplier for the sprite's vertical size.
``angle``          number     Rotation of the sprite in degrees.
``alpha``          number     The transparency of the sprite, from 1 to 0.
``depth``          number     | Controls what the instance will be rendered in front or behind of.
                              | Instances are rendered from highest to lowest depth.
``visible``        boolean    When set to false the instance's rendering will be disabled.
===============    =======    ===========

Methods (General)
=================

``instance:isValid()``
-------------------------------
	
	Checks whether the instance still exists.

	**Returns:**
	
		Returns true if the instance exists, false if it doesn't.
	
	**Example:**
		::
			
			if eggplantinst:isValid() then
			    -- Do something
			end
			
		Does something if eggplantinst still exists.
		
``instance:destroy()``
-------------------------------
	
	Destroys the instance.
	
	**Example:**
		::
			
			peach:destroy()
			
		This destroys the peach instance.
		
``instance:delete()``
-------------------------------
	
	Destroys the instance, but doesn't trigger the ``destroy`` event.
	
	.. warning:: Using this too frivolously can create issues, especially with objects that clean up after themselves in the ``destroy`` event.
	
	**Example:**
		::
			
			peach:delete()
			
		This destroys the peach instance, but doesn't trigger its ``destroy`` event.
		
``instance:getObject()``
-------------------------------
	
	Gets the :doc:`GMObject </class/gmObject>` that the instance is a type of.
	
	**Returns:**
	
		Returns a :doc:`GMObject </class/gmObject>` object.
	
	**Example:**
		::
			
			cherry:getObject():create(cherry.x, cherry.y)
			
		Creates a new cherry instance on top of the cherry instance already present.
	
``instance:collidesWith(other, x, y)``
--------------------------------------
	
	Checks whether the instance would collide with another instance, or any instance of a GMObject, at a position.
	
	The Game Maker documentation describes this as moving the object to a new position, checking if it collides with anything there, then moving it back.
	
	**Parameters:**
	
		===========     ====================      ===========
		Parameter       Type                      Description
		===========     ====================      ===========
		``other``       Instance or GMObject      The specific Instance or GMObject to check for collision with.
		``x``           number                    The horizontal coordinate of the position to check.
		``y``           number                    The vertical coordinate of the position to check.
		===========     ====================      ===========

	**Returns:**
	
		Returns true if there was a collision and false if not.
	
	**Example:**
		::
			
			if eggplant:collidesWith(othereggplant, eggplant.x, eggplant.y) then
			    -- Do something
			end
			
		This checks whether the eggplant instance is currently colliding with the other eggplant instance and does something.
		
		::
			
			if not eggplant:collidesWith(peachObj, eggplant.x, eggplant.y - 10) then
			    eggplant.y = eggplant.y - 10
			end
			
		This checks whether the eggplant instance would collide with a peach if it were to move 10 pixels upwards, and if it wouldn't, moves 10 pixels upwards.
		
``instance:collidesMap(x, y)``
-------------------------------
	
	Checks whether the instance would collide with solid ground at a position.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``x``           number      The horizontal coordinate of the position to check.
		``y``           number      The vertical coordinate of the position to check.
		===========     ======      ===========

	**Returns:**
	
		Returns true if there was a collision and false if not.
	
	**Example:**
		::
			
			if not leaf:collidesMap(leaf.x, leaf.y + 3) then
				-- Move down
			end
			
		This makes the leaf fall until it hits solid ground.
	
``instance:getNetIdentity()``
-------------------------------
	
	If this instance is used in networking by the vanilla game, this function can be used to easily identify it over the network.
	If the instance is not networked then this will throw an error instead.

	Networked instances include actors, map objects, and items.
	
	**Returns:**
	
		Returns a :doc:`NetInstance </class/netInstance>` representing the instance.
	
Methods (Variables)
===================

``instance:get(varName, ...)``
-------------------------------
	
	Directly gets a variable from the instance.

	Also supports variable argument count to get multiple values in a single call.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``varName``     string      The name of the instance variable to get.
		...             string      The names of any other variables to get.
		===========     ======      ===========

	**Returns:**
	
		Returns the value of the variable(s), as a string, number, or nil.
		
		.. note:: Since booleans are stored as numbers internally, a variable which is expected to have a ``true`` or ``false`` result will instead usually return a 0 or 1.
	
	**Examples:**
		::
			
			local speed = player:get("pHspeed")
			
		Gets the ``pHspeed`` of the instance ``player``.
		
		::
			
			local hspeed, vspeed = player:get("pHspeed", "pVspeed")
			
		Gets both the ``pHspeed`` and ``pVspeed`` of the instance ``player``.
	
``instance:set(varName, value)``
--------------------------------
	
	Directly sets a variable in the instance.
	
	**Parameters:**
	
		===========     =====================    ===========
		Parameter       Type                     Description
		===========     =====================    ===========
		``varName``     string                   The name of the instance variable to set.
		``value``       string, number or nil    The value to give the variable.
		===========     =====================    ===========
	
	**Returns:**

		Returns the calling instance to allow setters to be chained. 

	**Examples:**
		::
			
			player:set("pHspeed", 0)
			
		Sets the ``pHspeed`` of ``player`` to 0.

		::
			
			player:set("pHspeed", 0):set("pVspeed", 0)
			
		Uses the chainability of the method to set both the ``pHspeed`` and the ``pVspeed`` of ``player`` to 0.

``instance:getData()``
-------------------------------

	Used to get the instance's mod data table.

	This table can be used to store any arbitrary information, there are absolutely no limitations on what keys or values can be stored here.

	When storing custom information, it is preferable to use this over the instance's ``get`` and ``set`` methods for several reasons:
	
	* There are no limitations for what can be stored here. This includes nested tables, non-string keys, or even other Lua objects.
	* The ``get`` and ``set`` methods have significant overhead, while instance data has none. 
	* The table is also specific to each mod, so mods are less likely to conflict.

	**Returns:**
	
		Returns the mod-specific unique data table for the instance.
	
``instance:getModData(mod)``
-------------------------------

	Used to get the instance's data table from another mod.

	**Parameters:**
	
		===========     =====================    ===========
		Parameter       Type                     Description
		===========     =====================    ===========
		``mod``         string                   The internal name of the mod to get the data from.
		===========     =====================    ===========
	
	**Returns:**
	
		Returns the unique data table for the instance from a specific mod.
	
``instance:getAccessor()``
-------------------------------
	
	Gets the instance's associated ``InstanceAccessor`` object.
	
	The ``InstanceAccessor`` class is a special class; it does not have any methods or inherent fields but instead acts as a direct interface for getting and setting variables within instances.
	
	An ``InstanceAccessor`` can be used like a typical table, using any string as a key to get or set instance fields.

	**Returns:**
	
		Returns an ``InstanceAccessor`` object.
	
	**Example:**
		::
			
			local accessor = myInstance:getAccessor()
			accessor.timer = (accessor.timer or 0) + 1
			
		This example code increases the ``timer`` variable of ``myInstance`` by 1 each time it's ran.
	
.. _alarm-methods:
	
``getAlarm(index)``
-------------------------------

	.. note:: Alarms are only usable in built in Game Maker Objects. For modded objects you will have to use other methods.
	
	Gets the remaining frames until an alarm is triggered.
	
	Alarms in GameMaker are simple frame based timers. There are 12 of them for each instance and they are decremented every frame until they reach 0.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``index``       number      The index of the alarm. Between 0 and 11.
		===========     ======      ===========

	**Returns:**
	
		Returns the alarm's remaining time.
		
		A value of -1 means the alarm is not currently ticking.
	
	**Example:**
		::
			
			if inst:getAlarm(0) ~= -1 then
			    -- Do something
			end
			
		Does something if an instance's alarm is ticking.
		
``setAlarm(index, value)``
-------------------------------

	.. note:: Alarms are only usable in built in Game Maker Objects. For modded objects you will have to use other methods.
	
	Sets the remaining frames until an alarm is triggered.
	
	Alarms in GameMaker are simple frame based timers. There are 12 of them for each instance and they tick down one value every frame until they reach 0, at which point they're set to -1.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``index``       number      The index of the alarm. Between 0 and 11.
		``value``       number      The time to set the alarm to.
		===========     ======      ===========
	
	**Returns:**

		Returns the calling instance to allow setters to be chained. 

	**Example:**
		::
			
			instance:setAlarm(0, 30)
			
		Sets the instance's alarm 0 to half a second.

.. _instance-dumpvariables:

``instance:dumpVariables(dumpValues, advanced)``
------------------------------------------------

	| Writes a list of all the instance's variables to a file in the logs directory.
	| Useful both as a debug tool and to find undocumented variables that can be edited.
	
	Be careful to only call this method once when you need to, otherwise you'll quickly end up writing a lot to the log file.
	
	**Parameters:**
	
		==============     =======      ===========
		Parameter          Type         Description
		==============     =======      ===========
		``dumpValues``     boolean      *Optional:* When set to true the value of the variables will be written alongside the names. Defaults to false.
		``advanced``       boolean      | *Optional:* When set to true a handful of normally hidden variable names will be dumped too.
		                                | These include single letter long names, variables of unsupported types, and several hand-picked variables that aren't useful under normal circumstances.
										  Note there are some variables which are still hidden even with this option set to true, as you really should never touch those.
		==============     =======      ===========

	**Returns:**
	
		Returns true if there was a collision and false if not.
	
	
	
Subclasses
==========

There are multiple subclasses to the Instance class. They inherit all the methods and fields from the base class, but provide additional specific functionality depending on type.

ItemInstance
------------

An ``ItemInstance`` is any instance of an :doc:`Item </class/item>` object.

They have one additional method:

``instance:getItem()``
~~~~~~~~~~~~~~~~~~~~~~
	
	**Returns:**
	
		Returns the :doc:`Item </class/item>` which the instance represents.
	
	**Example:**
		::
			
			local item = instance:getItem()
			
		Gets the Item that the instance stored in variable ``instance`` was created from.
		
DamagerInstance
---------------

A ``DamagerInstance`` is any instance of a bullet or explosion.

They have two additional methods:

``instance:getParent()``
~~~~~~~~~~~~~~~~~~~~~~~~
	
	**Returns:**
	
		Returns the instance that created the damager.
		The value ``nil`` will be returned if the instance has no parent.
	
	**Example:**
		::
			
			local parent = explosion:getParent()
			
		Gets the instance which spawned the damager stored in ``explosion``.

``instance:isExplosion()``
~~~~~~~~~~~~~~~~~~~~~~~~~~

	**Returns:**
	
		Returns true if the instance is an explosion and false if it's a bullet.
		
	**Example:**
		::
			
			local direction
			if damager:isExplosion() then
			    direction = 90
			else
			    direction = damager:get("direction")
			end
			
		Sets the local variable ``direction`` to 90 if the instance in ``damager`` is an explosion, otherwise sets it to the bullet's direction.
		
ActorInstance
-------------

The ``ActorInstance`` class is used to manipulate instances of characters in the game world.

This class is advanced enough to warrant its own page; see :doc:`ActorInstance Class Reference </class/actorInstance>`.

PlayerInstance
--------------

The ``PlayerInstance`` class is a subclass of the ``ActorInstance`` class used to manipulate players.

This class is also advanced enough to warrant its own page; see :doc:`PlayerInstance Class Reference </class/playerInstance>`.

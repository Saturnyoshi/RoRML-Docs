***************
Vanilla Objects
***************

| This page contains lists of the built-in :doc:`GMObjects </class/gmObject>`.
| This list is not complete, but it does contain all useful objects.

| For finding the variables which instances of a specific object use, start with the :ref:`Instance:dumpVariables <instance-dumpvariables>` method.
| Documented variables for a few object categories can be found on :doc:`this page </misc/variables>`.

.. contents:: Table of Contents
   :local:

Actors
======

Allies
------
	
	============    ====
	Name            Description
	============    ====
	P               The player
	POI             | Enemy target. 1 exists for each player, the ID of which is stored in the player's ``child_poi`` variable.
	                | Removing a player's POI will stop enemies from targetting them.
	Drone1          The basic drone type
	Drone2          Attack drone
	Drone3          Missile drone
	Drone4          Healing drone
	Drone5          Laser drone
	Drone6          Flame drone
	Drone7          Medical drone
	DroneDisp       Temporary drone spawned by The Back-Up.
	ImpFriend       Imp spawned by Imp Overlord's Tentacle.
	Sucker          Creature spawned by Filial Imprinting.
	============    ====

Enemies
-------

	============    ====
	Name            Description
	============    ====
	Lizard          Lemurian
	LizardG         Elder Lemurian
	LizardF         Flying Evolved Lemurian
	LizardFG        Grounded Evolved Lemurian
	Jelly           Jellyfish
	JellyG2         Young Vagrant
	Golem           Stone Golem
	GolemS          Snow Golem
	Crab            Sand Crab
	Naut            Whorl
	Wisp            Small yellow Wisp
	WispG           Greater Wisp
	WispG2          Archaic Wisp
	Mush            Mushrum
	Spitter         spitter
	Child           Child
	ChildG          Parent
	Imp             Normal Imp
	ImpS            Purple Imp
	ImpM            Baby Imp
	Clay            Clay Man
	Bison           Bighorn Bison
	Spider          Mechanical Spider
	Slime           Gup
	Bug             Archer Bug
	Guard           Temple Guard
	GuardG          Sanctuary Guard
	BoarM           Boarlit
	BoarMS          Armored Boarlit
	============    ====
	
Bosses
------
	
	==============    ====
	Name              Description
	==============    ====
	GolemG            Colossus
	GiantJelly        Wandering Vagrant
	JellyLegs         Wandering Vagrant's tentacles
	Worm              Magma Worm controller
	WormHead          Magma Worm head
	WormBody          Magma Worm body
	WormWarning       Indicator of where the Magma Worm is going to come out.
	WispB             Ancient Wisp
	Boar              Toxic Beast
	BoarCorpse        Dead Toxic Beast
	ImpG              Imp Overlord
	ImpGS             Imp Vanguard
	Turtle            Cremator
	TurtleShell       Cremator's shell hitbox
	TurtleWave        Lava waves created by the Cremator
	TurtleCorpse      Dead Cremator
	Ifrit             Ifrit
	Scavenger         Scavenger
	LizardGS          Direseeker
	Acrid             Boss Acrid
	Boss1             Providence phase 1
	Boss3Fake         Speaking Providence
	Boss3             Providence final phase
	Boss2Clone        Umbra of Providence
	WurmController    Gilded Wurm controller
	WurmHead          Gilded Wurm Head
	WurmBody          Gilded Wurm Body
	==============    ====

Map Objects
===========

	==============    ====
	Name              Description
	==============    ====
	Chest1            Small chest
	Chest2            Large chest
	Chest3            3 item shop
	Chest4            Roulette shop
	Chest5            Golden chest
	Barrel1           Small canister
	Barrel2           Large canister
	Shrine1           Golem shrine
	Shrine2           Health shrine
	Shrine3           Imp shrine
	Shrine4           Rock shrine
	Shrine5           Percent health shrine
	Drone1Item        Broken basic drone
	Drone2Item        Broken attack drone
	Drone3Item        Broken missile drone
	Drone4Item        Broken healing drone
	Drone5Item        Broken laser drone
	Drone6Item        Broken flame drone
	Drone7Item        Broken medical drone
	Teleporter        The teleporter
	TeleporterFake    Level entry teleporter
	Base              Crashed pod at the beginning of the first stage
	Geyser            Jump pad
	==============    ====

Effects
=======

Pickups
-------

	==============    ====
	Name              Description
	==============    ====
	EfExp             Collectable experience
	EfGold            Collectable gold
	EfHeal            Infusion health orb
	EfHeal2           Healing orb
	SuckerPacket      Buffing pickup created by Filial Imprinting
	EfDeskPlant       Plants created by Interstellar Desk Plant
	==============    ====

Item effects
------------

	====================    ====
	Name                    Description
	====================    ====
	EfBrain                 Rotten Brain object
	EfBomb                  Gold Bomb object
	EfLantern               Safeguard Lantern object
	EfSawmerang             Sawmerang object
	EfThqwib                Thqwibs object
	Home                    Carrara Marble gate
	EfChestRain             Captain's Brooch object
	EfDecoy                 Crudely Drawn Buddy object
	EfTNTStick              Dynamite Plunger object
	EfBubbleShield          Shield Generator object
	EfBlizzard              Snowglobe effect, randomly freezes enemies and creates snow particles.
	EfMeteorShower          Glowing Meteorite effect, darkens the screen and randomly spawns meteors on actors.
	EfSmite                 Glowing Meteorite meteor
	EfWarbanner             Warbanner object
	EfMissile               Missiles used by AtG Missile Mk. 1 and 2.
	EfMissileSmall          Missiles fired by missile drones.
	EfMissileMagic          Ceremonial Dagger object
	EfMissileBox            Projectile spawner used by Disposable Missile Launcher, AtG Missile Mk. 2, and Nematocyst Nozzle.
	JellyMissileFriendly    Allied Wandering Vagrant missiles used by Nematocyst Nozzle.
	EfMine                  Panic Mine object
	EfPoisonMine            Dead Man's Foot object
	EfPoison2               Dead Man's Foot damage over time
	EfMortar                Mortar Tube projectile
	EfSticky                Sticky Bomb object
	EfNugget                Meat Nugget object
	Dot                     Bleeding damage over time used by Rusty Blade, Sawmerang, and Purple Imps.
	EfSpikestrip            Spikestrip object
	EfFirework              Bundle of Fireworks projectile
	EfThorns                Barbed Wire damage circle
	EfLightningRing         Chargefield Generator damage circle
	EfPoison                Toxic Centipede object
	EfMushroom              Bustling Fungus object
	EfIceCrystal            Frost Relic object
	EfMark                  The Hit List object
	EfScope                 Telescopic Sight object
	EfFireworkBurst         Firework flare object created by ``EfFirework``.
	EfJetpack               Rusty Jetpack jump effect.
	EffFlies                Flies spawned around the player by Alien Head.
	EfLaserBlast            Laser Turbine fire visual effect.
	====================    ====

Status
------

	==============    ====
	Name              Description
	==============    ====
	EfStun            Stars circling around stunned enemies.
	EfFear            Skull above feared enemies.
	EfOil             Object used by Chef's oil.
	EfLevel           Level up text.
	Buff              Used by Prescriptions, Massive Leech, and Pillaged Gold.
	CustomBar         Used by Prescriptions, Massive Leech, Pillaged Gold, Miner's X ability, and Sniper's X ability.
	==============    ====
	
Attacks
-------

	==================    ====
	Name                  Description
	==================    ====
	FireTrail             General purpose fire trail used by a number of things.
	ChainLightning        Lightning used by a number of things.
	EfMissileEnemy        Missiles used by Volatile elite enemies.
	EfGrenadeEnemy        Bombs created by the Artifact of Spite.
	JellyMissile          Projectile fired by Wandering Vagrants.
	WispBMine             Explosions created by the Ancient Wisp.
	MushDust              Poison dust cloud created by Mushrums.
	SpitterBullet         Used by the Spitter's attack.
	BugBullet             Projectile fired by Archer Bugs.
	SpiderBullet          Projectile fired by Mechanical Spiders.
	GuardBullet           Projectile fired by Temple Guards and Sanctuary Guards.
	ScavengerBullet       Electricity ball fired by the Scavenger.
	IfritBullet           Projectile fired by Ifrit which moves across the ground and explodes when hitting a target.
	IfritTower            Solid wall created by Ifrit.
	ImpGLaser             Imp Overlord's laser attack.
	ImpPortal             Portal created by the Artifact of Origin. Spawns an Imp Vanguard and several Purple Imps.
	TurtleMissile         Projectile fired by the Cremator.
	Boss1Shield           Providence's shield activation indicator.
	BossSkill1            Providence pink death circles.
	BossSkill2            Umbra of Providence's death circles.
	Boss1Bullet1          Ranged projectile fired by Providence.
	WurmMissile           Projectile spawned by the blue Gilded Wurm.
	ChefKnife             Chef Z skill projectile.
	CowboyDynamite        Bandit X skill projectile.
	HuntressBolt1         Huntress Z skill projectile.
	HuntressBoomerang     Huntress X skill projectile.
	HuntressBolt2         Huntress V skill projectile.
	HuntressBolt3         Huntress V skill projectile with Ancient Scepter.
	HuntressGrenade       Grenades created by Huntress's V skill projectile.
	EngiGrenade           Engineer Z skill projectile.
	EngiMine              Engineer X skill mine.
	EngiHarpoon           Engineer C skill harpoon.
	EngiTurret            Engineer V skill turret.
	PoisonTrail           Acrid's poison trail.
	FeralDiseaseMaster    Keeps track of enemies infected by Acrid's V skill.
	FeralDisease          Acrid V skill projectile.
	FeralDisease          Acrid V skill projectile with Ancient Scepter.
	Dot2                  Acrid V skill damage over time.
	RiotGrenade           Enforcer's V skill projectile.
	SniperBar             Sniper's reload bar.
	SniperDrone           Sniper's spotter drone.
	ConsHand              Loader C skill projectile.
	ConsRod               Loader V skill pylons.
	JanitorBaby           HAN-D's repair drones.
	JanitorGauge          HAN-D C skill meter.
	==================    ====

Visual
------

	==============    ====
	Name              Description
	==============    ====
	EfSparks          Bullet hit sparks. Simple animated sprite with random flipping on the Y axis.
	EfBullet2         Copy of EfSparks.
	MinerDust         Dash effect of Miner's X skill and Mercenary's C skill, as well as the ranged attacks of Providence and Ifrit.
	EfCircle          Circle which grows quickly before disappearing. Used in a number of places.
	EfFlash           Draws the sprite of an instance in a solid color. Decreases in opacity over time before disappearing.
	WhiteFlash        Full screen flash effect. Used several places, such as when a Wandering Vagrant or Imp Vanguard spawns.
	EfTrail           Character trails used by players when time is stopped and by blighted enemies.
	EfDodge           Text popup on dodged attacks.
	EfDamage          Damage popup used in a few places. Using ``misc.damage`` is preferred.
	BugGuts           Bouncing objects created when Archer Bugs are killed.
	BossText          Providence's speaking text when defeated.
	==============    ====

Stage Objects
=============

Decoration
----------

	==================    ====
	Name                  Description
	==================    ====
	EfDust1               60 pixel wide dust particle emitter.
	EfDust2               200 pixel wide dust particle emitter.
	EfRay                 Randomized light ray.
	EfRayFront            Duplicate of EfRay drawn at a lower depth.
	Waterfall             Animated waterfall.
	Pod                   Crashed escape pod created upon unlocking the Bandit and Enforcer.
	PodBehind             Near duplicated of ``Pod`` created upon unlocking the Huntress.
	MinerPod              Beat up escape pod found with the Direseeker.
	PigBeach              Fence used to travel to Boar Beach. Also used as decoration in Boar Beach.
	object415             A very nearly fully transparent image of a face and a hand. Found on the right side of one of the variants of Dried Lake.
	==================    ====

Unlocks
-------

	==================    ====
	Name                  Description
	==================    ====
	Deadman               Found in the bottom right of one variant of Hive Cluster. Unlocks Dead Man's Foot.
	FeralCage             Found in the top right of one variant of Sunken Tombs. Summons the Acrid boss.
	BlockDestroy          Dried Lake breakable wall.
	BlockDestroy2         Field of Sprites breakable floor.
	ArtifactButton        Used in several artifact unlock areas.
	MushroomButton        Found in Fungal Caverns. Used to unlock the Artifact of Distortion.
	GlowingRope           Found in Hive Cluster when unlocking the Artifact of Command.
	ArtifactNoise         Plays the artifact found sound effect and disappears when the player collides with it.
	SpawnArtifact2        Used to indicate the artifact spawn location in several levels.
	==================    ====

Contact Light
-------------

	==================    ====
	Name                  Description
	==================    ====
	Blastdoor             Main blast door object.
	BlastdoorRight        Blast door panel right side. Drawn above most objects.
	BlastdoorPanel        Used to activate blast doors.
	HologramProjector     Map of the ship with an indicator for the current location.
	Barrel3               Golden keycard canister.
	Door                  Keycard unlock door.
	Sign                  Signs below keycard doors.
	Medcab                Medical cabinet. Contains healing orbs and occasionally an item.
	GunChest              Orange weapon chest.
	Gauss                 Unteleported gauss cannon.
	GaussActive           Usable gauss cannon.
	Medbay                Unteleported surgical bed.
	MedbayActive          Usable surgical bed.
	Usechest              Unteleported Nano-Chest.
	UsechestActive        Usable Nano-Chest.
	HiddenHand            HAN-D unlock.
	DancingGolem          Easter egg golem.
	WallArtifact          Artifact display in the final boss arena.
	Screen                Randomized monitors in the final boss arena.
	RainSplash            Placed all over the place but doesn't actually do anything.
	==================    ====

Collision
---------

	==================    ====
	Name                  Description
	==================    ====
	B                     Main solid ground. Enemies and chests can spawn here.
	BNoSpawn              Solid walls and ground enemies can't spawn on.
	BNoSpawn2             Same as ``BNoSpawn``. Rarely used.
	BNoSpawn3             Same as ``BNoSpawn``. Rarely used.
	Rope                  Climbable rope or ladder collision box.
	Lava                  Collision box of lava found in Magma Barracks.
	Slow                  Collision box which applies a slowing debuff to colliding actors. Found in Hive Cluster.
	Water                 Water found in Sunken Tombs. Classic actors below this have modified physics.
	BossSpawn             Spawn region for Colossus and Toxic Beast.
	BossSpawn2            Spawn region for Ifrit.
	==================    ====
	
Items
=====

Items and artifacts should be accessed via their respective classes, but are listed here for the sake of completion.

Common
------

	==================    ====
	Name                  Item
	==================    ====
	Taser                 Taser
	Fireworks             Bundle of Fireworks
	Sticky                Sticky Bomb
	Scarf                 Hermit's Scarf
	Crowbar               Crowbar
	Nugget                Meat Nugget
	Warbanner             Warbanner
	Dice                  Snake Eyes
	Mortar                Mortar Tube
	Spikestrip            Spikestrip
	Medkit                First Aid Kit
	Stompers              Headstompers
	Root                  Bitter Root
	Gasoline              Gasoline
	Mushroom              Bustling Fungus
	Fireshield            Fire Shield
	Blade                 Rusty Blade
	Pig                   Life Savings
	Egg                   Sprouting Egg
	Thorns                Barbed Wire
	Hoof                  Paul's Goat Hoof
	Syringe               Soldier's Syringe
	Tooth                 Monster Tooth
	Glasses               Lens Maker's Glasses
	Battery               Mysterious Vial
	==================    ====

Uncommon
--------

	==================    ====
	Name                  Item
	==================    ====
	Feather               Hopoo Feather
	IceRelic              Frost Relic
	Whip                  Red Whip
	Roe                   Filial Imprinting
	Pelt                  Predatory Instincts
	LightningRing         Chargefield Generator
	Hourglass             Time Keeper's Secret
	Bottled               Will-o'-the-wisp
	Armsrace              Arms Race
	PoisonMine            Dead Man's Foot
	Cell                  Energy Cell
	Clover                56 Leaf Clover
	Teddy                 Tough Times
	Scythe                Harvester's Scythe
	Boxing                Boxing Gloves
	Purse                 Smart Shopper
	Rage                  Infusion
	Heart                 Guardian's Heart
	Mine                  Panic Mines
	Poison                Toxic Centipede
	Concussion            Concussion Grenade
	Missile               AtG Missile Mk. 1
	GoldGun               Golden Gun
	Ukulele               Ukulele
	Seed                  Leeching Seed
	Jetpack               Rusty Jetpack
	Shackle               Prison Shackles
	==================    ====
	
Rare
----

	==================    ====
	Name                  Item
	==================    ====
	LaserTurbine          Laser Turbine
	DeskPlant             Interstellar Desk Plant
	Blaster               Hyper-Threader
	Scepter               Ancient Scepter
	Hammer                Shattering Justice
	Hippo                 Dio's Friend
	Thallium              Thallium
	Jetpack2              Photon Jetpack
	Tesla                 Tesla Coil
	Repulsion             Repulsion Armor
	Scope                 Telescopic Sight
	Axe                   The Ol' Lopper
	IceCube               Permafrost
	Plasma                Plasma Chain
	Dagger                Ceremonial Dagger
	Hitlist               The Hit List
	Mask                  Happiest Mask
	JackBox               Old Box
	AlienHead             Alien Head
	Twins                 Rapid Mitosis
	Embryo                Beating Embryo
	Drill                 Heaven Cracker
	SkullRing             Wicked Ring
	MissileTri            AtG Missile Mk. 2
	Cannon                Brilliant Behemoth
	Fireshoe              Fireman's Boots
	==================    ====
	
Use
---

	==================    ====
	Name                  Item
	==================    ====
	Thqwib                Thqwib
	DynamitePlunger       Dynamite Plunger
	Amethyst              Gigantic Amethyst
	Meteor                Glowing Meteorite
	Marble                Carrara Marble
	Decoy                 Crudely Drawn Buddy
	Brain                 Rotten Brain
	Bomb                  Gold-plated Bomb
	Snowglobe             Snowglobe
	Sawmerang             Sawmerang
	Brooch                Captain's Brooch
	RepairKit             Drone Repair Kit
	Radio                 The Back-up
	JarSouls              Jar of Souls
	Lantern               Safeguard Lantern
	Watch                 Unstable Watch
	Mirror                Shattered Mirror
	BubbleShield          Shield Generator
	Pills                 Prescriptions
	Leech                 Massive Leech
	Minefield             Instant Minefield
	MissileBox            Disposable Missile Launcher
	Sack                  Pillaged Gold
	Doll                  Lost Doll
	Fruit                 Foreign Fruit
	Key                   Explorer's Key
	==================    ====
	
Boss
----

	==================    ====
	Name                  Item
	==================    ====
	Horn                  Ifrit's Horn
	Knurl                 Colossal Knurl
	JellyBox              Nematocyst Nozzle
	WormEye               Burning Witness
	Pauldron              Legendary Spark
	ImpTentacle           Imp Overlord's Tentacle
	==================    ====

Artifact
--------

	===============    ====
	Name               Item
	===============    ====
	Artifact1          Artifact of Honor
	Artifact2          Artifact of Kin
	Artifact3          Artifact of Distortion
	Artifact4          Artifact of Spite
	Artifact5          Artifact of Glass
	Artifact6          Artifact of Enigma
	Artifact7          Artifact of Sacrifice
	Artifact8          Artifact of Command
	Artifact9          Artifact of Spirit
	Artifact10         Artifact of Origin
	===============    ====
	
Misc
----

	==================    ====
	Name                  Item
	==================    ====
	Keycard               Keycard
	Artifact6Mini         Small Enigma
	Mystery               Artifact of Enigma use item
	MarbleUsed            Active Carrara Marble
	Tshirt                White Undershirt
	BookDrop              Monster log pickup
	==================    ====
Colored Text Formatting
=======================

Risk of Rain's colored text formatting allows you to draw multiple colors of text with a single string.
It is used by putting a color name between to ``&`` symbols in a string.
Keep in mind that ``&`` symbols can not be used for other purposes in strings drawn with color formatting.

The following is a complete list of recognized colors:

* ``r``: Red
* ``b``: Blue
* ``g``: Green
* ``y``: Yellow
* ``or``: Orange
* ``bl``: Black
* ``lt``: Light Gray
* ``dk``: Dark gray
* ``w``: White
* ``p``: Pink
* ``!``: Reset

The reset character is special as it returns to the original drawing color.

Below is an example string using a variety of different colors:

``"This is an &b&example&!& of a string using &or&string color formatting&!&, &p&this text will appear as pink&!&!"``
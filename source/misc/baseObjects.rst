*****************
Base Object Types
*****************

This page lists object types for use with :ref:`Object.base <object-base>`, including common variables and important behavior. 

.. contents:: Table of Contents
   :local:

Actors
======

NPC
---

	| This is the most basic custom actor type.
	| On its own this base does nothing, but can be damaged by actors on other teams, handles buffs, and does default actor rendering.

	When making custom enemies you'll usually want to use one of the below bases.

	**Variables:**
	
		=================      ===========
		Name                   Description
		=================      ===========
		``name``               The name of the actor. Displayed on the boss health bar, when a player is killed by it, and by Sniper's spotter.
		``maxhp``              | The actor's maximum health.
		                       | To have this value scale with enemies, use :ref:`Difficulty.getScaling <difficulty-getScaling>`.
		                       | The health of a Stone Golem, for example, is ``300 * Difficulty.getScaling("hp")``.
		``hp``                 Set this to the same initial value as ``maxhp``.
		``damage``             The base damage stat of the actor.
		                       For example, the damage of a Greater Wisp is ``23 * Difficulty.getScaling("damage")``.
		``exp_worth``          The amount of gold and experience dropped by the enemy.
		                       Uses the default scaling method (``6 * Difficulty.getScaling()`` for a Lemurian).
		=================      ===========

Enemy
-----

	Inherits from ``NPC``.

	| This is the basic custom enemy type, with no built in behavior.
	| Useful for making custom flying or other unusual enemies, but typically you'll use ``EnemyClassic`` instead.

EnemyClassic
------------

	Inherits from ``Enemy``.

	| This is the actor type used for most common enemies.
	| Enemies which walk around the stage and are affected by gravity are "classic" enemies.

	**Variables:**
	
		=================      ===========
		Name                   Description
		=================      ===========
		``knockback_cap``      The amount of damage the actor needs to take to receive knockback.
		                       Uses the default scaling method (``13 * Difficulty.getScaling()`` for a Lesser Wisp).
		``pHmax``              The actor's movement speed.
		``can_jump``           Whether the actor is able to jump up ledges. 1 or 0.
		``can_drop``           Whether the actor is able to drop down ledges. 1 or 0.
		=================      ===========

Boss
----

	Inherits from ``Enemy``.

	| This is the actor used for most bosses.
	| Does not include "classic" actor behavior, for that use ``BossClassic``.

	| This has a few differences over the default ``Enemy`` base:
	| - The boss healthbar will appear by default.
	| - The enemy's healthbar (when not a HUD boss) gains priority and will be drawn even if the max number of healthbars is already drawn.
	| - The enemy has an increased chance of dropping its monster log.

	**Variables:**
	
		=================      ===========
		Name                   Description
		=================      ===========
		``name2``              This is the boss's subtitle displayed on the boss healthbar. 
		=================      ===========

BossClassic
-----------

	Inherits from ``Boss``.

	| This is the actor type used for enemies which are both a boss and have "classic" behaviour.
	| Bosses using this in the base game include the Imp Overlord and Ancient Wisp.

	This base has no special traits, but combines the ``Boss`` and ``EnemyClassic`` bases.

Drone
-----

	Inherits from ``NPC``.

	The drone automatically enters the ``"chase"`` state when within range of an enemy, at which point custom drones should handle attacking.

	**Variables:**
	
		======================      ===========
		Name                        Description
		======================      ===========
		``child``                   The object ID of the drone's DroneItem, created so the drone can be re-purchased when it is destroyed.
		                            Set this to a negative value to not have anything created when the drone dies.
		``target``                  | Automatically updated to the ID of the actor the drone is trying to attack.
		                            | Make use of this when writing custom attack code
		``x_range``                 The horizontal range at which the drone will try to attack a target. 
		``y_range``                 The vertical range at which the drone will try to attack a target.
		``sprite_idle``             The drone's default animation.
		                            Can be set using ``instance:setAnimation("idle", sprite)``.
		``sprite_idle_broken``      Used as the drone's idle sprite when hp is below 50% of maxhp.
		                            Can be set using ``instance:setAnimation("idle_broken", sprite)``.
		======================      ===========

Interactables
=============

MapObject
---------

	| This base does nothing on its own, but is recommended to be used when creating custom interacables.
	| Also blocks other interactables from spawning on top of it when generating the level. 

Chest
-----

	Inherits from ``MapObject``.

	| This base is used to define new interactables like barrels and chests.
	| Interactables defined using this base are opened for free using the Explorer's Key.
	
	| When opened, the object's 0th alarm is set to 30
	| Check if ``instance:getAlarm(0) == 1`` to time item spawning with the base game.
	| The instance's ``active`` variable is also set to 1 for a frame when opened, and then changed to 2 afterwards.

	**Variables:**
	
		=================      ===========
		Name                   Description
		=================      ===========
		``text``               | The prompt which appears when the player stands near the object.
		                       | The price and button are automatically added to this text.
							   Example values from built-in objects include ``"to purchase chest"`` and ``"to open container"``.
		``sound``              | The sound that will be played when the object is opened.
		                       | If you do not wish for it to play a sound, set this value to negative, otherwise set it to the sound's ID.
		``shake``              Setting this to 0 removes the slight screenshake when opened.
		``cost``               | The amount of gold the chest costs to open.
		                       | Setting this to 0 makes the container free.
							   Otherwise, use :ref:`Difficulty.getScaling <difficulty-getScaling>` to find the right value.
							   An example of this value for a large chest in the base game would be ``math.ceil(25 * Difficulty.getScaling("cost"))``.
		=================      ===========

DroneItem
---------

	Inherits from ``MapObject``.

	This base is used to define new purchasable broken drones.

	**Variables:**
	
		=================      ===========
		Name                   Description
		=================      ===========
		``name``               The name of the drone, used to generate the purchase prompt.
		                       Examples from the base game include ``"missile drone"`` and ``"healing drone"``.
		``child``              The drone object that will be spawned when purchased.
		                       Set this to the Drone Object's ID.
		``cost``               The amount of gold the chest costs to open.
		                       Use :ref:`Difficulty.getScaling <difficulty-getScaling>` to find the right value. Note that unlike other purchasables, drones **do not use the "cost" scaling**.
		                       An example of this value for a basic drone in the base game would be ``math.ceil(40 * Difficulty.getScaling())``.
		=================      ===========



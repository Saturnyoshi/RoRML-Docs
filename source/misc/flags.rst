*************
Profile Flags
*************
Flags are strings defined in the launcher which can be used to toggle certain features or interact with mods.

For how to read the currently enabled flags from your own mods, see the :doc:`ModLoader module page </global/modloader>`.

In addition to being used by mods, there are a number of built-in flags, they are as follows:

	================================    =================================
	Name                                Effect
	================================    =================================
	``bandit_amethyst_glitch``          Enables the glitch where Bandit is able to gain infinite speed using the Gigantic Amethyst.
	``prov_shield_glitch``              Enables a glitch found in version 1.2.2 of the game which fully disables Providence's shield.
	``rope_glitch``                     Enables a glitch where ropes can be grabbed when teleporting back to spawn from off stage.
	``origin_glitch``                   Enables the glitch which causes the artifact of Origin to spawn extra sets of imps when time is stopped.
	``persistent_timestop_glitch``      Enables the glitch where stopped time persists between runs.
	``disable_boar_beach_online``       Disables entering Boar Beach in online co-op.
	================================    =================================




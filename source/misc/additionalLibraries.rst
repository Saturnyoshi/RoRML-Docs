***********************
Extra Libraries
***********************

| In addition to everything implemented by RoRML, a couple addiitonal libraries are available.
| These libraries are used by RoRML itself but are made available for mods to load without having to contain a copy of the code themselves.

Usage
=====

Using these libraries is simple, just ``require`` them as you would any file included in your mod.

Example:
::

    local json = require("json")
    local t = {array = {1, 2, 4, 8, 16}, a = "abc"}
    print(json.encode(t))

Loads the JSON library and uses it to print a JSON representation of the table ``t``.

Libraries
=========

bit
---

    The LuaJIT bitwise operations library.

    Documentation is available here: `luajit.org <https://bitop.luajit.org>`_

socket
------

    The LuaSocket library.

    Documentation is available here: `LuaSocket <http://w3.impa.br/~diego/software/luasocket/reference.html>`_

json.lua
--------

    Provides functions for serializing and deserializing Lua tables as JSON objects.

    Link: `json.lua <https://github.com/rxi/json.lua>`_

    **json.encode(value)**

        Returns a JSON string representing the value.

    **json.decode(string)**

        Returns the value represented by the provided JSON string.

semver.lua
----------

    Implements an object for comparing semantic versions in lua.

    Link: `semver.lua <https://github.com/kikito/semver.lua>`_

    **semver(...)**

        Returns a new semantic version object.

        May either take a single string representing the version (``semver("1.3.2")``) or multiple arguments representing each part of the verison number (``semver(1, 3, 2)``).

inspect.lua
-----------

    | Adds a function for getting a human readable string representation of any value, including expanding tables.
    | The included copy has been slightly modified to work nicely with RoRML types.

    Link: `inspect.lua <https://github.com/kikito/inspect.lua>`_

    **inspect(value)**

        Returns a string representation of the value.


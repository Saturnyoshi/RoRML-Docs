******************
Required Mod Files
******************

This page details the files required to make a functional mod.
  
.. contents:: Table of Contents
   :local:
   
``main.lua``
============

This is your primary code file. This file is automatically executed by the game when your mod is loaded. Depending on the size of your mod, this file may contain all of the source code, or just be used to load other files defining individual features.

Example
-------

::

	-- Print to console
	print("Hello, World!")
	-- Load our other file
	require("someFile")
	
Prints the string ``"Hello, world!"`` to the console and then loads the file ``someFile.lua``.


``metadata.json``
=================

| The metadata.json file in each mod contains important information about that mod, including things such as its name, version, and author.
| This is referenced by both the launcher and the game itself.

Fields
------
The following is a complete list of fields found in metadata.json:

====================    ================    ====
Name                    Type                Description
====================    ================    ====
name (Required)         string              This is the name of the mod as displayed in the launcher and is purely for visual purposes.
version (Required)      string              This is a semantic version formatted string indicating the mod version. Used by the dependency system to make sure dependencies are new enough.
internalname            string              Used when referencing the mod by code, try to keep this short and concise but prevent conflicts. 2 mods can not share the same ``internalname``. If omitted, this value will default to that of the ``name`` field.
author                  string              The author displayed in the launcher, purely for visual purposes.
description             string              The description displayed in the launcher when the mod is selected, purely for visual purposes.
dependencies            list                Complete list of mod dependencies. See :ref:`below <moddependencies>` for info.
mpcompat                bool                **Do not check this unless you actually know what you're doing.** Indicates whether the mod supports online co-op or not. If excluded, this value will be set to false and online co-op will be disabled.
====================    ================    ====

.. _moddependencies:

Dependencies
------------

| Dependencies are other mods referenced by your own mod, usually libraries which provide additional functionality to the API used by your mod. Dependencies can be optional and have a minimum required version.
| Marking a mod as a dependencies gives access to anything exposed by the mod via the :doc:`export </global/export>` function. Note that :doc:`namespace based functions </misc/contextSearch>` will be able to access content from all mods, regardless of whether they are set as dependencies.

The following is a list of dependency fields:

====================    ================    ====
Name                    Type                Description
====================    ================    ====
name (Required)         string              The ``internalname`` of the dependency.
version                 string              Semantic version string. If the version of the dependency mods is considered to be lower than this then it will be considered missing.
optional                bool                Setting this to true will make the game ignore the dependency if it’s missing, rather than failing to load the mod. Defaults to false.
====================    ================    ====

Example
-------
.. code-block:: json

	{
	    "name": "Metadata Example",
	    "description": "An example of all the metadata fields!",
	    "internalname": "metadataExample",
	    "version": "1.4.2",
	    "author": "Saturnyoshi",
	    "mpcompat": true,
	    "dependencies": [
	        {
	            "name": "item-removal-lib",
	            "version": "1.0.0",
	            "optional": true
	        }
	    ]
	}
	
An example of mod metadata with the item removal library mod added as an optional dependency.


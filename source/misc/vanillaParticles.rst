*****************
Vanilla Particles
*****************

.. note::
	
	This document is a work in progress and some of the information may be incomplete or false.

The following is a list of all the particles provided by the game, and a short description of each one:

================    ===========
Particle            Description
================    ===========
Fire                
Fire2               
FireIce             
Fire3               
Fire4               
Spark                
Rubble1                
Rubble2                
Poison                
Dust1                
Dust2                
JellyDust                
Assassin                
Speed                
SpeedPoison                
Lightning                
Heal                                
Smoke                
Smoke2                
Smoke3                
Smoke4                
Smoke5                
Snow                
SporeOld                
Spore                
Ice                 
Hero                
Lava                
PixelDust                
Leaf                
Mortar                
Radioactive                
HuntressBolt1                
HuntressBolt2                
HuntressBolt3                
TempleSnow                
Blood2                
Blood1                
Fire3                
EngiHarpoon                
HarpoonEnemy                
Rain                
Rain2                
RainSplash                
CyborgBullet1                
Bubble                
SpaceDust                
DiagFire                
CutsceneSmoke                
Hopoo                
SmokeFirework                
IceRelic                
FireworkFlare                
FireworkSmoke          
================    ===========      
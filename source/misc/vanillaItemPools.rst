******************
Vanilla Item Pools
******************

The following is a list of all the item pools built into the game:

================    ===========
Pool                Description
================    ===========
common              The item pool from which common (white) items are rolled.
uncommon            The item pool from which uncommon (green) items are rolled.
rare                The item pool from which rare (red) items are rolled.
use                 The item pool from which use items are rolled.
enigma              | Item pool used by Artifact of Enigma to select a random use item.
                    | The only default item pool where ``ignoreEnigma`` is true.
medcab              Item pool used by the medical cabinets in the last level.
gunchest            | Item pool used by the weapons chests in the last level.
                    | The only default item pool to use different item weights.
================    ===========

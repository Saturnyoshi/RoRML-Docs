*************
Vanilla Buffs
*************

The following is a list of all the buffs provided by the game, and a short description of each one:

================    ===========
Buff                Description
================    ===========
wormEye             The buff used by Burning Witness.
slow                The slowing debuff used by a multitude of things, such as Prison Shackles and Rotten Brain.
burstSpeed          The speed buff used by HAN-D and Filial Imprinting.
burstHealth         The healing buff that Filial Imprinting and leeching enemies use.
slow2               The freezing stun that permafrost uses.
shield              The armor buff used by the Iron Boarlit and Temple Guards.
thallium            The Thallium debuff.
warbanner           The Warbanner attack speed, damage, movement speed, and healing buff.
dice1               Level one of the crit buff from Snake Eyes.
dice2               Level two of the same.
dice3               Level three.
dice4               Level four.
dice5               Level five.
dice6               Level six.
snare               The snaring debuff that Taser uses.
dash                Unused significant speed buff.
poisonTrail         Acrid's poison trail buff.
sunder1             Level one of the armor debuff Shattering Justice uses.
sunder2             Level two of the same.
sunder3             Level three.
sunder4             Level four.
sunder5             Level five.
blood1              Level one of the attack speed buff Predatory Instincts uses.
blood2              Level two of the same.
blood3              Level three of the same.
burstAttackSpeed    The attack speed buff that Filial Imprinting uses.
superShield         The invulnerability that Providence uses.
burstSpeed2         The speed buff that Loader uses.
oil                 The debuff that enemies receive from Chef's oil.
================    ===========

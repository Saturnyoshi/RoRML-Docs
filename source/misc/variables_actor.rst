*************************
Common Variables (Actors)
*************************

This page lists common variable names found in :doc:`actor </class/actorInstance>` and :doc:`player </class/playerInstance>` instances.

For variables of other object types, see the main :doc:`common variables </misc/variables>` page.

.. contents:: Table of Contents
   :local:
	
General Variables and Stats
---------------------------
	
	======================    ====
	Name                      Description
	======================    ====
	name                      A string containing the name of the character, such as ``"Lemurian"`` or ``"Commando"``.
	team                      A string. Actors with the same team are unable to hurt each other. Usual teams are ``"player"`` or ``"enemy"``.
	hp                        The current health of the actor.
	maxhp                     The maximum health of the actor. Modifying this value does not work on players, the ``maxhp_base`` variable must be modified instead.
	hp_regen                  Added to the health of the actor each frame.
	lastHp                    Health of the actor on the previous frame.
	damage                    The base damage of the actor.
	attack_speed              The attack speed multiplier of the actor.
	armor                     The defense of the actor. An actor with 100 armor takes 50% damage, an actor with 200 armor takes 33% damage, an actor with 300 takes 25%, and so on.
	critical_chance           The critical chance rate of the actor. Effective range of 0 to 100, anything after or equal to 100 is a guaranteed crit.
	critical_chance_bonus     Value added to ``critical_chance`` when whether an attack should crit is calculated. Resets after each attack. Never used by the base game.
	invincible                | Invincibility timer. Counts down each frame. Damage is not taken if this is more than 0.
	                          | A value greater than 1000 causes the player's healthbar to display the text "invincible", any other value greater than 1 displays the text "immune".
	======================    ====


Movement and Skills
-------------------

	================    ====
	Name                Description
	================    ====
	pHspeed             The actor's current horizontal speed.
	pVspeed             The actor's current vertical speed.
	pHmax               The actor's maximum running speed.
	pVmax               The actor's jump height.
	pGravity1           Value added to the actor's ``pVspeed`` each frame.
	pGravity2           Added to ``pVspeed`` instead of ``pGravity1`` if ``moveUpHold`` is set to 1.
	moveLeft            Whether the actor should move left. Equivalent to holding the left key for players. 1 or 0.
	moveRight           Whether the actor should move right. Equivalent to holding the right key for players. 1 or 0.
	moveUp              Whether the actor should jump. Equivalent to pressing the jump key for players. 1 or 0.
	moveUpHold          Used to determine whether ``pGravity1`` or ``pGravity2`` should be added to the actor's vertical speed. 1 or 0. Only ever set to 1 by the player holding the jump button.
	free                Set to 1 if there's no ground below the actor, otherwise 0.
	can_jump            Determines whether an enemy can jump up ledges. 1 or 0.            
	can_drop            Determines whether an enemy can drop down ledges. 1 or 0.
	z_skill             Set to 1 when the actor should use its first skill.
	x_skill             Set to 1 when the actor should use its second skill.
	c_skill             Set to 1 when the actor should use its third skill.
	v_skill             Set to 1 when the actor should use its fourth skill.
	z_range             Horizontal range of an enemy's first skill.
	x_range             Horizontal range of an enemy's second skill, where applicable.
	c_range             Horizontal range of an enemy's third skill, where applicable.
	v_range             Horizontal range of an enemy's fourth skill, where applicable.
	activity            | The current action the actor is doing.
	                    | Any value from 1 up to 5 is one of the actor's skills, 30 means climbing a rope, 95 means using a command crate, and 99 means teleporting in or out of the stage.
	activity_type       Determines the limitations of movement an actor has when performing a skill. Type 3 and higher can jump, 4 can walk. Type 2 floats. Type 0 means no skill being used, so no limitations.
	activity_var1       Used for different purposes depending on the actor's current skill.
	activity_var2       Used for different purposes depending on the actor's current skill.
	================    ====
	
Enemy Only
----------

	These variables only have an effect when working with non-player actors.
	
	Some of these fields only affect "classic" enemies.
	A "classic" enemy is any enemy or boss which walks across the ground and can fall; Colossus, for example, would not be considered a "classic" enemy as it is unable to fall down ledges.
	
	======================    ====
	Name                      Description
	======================    ====
	state                     String indicating what action the enemy is currently doing. Usually something like ``"idle"`` or ``"chase"``.
	point_value               | When the enemy is despawned, 80% of this value is added back to the director's enemy spawn points.
	                          | The value of an enemy increases depending on how strong the enemy is and whether it's an elite (with the artifact of Honor disabled).
					      	  This value is also used to determine how often enemies drop items with the aritfact of Sacrifice enabled.
	exp_worth                 The money and EXP value of the enemy. An enemy's dropped EXP is always equal to its gold not counting the Smart Shopper item.
	prefix_type               | Contains whether the enemy is blighted or elite.
	                          | If this value is 1, the enemy is elite. If it's 2, the enemy is blighted. A value of 0 means the enemy is not blighted or elite.
	elite_type                | If the enemy is not elite this value is -1, otherwise it is the ID of the enemy's elite type.
	                          | Elite type IDs starting from 0 in order are as follows: blazing, frenzied, leeching, overloading, volatile.
	blight_type               If the enemy is not blighted this value is -1, otherwise it's two of the numbers 3, 5, 7, 11, and 13 randomly multiplied together to indicate a combination of elite effects.
	elite_is_hard             | Indicates whether the enemy should have the additional elite effects that are enabled from the third level onwards or Monsoon difficulty.
	                          | These effects include the Overloading elite passive lightning, Frenzied elite teleportation, Leeching elite ally healing, and Volatile elite missiles.
	ghost                     *Classic enemies only:* Number indicating whether the enemy is a ghost. 0 is not a ghost, 1 is a ghost; Other values will have unwanted effects.
	show_boss_health          Indicates whether the enemy's health should be displayed on the HUD. Set to 1 by bosses and blighted enemies, otherwise 0.
	name2                     The enemy's boss title when ``show_boss_health`` is 1. For example, Wandering Vagrants have ``"Gentle Protector"`` and blighted enemies have ``"Phantasm"``.
	disable_ai                *Classic enemies only:* When this variable is set to 1, enemies are unable to move and attack. Automatically updated occasionally, being set to 1 if the enemy is more than 800 pixels from a player and reset to 0 otherwise.
	death_timer               *Classic enemies only:* Time until the enemy despawns. 
	target                    Instance ID the enemy is trying to attack. When the enemy is targetting the player, this is the ID of the player's "POI" object. If the enemy is a ghost targetting another enemy, this is the ID of that enemy.
	lasthit_x                 The X position of the previous attack the enemy was hit by. Used to determine the direction the enemy is hit when it takes knockback.
	boss_death_index          The frame since opening the game that the enemy was spawned on. Only used by bosses to determine the time it took to kill them.
	sync                      Forces the enemy to be spawned for clients when playing in online coop as the host. Will have unintended effects if set to 1 when not the host. Only used by the Ancient Wisp.
	health_tier               Starts at 10 when the enemy is at max health, with each value indicating 10% health. Used to indicate the last time the enemy's health was synced.
	health_tier_threshold     The amount of change in health needed before enemy health is synced again. Usually 3 on normal enemies and 1 on bosses, indicating 30% and 10% respectively.
	make_sound                Hit sound effect cooldown used by a few enemies. These enemies will only make a sound when hit if this value is 0. Has no effect on any other enemies.
	hit_pitch                 The pitch the enemy's hit sound is played at. Only used by Spitters, otherwise this value is always 1.
	shake_frame               Causes the screen to shake when the enemy's death animation is on a certain frame. A value of -1 is no shake, 0 is the first frame, 1 is the second frame, etc.
	blast                     When this value is anything other than 0 when the enemy dies, the coins it spawns will be shot out faster. This is used by the Bandit's "Lights Out" skill to make kills more dramatic.
	======================    ====

Player Only
-----------
	
	These variables do not exist for any non-player actors.
	
	======================    ====
	name                      Description
	======================    ====
	maxhp_base                The base maximum health of the player. The player's ``maxhp`` is set to ``maxhp_base`` multiplied by ``percent_hp`` each frame.
	percent_hp                Multiplier for ``maxhp_base``. Used by Bitter Root.
	maxhpcap                  The max ``maxhp`` of the player. Never any number other than 9999.
	use_cooldown              Base use item cooldown in seconds. Starts at 45. Decreased by Rapid Mitosis.
	child_poi                 The ID of the player's "POI" instance; This is what enemies use to target and attack players. Destroying this instance will cause enemies to stop targetting the player.
	item_count_total          The total number of items the player has picked up.
	outside_screen            Increased by 1 for each frame the player is out of bounds. When it reaches 90, the player is teleported back to the starting teleporter or nearest ground.
	distance_ran              The number of pixels the player has walked since the start of the run. Increased each frame by the absolute value of ``pHspeed``.
	opened_chests             The number of chests opened by players. Does not work properly in coop, only increasing for the first player. Displayed on final results screen.
	boss_count                The number of bosses killed. Increased by 1 for each boss killed while the player is alive. Displayed on final results screen.
	boss_points               The score displayed on the final results screen for killed bosses.
	dead                      1 when the player is dead, 0 otherwise. Probably shouldn't be modified.
	expr                      The player's current collected experience points.        
	maxexp                    The experience points required to level up. When ``expr`` reaches this value, it is reset back to 0 and the player levels up.
	level                     The player's current level. Does not actually affect anything.
	ropeUp                    Whether the player is holding the up key to move up ropes. 1 or 0.
	ropeDown                  Whether the player is holding the down key to move down ropes. 1 or 0.
	rope_parent               Instance ID of the rope the player is currently grabbing on to. If the instance does not exist, then the player is not grabbing a rope.
	canrope                   Indicates whether or not the player is allowed to grab ropes.
	artifact_lock_choice      The locked skill when the artifact of Distortion is active. Ranges from 0 to 3, with 0 being the first skill.
	true_invincible           When set to 1 the player's health is constantly reverted to its previous value. Never used by the base game.
	gold_cooldown             Cooldown before the coin pickup sound effect can be played again. Set to 6 when the sound is played, counts down by 1 each frame.
	use_item_held             The number of frames the player has held their current use item for. Increases by 1 every 5 seconds.
	swap_item                 Set to 1 on the frame you pick up a new use item. Cannot pick up another use item if its set to 1. Used to make sure you don't swap use items multiple times on the same frame.
	user_name                 The player's displayed name in online coop.
	item_value                Compared to the director's ``average_item_value`` when force item sharing is enabled to determine whether the player should be allowed to pick up items.
	following_player          Used in online coop when the player is dead. The instance ID of the player the camera is focused on.
	following_player_index    The player index of ``folowing_player``.
	force_z                   Makes the player use their first skill. Used in online coop to sync skill usage.
	force_x                   Makes the player use their second skill.
	force_c                   Makes the player use their third skill.
	force_v                   Makes the player use their fourth skill.
	force_swap                When set to 1 the player will pick up use items they walk over even if they aren't pressing the swap key. Used in online coop to sync item pickup.
	ping                      The player's ping as a string in online coop. Defaults to ``"---"`` and is never changed for any players other than the local one.
	ready                     Whether the player is ready to teleport to the next level in online coop. Determines whether the checkmark below them is filled when teleporting.
	======================    ====
	
Miscellaneous
-------------

	==================    ====
	Name                  Description
	==================    ====
	force_death           When set to 1, the actor is instantly killed.
	ghost_x               In online coop, this value is gradually moved towards the actor's real x position and used for drawing. This is to ensure enemies don't appear to teleport when their position is updated.
	ghost_y               In online coop, this value is gradually moved towards the actor's real y position and used for drawing.
	knockback_cap         The amount of damage an enemy needs to take to receive knockback
	knockback_value       The power of the knockback. If this value is more than 3, ``knockback_cap`` will be ignored when deciding whether the enemy should take knockback.
	force_knockback       When this is set to 1, the enemy will take knockback regardless of all other factors and then reset the variable to 0.
	frozen                Set to 1 on the frame the actor is frozen, set to 2 afterwards. Set to 0 when not frozen. Used by the Permafrost and Snowglobe items.
	stunned               Set to 1 when the enemy is stunned with stars floating around its head, otherwise 0.
	stun_immune           | When greater than 0 the enemy is immune to slowing debuffs.
	                      | In the base game, this applies to elder lemurians, boss imps, and Providence.
	dot_immune            | Cooldown before the actor can be hit again by fire trails, poison trails, and mushroom dust clouds. Set to 30 when the actor is hit for a half second cooldown.
	                      | Because of this, overlapping environmental DoT damage sources have no effect.
	weather_immune        | Cooldown before the actor can be hit again by a damager with ``weather`` set to 1. When the actor is hit by a ``weather`` attack, this is set to 240, for a 4 second cooldown. 
	                      | Never used in the base game.
	walk_speed_coeff      Multiplier for the speed of the actor's walk animation. Only used by Ifrit and the Enforcer.
	bunker                Enforcer shielding flag. When set to 1, damage taken from the front is blocked. Also causes players to be unable to go up and down ropes.
	poison_trail          Used by the Acrid's C ability. Never directly modified, only changed through the ``poisonTrail`` buff.
	poison_speed_boost    When set to 1, the actor will gain 0.4 movement speed for a frame and then the variable will be reset to 0. Set to 1 when a player walks over a poison trail.
	==================    ====
	
Alarms
------

	| In addition to their many variables, actors also use alarms for a few things, such a skill cooldowns.
	| These can be modified using the instance :ref:`alarm methods <alarm-methods>`.
	
	===========    ====
	Alarm Index    Description
	===========    ====
	Alarm 0        *Player only:* Use item cooldown.
	Alarm 2        First skill cooldown.
	Alarm 3        Second skill cooldown.
	Alarm 4        Third skill cooldown.
	Alarm 5        Fourth skill cooldown.
	Alarm 6        *Player only:* Increases ``use_item_held`` by 1 every 5 seconds. Only used by the local player in online coop.
	Alarm 7        Knockback timer. Set to a value when an enemy is being knocked away or frozen.
	===========    ====
	


Item Related
------------
	
	.. note:: | Although many of these only work with players, they're present on all actor instances;
	          | Don't depend on this as it may change in the future.
	
	================    ============    ====
	Name                Player only?    Description
	================    ============    ====
	mortar              No              Item stack of Mortar Tube.
	mushroom            Yes             Item stack of Bustling Fungus.
	still_timer         Yes             Timer used by Bustling Fungus to determine when the mushrooms should pop up. Increased by 1 each frame the player is still.
	nugget              No              Item stack of Meat Nugget.
	crowbar             No              Item stack of Crowbar.
	scarf               Yes             Item stack of Hermit's Scarf.
	scarfed             Yes             Whether the Hermit's Scarf is active this frame.
	cell                Yes             Item stack of Energy Cell.
	cell_bonus_last     Yes             Energy Cell bonus from the previous frame. Subtracted from the player's ``attack_speed`` each frame before the new bonus is calculated.
	clover              Yes             Item stack of 56 Leaf Clover.
	wolfblood           No              Item stack of Predatory Instincts.
	plasma              No              Item stack of Plasma Chain.
	plasma_count        No              Current number of active plasma chains.
	poison_mine         Yes             Item stack of Dead Man's Foot.
	reflector           Yes             Item stack of Repulsion Armor.
	reflector_charge	Yes             Number of hits with Repulsion Armor. When it reaches 6 the item's effect is triggered.
	reflecting          Yes             Repulsion Armor reflect timer. Counts down to 0 by 1 each frame.
	reflecting_hit      Yes             Set to 1 if the player is hit while Repulsion Armor is active; set back to 0 in the same frame after a bullet is fired back to the nearest enemy.
	dagger              Yes             Item stack of the Sacrificial Dagger.
	medkit              Yes             Item stack of First Aid Kit.
	medkit_cd           Yes             Timer before the First Aid Kit can be activated again. Counts down by 1 each frame.
	medkit_timer        Yes             Displayed frame of First Aid Kit proc animation. When it reaches 10 the player is healed and the effect is finished.
	armsrace            Yes             Item stack of Arms Race. The ``mortar`` variable of the player's drones is set to this value and the ``missile`` variable is set to it divided by 2.
	lava_pillar         Yes             Item stack of Will-O'-The-Wisp.
	lifesteal           No              Item stack of Leeching Seed. Also used by leeching elites.
	lightning           No              Item stack of Ukulele. Also used by overloading elites.
	axe                 No              Item stack of The Ol' Lopper.
	lightning_ring      Yes             Item stack of Chargefield Generator.
	warbanner           Yes             Item stack of Warbanner.
	blaster             No              Item stack of Hyper-Threader.
	bleed               No              Item stack of Rusty Blade.
	maxshield           Yes             Maximum shield health. Guardian's Heart adds 60 to this.
	shield              Yes             Current shield health.
	shield_cooldown     Yes             Timer since being hit before ``shield`` is restored to ``maxshield``. Also used by Sprouting Egg. Counts down by 1 each frame.
	worm_eye            Yes             Item stack of Burning Witness.
	mine                Yes             Item stack of Panic Mines.
	missile             No              Item stack of AtG Missile Mk. 1.
	missile_tri         No              Item stack of AtG Missile Mk. 2.
	horn                No              Item stack of Ifrit's Horn.
	hourglass           Yes             Item stack of Time Keeper's Secret.
	hourglass_cd        Yes             Time Keeper's Secret recharge timer. Counts down by 1 each frame.
	hp_after_kill       Yes             Item stack of Infusion.
	taser               No              Item stack of Taser.
	tentacle            Yes             Item stack of Imp Overlord's Tentacle.
	tentacle_id         Yes             The instance ID of the imp spawned by Imp Overlord's Tentacle.
	tentacle_cd         Yes             Cooldown timer before Imp Overlord's Tentacle can spawn another imp after one is killed. Counts down by 1 each frame.
	tesla               Yes             Item stack of Tesla Coil.
	jackbox             Yes             Item stack of Old Box.
	jetpack             Yes             Item stack of Photon Jetpack.
	jetpack_fuel        Yes             The amount of fuel remaining on the Photon Jetpack. Counts down by 2 each frame the jump button is held.
	jetpack_cd          Yes             Cooldown for ``jetpack_fuel`` to recharge. Set to 20 each time the jetpack is used. Counts down by 1 each frame.
	thallium            No              Item stack of Thallium.
	keycard             Yes             Item stack of Keycard. Decreased by 1 each time a keycard is used.
	gold_gun            No              Item stack of Golden Gun.
	gold_on_hit         No              Used by Pillaged Gold and one of the imps spawned by Imp Overlord's Tentacle. When an enemy is hit, this many coins is spawned.
	gp5                 Yes             Used by Life Savings. Value added to the player's gold each frame.
	spikestrip          Yes             Item stack of Spikestrip.
	heal_after_kill     Yes             Item stack of Monster Tooth.
	sticky              No              Item stack of Sticky Bomb.
	stompers            No              Item stack of Head Stompers.
	sunder              No              Item stack of Shattering Justice.
	mark                Yes             Item stack of The Hit list.
	mark_tally          Yes             Total number of collected hit list ticks. Only used for the HUD.
	mask                Yes             Item stack of Happiest Mask.
	redwhip             Yes             Item stack of Red Whip
	combat_timer        Yes             Used by Red Whip. Counts down by 1 each frame. Reset to 90 when the player uses a skill or takes damage.
	knockback           No              Item stack of Boxing Glove.
	gas                 Yes             Item stack of Gasoline.
	purse               Yes             Item stack of Smart Shopper.
	dice                Yes             Item stack of Snake Eyes.
	drill               Yes             Item stack of Heaven Cracker.
	egg_regen           Yes             Health regen value from Sprouting Egg. Added to the player's health each frame that ``shield_cooldown`` is 0.
	embryo              Yes             Item stack of Beating Embryo.
	scepter             Yes             Item stack of Ancient Scepter. Note that setting this to 1 will not change the player's skill icon, that is handled in the item's pickup code.
	scope               No              Item stack of Telescopic Sight.
	scythe              No              Item stack of Harvester's Scythe.
	explosive_shot      No              Item stack of Brilliant Behemoth. Also used by Volatile elites.
	sp                  Yes             Attack multiplier used by Shattered Mirror. Usually 0. Set to 1 when the item is used, 2 if Beating Embryo procs.
	sp_dur              Yes             Counts down by 1 each frame. When it reaches 0 ``sp`` is reset back to 0.
	spark               No              Item stack of Legendary Spark.
	hippo               Yes             Used by Dio's Friend. If the player dies while this is set to 1 they will be revived and the value will be reset to 0.
	fire_trail          No              Used by Burning Witness, Fireman's Boots, and blazing elites.
	fireshield          Yes             Item stack of Fire Shield.
	skull_ring          No              Item stack of Wicked Ring
	slow_on_hit         No              Item stack of Prison Shackles.
	freeze              No              Item stack of Permafrost.
	cdr                 No              Cooldown reduction. Used by Alien Head. Ranged from 0 to 1, where 1 is 100% reduction.
	icerelic            Yes             Item stack of Frost Relic. Does not exist on non-player actors.
	laserturbine        Yes             Item stack of Laser Turbine. Does not exist on non-player actors.
	turbinecharge       Yes             | Current Laser Turbine charge. When this reaches 100 the laser is fired. This value is increased by ``laserturbine`` multiplied by 0.13 every frame the player's ``activity`` is any value other than 0, 30, or 95.
	                                    | Does not exist on non-player actors.
	deskplant           Yes             Item stack of Interstellar Desk Plant. Does not exist on non-player actors.
	feather             Yes             Item stack of Hopoo Feather. Does not exist on non-player actors.
	jump_count          Yes             Number of midair jumps used up with Hopoo Feather. Can jump again midair if this is less than ``feather``. Does not exist on non-player actors.
	fireworks           Yes             Item stack of Bundle of Fireworks. Does not exist on non-player actors.
	================    ============    ====


Deprecated Variables
--------------------
	
	.. warning:: | Do not use these.
	             | They are only listed here to make old code easier to understand and no longer have any effect.
	
	These are variables that were previously used by systems which were overhauled. These variables still exist in instances but will have no effect.
	
	================    ====
	elite               | This number is very situational.
	                    | For "classic" enemies this value is set to 2 when the enemy is an elite, otherwise it's 0.
						  For other bosses, this value is usually 2 and is only set to 1 when the boss has just spawned and is an elite.
	elite_tier          | This variable contains the type of elite a "classic" enemy is. A value of -1 means not elite, anything from 0 to 9 is a normal elite, and anything greater than 9 is a blighted enemy.
	                    | For any non-classic type enemy this value will always be -1.
	================    ====

**************
Vanilla Sounds
**************

The following is a list of all the sounds provided by the game, and a short description of each one:

====================    ===========
Sound                   Description
====================    ===========
Achievement             Unlocking an achievement.
BisonDeath              Bighorn Bison's death sound.
BisonHit                Bighorn Bison's hit sound.
BisonShoot1             Bighorn Bison's attack sound.
BisonSpawn              Bighorn Bison's spawn sound.
Blastdoor               Large doors opening in the final level.
BoarDeath               Toxic Beast's death sound.
BoarExplosion           Toxic Beast's stampede sound.
BoarHit                 Toxic Beast's hit sound.
BoarMDeath              Boarlit's death sound.
BoarShoot1              Toxic Beast's sound when spawning Boarlits. Doubles as its spawn sound.
Boss1Shoot1             Providence's attacking sound.
Boss1Shoot2             Providence's ground stab attack sound.
BossSkill2              Unused sound from an early version of Providence attacking. Slightly longer version of the Lightning sound.
BubbleShield            Sound used by Shield Generator.
Bullet1                 Shooting sound used by Commando's Double Tap.
Bullet2                 Shooting sound used by various actors, such as Commando's Full Metal Jacket.
Bullet3                 Shooting sound used by various actors, such as Engineer's Auto-Turrets.
Casing                  Sound of bullet casing dropping used by Sniper's Snipe.
ChainLightning          Electric sound effect, such as making contact with a Jellyfish.
Chat                    Sound used when text displays during a cutscene, or when a player sends a chat message.
ChefShoot2_1            Sound used by CHEF's Sear / Blaze.
Chest0                  Sound made by Containers and most active items in the final level.
Chest1                  Sound made by Small Chests / Shops, as well as Keycard doors and Gun Chests in the final level.
Chest2                  Sound made by Large Chests and Roulette Chambers (choosing the item), as well as the active chest item in the final level.
Chest5                  Sound made by Golden Chests.
ChildDeath              Child / Parent's death sound.
ChildGShoot1            Parent's attack sound.
ChildHit                Child / Parent's hit sound.
ChildShoot1             Child's attack sound.
ClayDeath               Clay Man's death sound.
ClayHit                 Clay Man's hit sound.
ClayShoot1              Clay Man's attack sound.
ClaySpawn               Clay Man's spawn sound.
Click                   Sound used for option selection in any menu, as well as rotating items in Roulette Chambers.
Coin                    Sound used by coins.
Coins                   Sound used by Gold Bomb.
CowboyShoot1            Sound used by Bandit's Blast.
CowboyShoot2            Sound used by Bandit's Dynamite Toss.
CowboyShoot4_1          Sound used by Bandit's Lights Out (charging).
CowboyShoot4_2          Sound used by Bandit's Lights Out (shooting).
CrabDeath               Sand Crab's death sound.
CrabSpawn               Sand Crab's spawn sound.
Crit                    Sound used when an attack crits.
Crowbar                 Sound used by an attack when it procs with a Crowbar.
CutsceneAlarm           Alarm sound used in the opening cutscene.
CutsceneJet             Ship sound used in the opening cutscene.
CutscenePass            Sound used in the opening cutscene.
Difficulty              Sound used when the difficulty increases in-game.
Doll                    Sound used by Lost Doll.
Drill                   Sound used by Heaven Cracker.
Drone1Spawn             Sound used when repairing a broken drone.
DroneDeath              Sound used when a drone breaks.
EfMushroom              Sound used by Bustling Fungus.
Embryo                  Sound used by Beating Embryo.
Error                   Sound used when interacting with an object without having the required amount of gold.
ExplosiveShot           Sound used by Brilliant Behemoth.
FeralShoot1             Sound used by Acrid's Festering Wounds.
FeralShoot2             Sound used by Acrid's Neurotoxin and Epidemic / Pandemic.
Frozen                  Sound used when an actor gets frozen.
Geyser                  Sound used by Jump Pads.
GiantJellyExplosion     Wandering Vagrant's attack sound.
GiantJellyHit           Wandering Vagrant's hit sound.
GolemAttack1            Rock Golem's attack sound.
GolemDeath              Rock Golem's death sound.
GolemHit                Rock Golem's hit sound.
GolemSpawn              Rock Golem's spawn sound.
GuardDeath              Temple Guard's death sound.
GuardHit                Temple Guard's hit sound.
GuardSpawn              Temple Guard's spawn sound.
HeavyShoot1             Unused character Heavy's attack sound.
Hitlist                 Sound used by The Hit List, and Archer Bug's attack sound.
HuntressShoot1          Sound used by Huntress' Strafe.
HuntressShoot3          Sound used by Huntress' Blink.
ImpDeath                Every Imp variant's death sound.
ImpGShoot1              Imp Overlord / Imp Vanguard's attack sound.
ImpHit                  Every Imp variant's hit sound.
ImpShoot1               Black Imp / Purple Imp's attack sound.
ImpShoot2               Every Imp variant's teleport sound.
JanitorShoot1_1         Sound used by HAN-D's HURT (charging).
JanitorShoot1_2         Sound used by HAN-D's HURT (hitting).
JanitorShoot2_1         Sound used by HAN-D's DRONE (gaining drone).
JanitorShoot2_2         Sound used by HAN-D's DRONE (firing drone).
JanitorShoot4_1         Sound used by HAN-D's FORCED_REASSEMBLY (charging).
JanitorShoot4_2         Sound used by HAN-D's FORCED_REASSEMBLY (hitting).
JarSouls                Sound used by Jar of Souls.
JellyDeath              Jellyfish / Wandering Vagrant's death sound.
JellyHit                Jellyfish's hit sound.
LevelUp                 Sound of leveling up.
LevelUpWar              Sound used by Warbanner.
Lightning               Sound of lightning used by the Artifact of Origin when it spawns the Imp Portal, as well as the resulting Imp Vanguard.
LizardDeath             Lemurian's death sound.
LizardGDeath            Elder Lemurian's death sound.
LizardGHit              Elder Lemurian's hit sound.
LizardGShoot1           Elder Lemurian's attack sound.
LizardGSpawn            Elder Lemurian's spawn sound.
LizardHit               Lemurian's hit sound.
LizardShoot1            Lemurian's attack sound.
LizardSpawn             Lemurian's spawn sound.
Mine                    Sound used by Instant Minefield and Sticky Bombs, as well as Engineer's Bounding Mine.
MinerShoot1             Sound used by Miner's Crush.
MinerShoot2             Sound used by Miner's Drill Charge.
MinerShoot3             Sound used by Miner's Backblast.
MinerShoot4             Sound used by Miner's To The Stars / Starbound.
MissileLaunch           Sound of missiles firing, used by many actors and items.
MS                      Sound used when teleporting to Boar Beach.
Mush                    Unused, slightly more silent version of MushHit.
MushDeath               Mushrum's death sound.
MushHit                 Mushrum's hit sound.
MushShoot1              Mushrum's attack sound.
MushSpawn               Mushrum's spawn sound.
NautShoot1              Whorl's attack sound.
Pickup                  Sound used when picking up items, as well as selecting some menu options.
PigShoot1               Unused, higher pitch version of BoarShoot1.
PodDeath                Unused sound.
PodHit                  Unused sound.
PyroShoot1              Unused character Pyro's attack sound.
Reflect                 Sound used by Huntress' Laser Glaive bouncing off enemies.
Reload                  Sound used by Sniper's Snipe (reloading).
Revive                  Sound used by Dio's Best Friend, as well as when picking up an Artifact.
RiotGrenade             Sound used by Enforcer's Crowd Control / Tear Gas.
RiotShoot1              Sound used by Enforcer's Riot Shotgun.
SamuraiShoot1           Sound used by Mercenary's Laser Sword.
SamuraiShoot2           Sound used by Mercenary's Whirlwind.
ScavengerHit            Scavenger's hit sound.
Shield                  Sound used by Guardian's Heart (on shield regen).
Shrine1                 Sound used by Shrines, when activated.
Smite                   Sound used by various Mines (on exploding), as well as Ifrit's attacks and Imp Vanguard's spawn sound.
Sniper                  Sound used by Sniper's Steady Aim (firing bullet).
Sniper2                 Sound used by Telescopic Scope, as well as the active gun item in the final level.
SniperShoot3            Sound used by Sniper's Snipe (firing bullet).
SpiderHit               Mechanical Spider's hit sound.
SpiderShoot1            Mechanical Spider's attack sound.
SpitterHit              Spitter's hit sound.
SpitterShoot1           Spitter's attack sound.
SpitterSpawn            Spitter's spawn sound.
Squeaky                 Sound used by Crudely Drawn Buddy.
Teleporter              Sound used when teleporting to another stage, as well as activating active items in the final level.
Use                     Healing "pop" sound, used by healing items and drones.
Watch                   Ticking clock sound, used by Unstable Watch when time stops.
WispBDeath              Ancient Wisp's death sound.
WispBShoot1             Ancient Wisp's attack sound.
WispDeath               Wisp's death sound.
WispGDeath              Greater Wisp's death sound.
WispGShoot1             Greater Wisp's attack sound.
WispHit                 Every Wisp variant's hit sound.
WispShoot1              Wisp's attack sound.
WispSpawn               Every Wisp variant's spawn sound.
WormBurning             Sound of fire burning around Magma Worm.
WormDeath               Magma Worm / Cremator's death sound.
WormExplosion           Sound used by various objects, such as Magma Worm, Captain's Brooch, Ifrit's walls and some cutscene sounds.
WormHit                 Every Worm variant's hit sound.
WormRoar                Magma Worm's roaring sound, at the beginning of every attack.
====================    ===========

In addition to all of these, music is available just like other sounds.

====================    ==================================    =========
Sound                   Track Name                            Stage / Usage
====================    ==================================    =========
musicStage1             Monsoon                               Desolate Forest
musicStage2             Aurora Borealis                       Dried Lake
musicStage3             Cyclogenesis                          Damp Caverns
musicStage4             Chanson d'Automne..                   Sky Meadow
musicStage5             Aurora Borealis (Duplicate)           Ancient Valley
musicStage6             25.3°N 91.7°E                         Sunken Tombs and credits
musicStage7             Chanson d'Automne.. (Duplicate)       Hive Cluster
musicStage8             Tropic of Capricorn                   Magma Barracks
musicStage9             Arctic Oscillation                    Temple of the Elders
musicStage10            Precipitation                         Providence battle
musicStage11            Coalescence                           Risk of Rain (stage)
musicBoss1a             Dew Point                             Random boss music for first 2 levels.
musicBoss1b             Surface Tension                       Random boss music for first 2 levels.
musicBoss1c             Moisture Deficit                      Random boss music for first 2 levels.
musicBoss2a             Hailstorm                             Random boss music for next 3 levels.
musicBoss2b             Double Fucking Rainbow                Random boss music for next 3 levels.
musicBoss2c             Tropic of Cancer                      Random boss music for next 3 levels.
musicTitle              Risk of Rain                          Title screen.
HitItSon                                                      Dancing golem easter egg.
====================    ==================================    =========

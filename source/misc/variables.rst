****************
Common Variables
****************

| This page lists common variable names found in several instance types.
| Also on this page are common alarm indexes which can be modified with the instance :ref:`alarm methods <alarm-methods>`.

For variables of actors, see the :doc:`common actor variables </misc/variables_actor>` page.


.. contents:: Table of Contents
   :local:
   
   
Items
=====

	These variables are found on all instances of item objects.

	============    ====
	Name            Description
	============    ====
	text1           The name of the item.
	text2           The pickup text of the item.
	used            Whether the item has been picked up already. If it has then the item's name and pickup text is displayed before the instance is destroyed.
	pVspeed         The vertical speed of the item. Increased each frame by ``pGravity``, resets upon hitting a solid.
	pGravity        How fast the item falls.
	f               Used for the item bobbing cosine wave.
	yy              The vertical offset of the item sprite. Set each frame to ``cos(f) * 2``.
	yo              The vertical offset of the item's text after it has been picked up. Increases by 0.1 each frame, when it reaches 30 the item instance is destroyed.
	item_dibs       The instance ID of the player who can pick up the item before anyone else.
	icount          Timer before the item can be picked up by players other than ``item_dibs`` in online coop.
	force_pickup    Used in online coop to sync item pickups.
	item_switch     Used by use items. Set to 1 when a player is standing next to the item and the prompt to pick it up is displayed. Set to 2 on the frame the item is swapped with another item.
	owner           Instance ID of the player who picked up the item. Used in online coop to decide whether the item should play a sound when picked up.
	sound_played    When this variable is equal to 0 and ``yo`` is not equal to 0 the item pickup sound is played and the variable is set to 1. In online coop, the sound will not be played if ``owner`` is not the local player.
	do_despawn      Whether the item should despawn. Equal to ``is_use`` by default.
	time            The remaining time until the item despawns if ``do_despawn`` is 1. Once this variable hits 0 the transparency of the item is decreased by 10% each frame until it hits 0 and is destroyed.
	is_use          Whether the item is a use item.
	============    ====
	
	Items also make use of a single alarm:
	
	===========    ====
	Alarm Index    Description
	===========    ====
	Alarm 0        Timer before the item can be picked up by players.
	===========    ====

Damagers
========

These are variables found on instances of damagers, including bullets and explosions.
	
General
------------
	
		==================    ====
		Name                  Description
		==================    ====
		damage                Real damage of the attack.
		damage_fake           Displayed damage of the attack. This is usually the ``damage`` with a little bit of random variance.
		critical              Critical hit flag. 1 or 0, other values may have unusual side effects. When changing this value, be sure to multiply ``damage`` and ``damage_fake`` by 2.
		parent                The instance ID of the instance that spawned the damager. If you want to get the parent, use ``DamagerInstance:getParent()`` instead.
		team                  The team of the damager. Damagers can only hit actors on a different team. If the team is ``"playerproc"`` it will also be unable to hit ``"player"`` actors; Same for ``"enemyproc"``.
		hit_number            The number of targets the damager has hit.
		specific_target       *Bullets only:* If this is set to an actor's instance ID, the bullet will only hit that actor and nobody else. Used by projectile attacks to deal damage to hit enemies.
		climb                 Upwards offset of damage text. Used by things such as the Commando's fourth ability to make sure damage numbers don't overlap.
		stun                  | Causes hit targets to become stunned. The stun lasts for 1.5 seconds multiplied by this value.
		                      | Used alongside ``stun_ef`` by Concussion Grenade.
		knockback             | Causes hit targets to be knocked away. A higher value means higher knockback.
		                      | Used alongside ``knockback_glove`` by Boxing Gloves.
		knockup               Causes hit targets to be knocked upwards. A higher value knocks them upwards farther.
		percent_hp            | Uses the hit enemy's health multiplied by this value as the minimum damage. Any extra damage gained is kept on subsequent hits.
		                      | A ``percent_hp`` of 0.5 means the attack will deal at least half the health of the hit target as damage.
		damage_degrade        *Bullets only:* Works with piercing bullets. The damage of the bullet is multiplied by this value when it hits an enemy. Used by the Sniper's second ability.
		sniper_bonus          Dealt as bonus damage multiplied by the attack's damage. A value of 0.5 will deal 50% of the attack's damage as bonus. Different damage text color if equal to 0.6 or not.
		refresh_x             *Bullets only:* Used by Bandit's Lights Out skill. Set to 2 when an enemy is killed, 3 when a boss is killed, and -1 if an enemy isn't killed.
		steam                 Replenishes HAN-D's third ability by this value multiplied by 6. Will crash on other characters.
		oil                   Used by CHEF. Applies the oil debuff to targets and spawns the oil effect.
		fire                  Also used by CHEF. If the attack hits an oiled enemy, an explosion will be created for 30% of the attack's damage and the oil effect will be removed.
		poison_dot            Poison damage over time used by Acrid and the Imp Vanguard.
		pregnate              Unused. Deals DoT and spawns an object which sticks to the target enemy, before exploding into a chunk of green nuggets.
		explosion_2           Unused. Meant to work like ``explosive_shot`` but doesn't work for some reason.
		snare                 Unused. Applies the ``snare`` debuff to hit targets; This value is used as the buff duration in seconds.
		blaze                 Unused. Fire damage over time; Deals 30% of the attack's damage multiplied by this value per tick.
		weather               Unused. The damager will only be able to hit actors with ``weather_immune`` set to 0, and when an actor is hit its ``weather_immune`` value will be set to 240.
		==================    ====

Item Related
------------
	
		==================    ====
		Name                  Description
		==================    ====
		explosive_shot        Explosion effect of Brilliant Behemoth and volatile elites. The damage of the explosion is 20% of the attack's damage multiplied by this.
		lifesteal             Used by Leeching Seed and leeching elites. The healing calculation is ``(lifesteal + 1) * (log10(hit_number) + 1)``.
		scythe                Used by Harvester's Scythe. Heals the parent when a critical attack hits. The healing calculation is ``8 + (2 * (scythe - 1))``.
		skull_ring            Decreases parent skill cooldowns by 60 each if the attack is critical and hits.
		crowbar               Parent's Item stack of Crowbar.
		crowbar_hit           | Flag for whether the Crowbar effect has procced yet.
							  | Used to make sure it doesn't proc twice on piercing bullets, otherwise the damage of the attack would increase with each proc.
		blaster               Hyper-Threader effect. Higher values increase the number of times fired lasers bounce off enemies. Set to 0 when an enemy is hit so only 1 laser is fired.
		sunder                Used by Shattering Justice. Gives hit targets one of the ``sunder`` debuffs; Higher values mean a higher debuff duration.
		wolfblood             Used by Predatory Instincts. Gives the parent one of the ``blood`` buffs when a critical attack hits. 
		axe                   Used by The Ol' Lopper. Causes attacks to become critical when hitting targets below a certain health percentage; Higher values increase the health percentage.
		slow_on_hit           Used by Prison Shackles. Applies the ``slow`` debuff to hit targets; The buff duration starts at 1 second and increases by half a second for every value after 1.
		gold_on_hit           Used by Pillaged Gold. When the attack hits something, this many coins are created.
		horn                  Used by Ifrit's Horn. Fires a projectile if the attack hits something; Higher values mean higher projectile damage.
		spark                 Used by Legendary Spark. Spawns a wisp explosion if the projectile hits something. Higher values cause more explosions to spawn.
		sticky                Used by Sticky Bomb. Sticks a bomb to the first hit enemy, and then sets the variable to 0; Higher values mean higher bomb damage.
		nugget                Used by Meat Nugget. Spawns 2 healing nuggets if the attack hits something. Nuggets heal for this value multiplied by 6.
		thallium              Used by Thallium. Applies the ``thallium`` debuff to hit targets. Higher values don't do anything.
		mortar                Used by Mortar Tube. Causes a mortar to be shot if the attack hits something. The damage of the mortar is the damage of the attack times 1.7 times this value.
		taser                 Used by Taser. Applies the ``snare`` debuff to hit targets; The buff duration starts at 1 second and increases by half a second for every value after 1.
		lightning             Used by Ukelele and overloading elites. Creates lightning on hit targets. This damage of the lightning is the damage of the attack times 0.3 times this value.
		missile               Used by AtG Missile Mk. 1. Creates a single missile if the attack hits.
		missile_tri           Used by AtG Missile Mk. 2. Creates three missiles if the attack hits.
		bleed                 Used by Rusty Blade. Causes damage over time for 4 ticks dealing 35% of the attack's damage each; Higher values increase the damage of each tick by 12%, though this is never used in the base game.
		scope                 Used by Telesopic Sight. Kills the first hit non-boss enemy and then sets the variable to 0 so it doesn't work again.
		freeze                Used by Permafrost. Hit "classic" enemies become frozen, stopping their movement and causing them to take more knockback.
		stun_ef               Used by Concussion Grenade. Causes a hit effect to be spawned when used alongside ``stun``.
		fear                  Used by Old Box. Causes hit targets to become feared and running away.
		taunt                 Used by Crudely Drawn Buddy. Causes hit targets to become confused and run randomly.
		knockback_glove       Used by Boxing Gloves. Causes the boxing glove punch effect when set to 1 and used with ``knockback``.
		plasma                Used by Plasma Chain. Attaches a plasma chain between the first hit target and the parent.
		gold_gun              Used by Golden Gun. The stack of the item. Power is based on ``gold``.
		gold                  Used with ``gold_gun``. The player's current gold is copied to this value when an attack is shot with it.
		dynamite_on_hit       Used by Dynamite Plunger. Spawns a dynamite stick on the parent when the attack hits something.
		poison                Used by Dead Man's Foot. Causes damage over time for 3 + ``poison`` ticks.
		==================    ====

.. _director-fields:

Director
========

	The director instance can be accessed via :ref:`misc.director <misc-director>`

	==================    ====
	Name                  Description
	==================    ====
	time_start            The number of seconds since the run started. Increased by 1 each second as long as time isn't stopped.
	points                The director's enemy spawning budget. Increases over time as well as whenever the teleporter or a blast door is activated.
	enemy_buff            Multiplier for the power of enemies and cost of chests. Increases every minute by an amount which depends on the current difficulty, also increased at the start of every stage.
	bonus_rate            | Increased by 5 while the teleporter is active. Activating a blast door increases it by 5 until the door opens when it decreases the value by 5 again. The value is reset to 1 at the beginning of each stage.
	                      | A higher value increases the rate the director spawns enemies and causes enemies to be spawned closer to the player.
	stages_passed         The number of stages cleared so far in the run. Starts at 0. The divine teleporter spawns when this is more than or equal to 4 at the start of the level.
	spawn_boss            | When set to 1, the director will spawn a boss, waiting to build up more points if it has to, and then reset the variable to 0. Used by the teleporter to ensure a boss is spawned.
	                      | The director may ignore this flag as long as another boss or blighted enemy exists.
	boss_drop             | Determines the quality of boss item drops. Initially set to 0 but changes to 1 when the first boss is killed.
	                      | Makes it so the first boss killed drops an item of at least uncommon rarity, but all boss drops from then onwards have the same chances as a common chest.
	ghost_count           | The number of ghosts currently spawned. Subtracted from the enemy count when the teleporter counts remaining enemies.
	                      | Should be increased by 1 when manually spawning ghosts by setting the actor's ``ghost`` variable.
	target_poi            The ID of the "POI" instance of the current player that the director is trying to spawn enemies on.
	count                 The number of remaining enemies of the selected type to be spawned. When enemies are spawned, the director tries to spawn them in groups if it can; This is the remaining size of those groups.
	card_choice           The ID of the selected enemy card to spawn.
	keycard_dropped       | The number of keycards spawned in the last stage. Used to make sure they stop dropping from enemies after you've collected all 4.
	                      | Resets each level even though it's only used in 1 stage.
	multikill             The number of enemies killed within the last frame. Saved to the player's stats.
	drone_count           The maximum number of drones alive at any one time in the run. Saved to the player's stats.
	average_item_value    | Compared to the player's ``item_value`` when force item sharing is enabled to determine whether they should be allowed to pick up items.
	                      | Recalculated each frame based on the ``item_value`` of all players.
	teleporter_x          Used for syncing the teleporter spawn in online coop. Not useful.
	teleporter_y          Used for syncing the teleporter spawn in online coop. Not useful.
	epic_teleporter       Used for syncing the teleporter spawn in online coop. Not useful.
	==================    ====

	The director also makes use of a couple alarms:
	
	===========    ====
	Alarm Index    Description
	===========    ====
	Alarm 0        | Counts down every second. Increases ``time_start`` by 1 if time isn't stopped.
	               | Also handles the minutely difficulty increase and artifact of Origin.
	Alarm 1        Enemy spawning timer. The director tries to spawn an enemy when this finishes counting down.
	===========    ====
	
.. _hud-fields:

HUD
===

	The HUD instance can be accessed via :ref:`misc.hud <misc-hud>`

	==========================    ====
	Name                          Description
	==========================    ====
	show_items                    Whether to display the player's items on the HUD.
	show_time                     Whether to display the current time, difficulty, and artifact of Kin display (if active) on the HUD.
	show_gold                     Whether to display the player's gold on the HUD.
	show_skills                   Whether to display the part of the HUD containing the player's level, health, skills, and use item.
	show_level_name               Whether to display the name of the level when entering it.
	show_boss                     Whether to dispay the boss healthbar at the top of the screen.
	artifact_lock_choice_alpha    Transparency of the skill lock overlay for artifact of Distortion, from 1 to 0.
	info_index                    Index of the player's skill the user is mousing over to display info of.
	title_alpha                   Transparency of the stage title when changing levels, from 1 to 0 but values greater than 1 are used.
	hp_bar_count                  Number of enemy healthbars displayed this frame, used to limit the number of healthbars displayed at any given moment.
	hit_sound_count               Number of enemy hit sound effects played this frame, used to make sure no more than 2 hit sounds are played each frame.
	second                        Number of seconds the player is into the run excluding, full minutes. No direct impact on game difficulty.
	minute                        Number of minutes the player is into the run. No direct impact on game difficulty.
	ssecond                       String version of ``second``, displayed on the HUD.
	sminute                       String version of ``minute``, displayed on the HUD.
	difficulty                    The difficulty title displayed on the HUD, like "Very Easy" or "I SEE YOU".
	objective_text                Text displayed at the bottom of the screen directing the player what to do, like "Find the teleporter."
	gold                          The player's current gold.
	total_gold                    Amount of gold the player has picked up throughout the whole run.
	boss_hp_color                 GML colour of the main part of the boss healthbar. Usually red, Providence uses purple. 
	==========================    ====

********************************
Online modding: How to sync mods
********************************

Introduction
============

Multiplayer in Risk of Rain
---------------------------

First off, it's important to know how Risk of Rain's multiplayer works: a multiplayer lobby is made up by various **clients**, one of them being the **host**: Risk of Rain follows a **client-server approach**.

Everytime an action is done by a client (triggering a teleporter, opening a chest, etc), it gets relayed to the host so it knows what has been done, which then sends that information to the other clients so that everyone is updated equally. This is known as **syncing**.

However, only vanilla interactions are synced (and some of them aren't well synced), which might be a problem for modding. This guide will hopefully help you with understanding how to setup packages and networking to sync your mod.

Does my mod need syncing?
-------------------------

As said before, not all mods require syncing. You should familiarize yourself with the concept of client-side and server-side code: simply put, there are times where your code can be run independently of other players, and thus does not have any data to be sent.

Some examples of client-side code include:
    - UI elements that depend on already synced elements (displays with enemy count, stage info, health bars, etc)
    - UI elements for custom stats (number of jumps, steps taken, etc)
    - Sprite swaps

Some examples of server-side code that have to be synced include:
    - Custom objects (Note: while creating an object is always going to need syncing, its behavior might not need syncing depending on what it does)
    - Complex survivor abilities (essentially, if it uses random elements)

Programming with networking: useful advices
-------------------------------------------

To start modding for multiplayer, you need to be aware of exactly that: **you're modding for multiplayer**. Your code needs to work in an environment where more than one client is going to be active at a time, which means you should be coding accordingly:
    - **Stop using global static variables for everything.** Seriously.
      These variables are those defined outside functions and not assigned to any instance. For example, don't store your total number of kills on a variable; set them on the Player instance instead (using accessors ``:set()`` and ``:get()``, or better yet, the ``:getData()`` table).
      There are exceptions, of course, but always think about whether you could set that variable on an instance or not.
    - **Your code is run by everyone**. Both clients and host. Always keep in mind that you're coding for both.
      For example, if your mod has to setup one-time only things (like creating map objects), make sure to only let the host access it (``if net.host then ...``).
    - **Keep it simple**. Don't send unnecessary information over packets.
      If you need to sync your custom object instances, only send the changing variables instead of sending the whole instance over.

Enabling multiplayer compatibility
----------------------------------

To make your mod multiplayer-compatible, simply add the field ``mpcompat`` to your ``metadata.json`` mod file, and set it to ``true``:

.. code-block:: json
    
    {
        "mpcompat": true
    }
    
Packets
=======

What is a packet?
-----------------

To be able to sync variables properly, you are going to need to create **packets**. These are your way of communicating between different clients.

You can think of packets as types of letters the clients are going to be sending to one another. Each letter is identified by a name and a recipient, and contains the variables the sender wants to send to the recipient.

The recipient, upon receiving the letter, knows that it needs to run the function identified by the name in the letter, with the variables received as parameters.

Creating a packet
-----------------

You can create a packet by giving it a unique name, along with a function:

::
    
    local myPacket = net.Packet("My Packet Name", myPacketFunction)
    
The parameters for the function on your packet define what data you're going to be syncing. However, note that the first parameter will always be the player who sent the packet. Therefore, if you're planning on syncing three different variables, define a function that takes *four* variables.

Sending a packet
----------------

Packets can be sent by both the clients and the host. However, since the game follows a client-server approach, there are some limitations to keep in mind:
    - Clients can only send packets to the host
    - Host can send packets to one client, all clients, or all except one client

Clients can send their packets as follows:
::
    
    myPacket:sendAsClient(parameter1, parameter2, ...)
    
    
The host can send packages as follows:
::
    
    myPacket:sendAsHost(recipient, player, parameter1, parameter2, ...)
    
    
where the first two parameters depend on your desired recipient:
    - If targetting only one client, ``recipient`` is ``net.DIRECT`` and ``player`` is the ``PlayerInstance`` of the client to target;
    - If targetting every client, ``recipient`` is ``net.ALL`` and ``player`` is not needed (you should set it to ``nil``)
    - If targetting all clients except one, ``recipient`` is ``net.EXCLUDE`` and ``player`` is the ``PlayerInstance`` of the client to ignore.

Syncing information
-------------------

More often than not, you'll want to sync one or more variables across all players; however, only the host has the ability to send a packet to everyone. This usually means that syncing a value across all players is a two-step process for clients:
    1. Client sends a packet to the host
    2. Host gets the value and does something with it, and sends the same value to all clients except the original sender

Of course, if you're a host, you only need to send the value to all clients.

Practical examples
==================

Syncing custom instances
------------------------

| The idea behind syncing custom objects is that you should **only sync what needs to be synced**.
| Take, for example, this custom ball object.

::
    
    ballObject:addCallback("step", function(self)
        local t = self:getData()
        if t["speedX"] ~= nil or t["speedY"] ~= nil then
            -- do physics
        end
    end)
    
    registercallback("onPlayerStep", function(player)
        local inst = ballObject:findNearest(player.x, player.y)
        local t = inst:getData()
        if player:collidesWith(inst, player.x, player.y) then
            t["speedX"], t["speedY"] = 1 + math.random() * 2, 1 + math.random() * 2
        end
    end)
    

This object is shot at a random direction with a random velocity whenever a player hits the ``A`` key when colliding with it.
You might be tempted to sync it by making the host send both its X and Y coordinates every step with a packet. It *would* work, only you'd be sending a whopping 60 packets per second. This might not seem a lot, given that you're only sending two values, but you would be **flooding** the other clients with packets (think "denial of service").
Not only that, but Risk of Rain has pretty bad networking as-is. Keeping your mod lightweight syncing-wise will benefit people playing from far away distances.
Also, keep in mind that your mod users will probably be using other mods as well. If every mod is this intensive packet-wise, it's very likely for the clients to be extremely delayed, making the gameplay awful.

| So, what's the ideal solution? Simple: create a packet that is sent whenever a player interacts with the ball!
| Assuming there is more than one ball present at a time, an identifier for that instance also needs to be sent:

::
    
    local ballPacket = net.Packet("Shooting Ball Packet", function(player, ballId, speedX, speedY)
        local list = ballObject:findMatching("ballId", ballId)
        if #list > 0 then
            local inst = list[1]
            local t = inst:getData()
            t["speedX"], t["speedY"] = speedX, speedY
            if net.host then
                ballPacket:sendAsHost(net.EXCLUDE, player, ballId, speedX, speedY)
            end
        end
    end)

    registercallback("onPlayerStep", function(player)
        if not net.online or net.localPlayer == player then
            local inst = ball:findNearest(player.x, player.y)
            local t = inst:getData()
            if player:collidesWith(inst, player.x, player.y) then
                t["speedX"], t["speedY"] = 1 + math.random() * 2, 1 + math.random() * 2
                if net.host then
                    ballPacket:sendAsHost(net.ALL, nil, inst:get("ballId"), t["speedX"], t["speedY"])
                else
                    ballPacket:sendAsClient(inst:get("ballId"), t["speedX"], t["speedY"])
                end
            end
        end
    end)
    

*Note:* while it is true that ``sendAsClient`` and ``sendAsHost`` do nothing if you're not a client/host respectively, it's always good practice to surround calls with a check (``if net.host then ...``).
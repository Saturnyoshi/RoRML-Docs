****************
ModLoader Module
****************

The ``modloader`` module is used to get information about the current profile and loaded mods.

.. contents:: Table of Contents
   :local:

Launch Settings
===============

``modloader.getProfileName()``
-------------------------------
	
	Gets the name of the currently loaded profile.

	**Returns:**
	
		The name of the loaded profile.
	
	**Example:**
		::
			
			if modloader.getProfileName() == "Secret Profile Name" then
				-- unlock secret easter egg features
			end
			
		This can be used for a multitude of things, including hiding secret fun eastereggs for putting in specific profile names.
   
``modloader.getSaveName()``
-------------------------------
	
	Gets the name of the currently selected save file.

	**Returns:**
	
		The name of the loaded save file.
	
	**Example:**
		::
			
			print("Save name: "..modloader.getSaveName())
			
		Prints the save name.
   
Flags
=====

Flags are a way for the end user to affect mod behavior directly from the launcher.

There are several built-in flags used to toggle useful legacy behavior and bugs, a full list of them can be found :doc:`here </misc/flags>`.

``modloader.getFlags()``
-------------------------------
	
	Returns a table of all current flags.

	**Returns:**
	
		Returns a numerically indexed table of strings defined in the launcher.
	
	**Example:**
		::
			
			flags = modloader.getFlags()
			if _, v in ipairs(flags) do
				print(v)
			end
			
		Prints all currently active flags to the console.
		
``modloader.checkFlag(flag)``
-------------------------------
	
	Used to check whether a specific flag exists or not.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``flag``        string      The flag to check for.
		===========     ======      ===========

	**Returns:**
	
		Returns true if the flag exists.
	
	**Example:**
		::
			
			local bounce = modloader.checkFlag("fruits_should_bounce")
			
		Stores whether the flag ``fruits_should_bounce`` has been set in the current profile in the ``bounce`` variable.
		
Mod Info
========

``modloader.getMods()``
-------------------------------
	
	Gets a list of all loaded mods.

	**Returns:**
	
		Returns a numerically indexed table of the internal names of all loaded mods.
	
	**Example:**
		::
			
			print("Loaded mods: "..tostring(#modloader.getMods()))
			
		Prints the number of loaded mods.

``modloader.checkMod(mod)``
-------------------------------
	
	Checks whether a mod is loaded or not.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``mod``         string      The mod's internal name.
		===========     ======      ===========

	**Returns:**
	
		Returns true if the mod is loaded.
	
	**Example:**
		::
			
			if modloader.checkMod("example-mod") then
			    -- ENABLE EXTRA FUNCTIONALITY
			end
			
		This provides a way to add cross-mod functionality by checking if another mod is loaded or not.
		
``modloader.getModName(mod)``
-------------------------------
	
	Gets the full name of the mod.
	
	Calling this function on a mod that isn't loaded will cause an error.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``mod``         string      The mod's internal name.
		===========     ======      ===========

	**Returns:**
	
		The display name of the mod.
	
	**Example:**
		::
			
			for _, mod in ipairs(modloader.getMods()) do
			    print(modloader.getModName(mod))
			end
			
		Prints the name of each loaded mod.
		
``modloader.getModDescription(mod)``
------------------------------------
	
	Gets the description of the mod.
	
	Calling this function on a mod that isn't loaded will cause an error.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``mod``         string      The mod's internal name.
		===========     ======      ===========

	**Returns:**
	
		The description of the mod.
		
``modloader.getModVersion(mod)``
------------------------------------
	
	Gets the version of the mod.
	
	Calling this function on a mod that isn't loaded will cause an error.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``mod``         string      The mod's internal name.
		===========     ======      ===========

	**Returns:**
	
		Returns the version of the mod, as a string.
		
``modloader.getModAuthor(mod)``
------------------------------------
	
	Gets the author of the mod.
	
	Calling this function on a mod that isn't loaded will cause an error.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``mod``         string      The mod's internal name.
		===========     ======      ===========

	**Returns:**
	
		Returns the author of the mod.
		
Miscellaneous
=============

``modloader.getActiveNamespace()``
----------------------------------
	
	Returns the namespace that new objects will be added to.
	
	This will always match the internal name of the current mod.

	**Returns:**
	
		The current namespace as a string.
	
	**Example:**
		::
			
			print(modloader.getModName(modloader.getActiveNamespace()))
			
		Prints the name of the mod running the code.
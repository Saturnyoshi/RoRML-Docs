***********
Misc Module
***********

The ``misc`` module is used to manipulate some miscellaneous game functionality.

.. contents:: Table of Contents
   :local:
   
Functions
=========

``misc.setRunSeed(seed)``
--------------------------------------------------
	
	Sets the level seed of the *next* run.
	
	The seed determines the level and location of objects but does not currently affect item RNG.

	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``seed``         number       The seed. Must be an integer value greater than 0.
		============     =======      ===========

``misc.damage(damage, x, y, critical, color)``
--------------------------------------------------
	
	Creates damage text.
	
	**Parameters:**
	
		============     =======      ===========
		Parameter        Type         Description
		============     =======      ===========
		``damage``       number       The value to be displayed in the damage text.
		``x``            number       The x position for the damage text to be created at.
		``y``            number       The y position for the damage text to be created at.
		``critical``     boolean      Whether the damage text should appear like a critical hit does or not.
		``color``        Color        The :doc:`Color </class/color>` of the damage text.
		============     =======      ===========

	**Example:**
		::
			
			misc.damage(200, target.x, target.y, false, Color.ORANGE)
			
		This would create a damage text displaying the number 200 in the color orange.
		
``misc.shakeScreen(frames)``
----------------------------

	Causes screenshake for the specified number of frames.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``frames``      number      The number of frames to shake the screen for.
		===========     ======      ===========

``misc.getIngame()``
------------------------

	**Returns:**
	
		Returns false when called in menus, otherwise true.

``misc.getOption(name)``
------------------------

	**Returns:**
	
		Gets the value of one of the game's configuration options.
		
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The option to get. See below for a list of options
		===========     ======      ===========
	
		====================     =======     =====================
		Name                     Type        Description
		====================     =======     =====================
		video.quality            number      The graphics quality. 1 is low, 2 is medium, and 3 is high.
		video.fullscreen         boolean     Whether the game is in fullscreen mode
		video.scale              number      The currently selected zoom scale.
		video.hud_scale          number      The currently selected HUD scale. Usually the same as video.scale. Can only be set through the console.
		video.show_money         boolean     When set to false coins and exp are hidden. Can only be set through the console.
		video.show_damage        boolean     When set to false damage indicators are not created.
		video.frameskip          boolean     When set to true draw events are only called every other frame.
		video.vsync              boolean     Whether vertical synchronization is enabled.
		general.volume           number      The sound effects volume. Ranges from 0 at 0% to 1 at 100%.
		general.music_volume     number      The music volume. Ranges from 0 at 0% to 1 at 100%.
		====================     =======     =====================

	**Example:**
		::
		
			if misc.getOption("video.quality") == 3 then
				-- Do something
			end
		
		Checks if the graphics quality is set to high and then does something.
		Useful for allowing the player to disable expensive visual effects.
		
``misc.setGold(value)``
-------------------------------
	
	Sets the amount of gold the player has.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       number      The value to set the player's gold to.
		===========     ======      ===========
	
	**Example:**
		::
			
			misc.setGold(misc.getGold() + 10)
			
		Increases the player's gold by 10.

``misc.getGold()``
-------------------------------
	
	**Returns:**
	
		Returns the amount of gold the player has.
	
	**Example:**
		::
			
			if misc.getGold() >= 1000 then
			    -- Do something
			end
			
		Checks if the player has at least 1000 gold and then does something.

``misc.setTimeStop(value)``
-------------------------------
	
	When time is stopped, enemies are frozen and a ticking sound is played.
	
	This mechanic is used in the base game by the items Unstable Watch and Time Keeper's Secret.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       number      The number of steps to stop time for.
		===========     ======      ===========
	
	**Example:**
		::
			
			misc.setTimeStop(6 * 60)
			
		Stops time for 6 seconds.

``misc.getTimeStop()``
-------------------------------
	
	**Returns:**
	
		Returns the remaining frames time is stopped for. If 0 then time isn't stopped.
	
	**Example:**
		::
			
			if misc.getTimeStop() == 0 then
				-- Do something
			end
			
		Checks to make sure time isn't stopped before executing code. Useful for custom enemy behavior.
		
``misc.getTime()``
-------------------------------
	
	Get the time on the in-game clock.

	**Returns:**
	
		Returns two numbers; the minutes and the seconds.
	
	**Example:**
		::
			
			local m, s = misc.getTime()
			if m == 20 and s == 20 then
			    -- Do something
			end
			
		Does something when the time displayed on the clock is 20:20.

``misc.fireBullet(x, y, direction, distance, damage, team, [hitSprite], [properties])``
---------------------------------------------------------------------------------------
	
	Creates a hitscan bullet.

	| This function is similar to the :ref:`ActorInstance:fireBullet <actorinstance-bullet>` method with a few key differences:
	| Exact damage is specified, team is specified, and the function can be called without an actor.

	**Parameters:**
	
		==============     ======      ===========
		Parameter          Type        Description
		==============     ======      ===========
		``x``              number      Horizontal origin of the bullet.
		``y``              number      Vertical origin of the bullet.
		``direction``      number      Direction the bullet "travels" in.
		``distance``       number      Max distance of the bullet.
		``damage``         number      Damage of the bullet.
		``team``           string      The team to create the bullet on. Usually ``"player"``, ``"enemy"``, or ``"neutral"``.
		``hitSprite``      Sprite      *Optional:* The sprite of the sparks spawned when the damager hits something, if left nil then sparks won't be created.
		``properties``     number      *Optional:* A combination of damager property constants, as seen below, added together.
		==============     ======      ===========
		
	**Damager Property Constants:**
		
		======================    ======
		Constant                  Effect
		======================    ======
		DAMAGER_BULLET_PIERCE     Allows the bullet to pierce through any number of enemies, like the Commando’s FMJ.
		======================    ======
		
	**Returns:**
	
		Returns the created :doc:`DamagerInstance </class/instance>`.
	
	**Example:**
		::
			
			misc.fireBullet(x, y, 180, 400, 30, "neutral", hitSprite)
			
		Fires a bullet from the position (``x``, ``y``) traveling 400 pixels left, dealing 30 damage, and on the team ``"neutral"``.

``misc.fireExplosion(x, y, width, height, damage, team, [explosionSprite], [hitSprite], [properties])``
--------------------------------------------------------------------------------------------------------
	
	Creates an explosion.

	| This function is similar to the :ref:`ActorInstance:fireExplosion <actorinstance-explosion>` method with a few key differences:
	| Exact damage is specified, team is specified, and the function can be called without an actor.
	
	**Parameters:**
	
		===================     ======      ===========
		Parameter               Type        Description
		===================     ======      ===========
		``x``                   number      Horizontal origin of the explosion.
		``y``                   number      Vertical origin of the explosion.
		``width``               number      The width of the explosion, as a multiple of 19.
		``height``              number      The height of the explosion, as a multiple of 4.
		``damage``              number      The damage of the explosion.
		``team``                string      The team to create the explosion on. Usually ``"player"``, ``"enemy"``, or ``"neutral"``.
		``explosionSprite``     Sprite      *Optional:* The animation used by the explosion itself, if left nil then the explosion won't be visible
		``hitSprite``           Sprite      *Optional:* The sprite of the sparks spawned when the damager hits something, if left nil then sparks won't be created.
		===================     ======      ===========
		
	**Returns:**
	
		Returns the created :doc:`DamagerInstance </class/instance>`.
	
	**Example:**
		::
			
			misc.fireExplosion(target.x, target.y, 3, 1.5, 25, "player", explosionSprite, hitSprite)
			
		Fires an explosion om the instance ``target`` on the team ``"player"`` which deals 25 damage.

		
Fields
======

``misc.players``
--------------------------

	| An array table containing each :doc:`PlayerInstance </class/playerInstance>` in order.
	| This table is updated at the start of every run.
	
	**Example:**
		::
		
			for _, v in ipairs(misc.players) do
				print(v:get("name"))
			end
			
		Prints the name of each player.
		
.. _misc-director:
		
``misc.director``
---------------------------

	| Contains an :doc:`Instance </class/instance>` of the director :doc:`GMObject </class/gmObject>` not otherwise accessible.
	| This field is replaced at the start of every run.
	
	
	For a list of the instance's variables, see the page on :ref:`common variables <director-fields>`.
	
	**Example:**
		::
		
			misc.director:set("enemy_buff", director:get("enemy_buff") + 0.3)
			
		Increases the strength of enemies by 30% of their starting strength.
	
.. _misc-hud:

``misc.hud``
---------------------------

	Alias: ``misc.HUD``

	| Contains an :doc:`Instance </class/instance>` of the HUD :doc:`GMObject </class/gmObject>` not otherwise accessible.
	| This field is replaced at the start of every run.
	

	For a list of the instance's variables, see the page on :ref:`common variables <hud-fields>`.
	
	**Example:**
		::
		
			misc.hud:set("show_gold", 0)
			
		Hides the part of the HUD that displays the player's gold.
	


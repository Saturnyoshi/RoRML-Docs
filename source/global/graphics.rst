***************
Graphics Module
***************

The ``graphics`` module is used to draw things to the screen, whether it's an image, shape, or text.

.. note::
	| Drawing functions can only be used either during a drawing callback, or to a surface.
	| Drawing functions will have no effect when called during a normal callback.

.. contents:: Table of Contents
   :local:

Functions (Images)
==================

``graphics.drawImage{...}``
------------------------------------------------

	Draws a :doc:`Sprite </class/sprite>` or :doc:`Surface </class/gmObject>`.
	
	The color, transparency, and scaling of the image can all be specified using optional arguments.
	
	**Parameters:**
	
		This function uses named arguments, see below for examples.
	
		==============  =================  ===========
		Parameter       Type               Description
		==============  =================  ===========
		``image``       Sprite or Surface  *Required:* The image to be drawn. Also may be the element at index 1.
		``x``           number             *Required:* The X position to draw at. Also may be the element at index 2.
		``y``           number             *Required:* The Y position to draw at. Also may be the element at index 3.
		``subimage``    number             Only applies to sprites. The frame of the sprite to draw, starting from 1. Also may be the element at index 4.
		``color``       Color              Alias: ``colour``. The color to tint the image with.
		``solidColor``  Color              Alias: ``solidColour``. If this value is supplied, then the sprite will be drawn entirely with this color. Takes priority over ``color``.
		``alpha``       number             The transparency of the image, as a decimal value from 0 to 1.
		``angle``       number             The rotation of the image in degrees, counterclockwise.
		``width``       number             The width to draw the image as in pixels.
		``height``      number             The height to draw the image as in pixels.
		``xscale``      number             Horizontal size multiplier. Stacks with ``width``.
		``yscale``      number             Vertical size multiplier. Stacks with ``height``.
		``scale``       number             Horizontal and vertical size multiplier. Stacks with all previous scale modifying parameters.
		``region``      table              The region of the sprite to draw, defined as a table representing the top-left corner of a rectangle and its width and height: ``{x, y, width, height}``.
		==============  =================  ===========
	
	**Examples:**
		::
		
			graphics.drawImage{
			    image = myImage,
			    x = 50,
			    y = 50
			}
			
		::
		
			graphics.drawImage{myImage, 50, 50}
			
		::
		
			myImage:draw(50, 50)
			
		All 3 of these examples do the same thing, but decrease in complexity each time.
		The second example utilizes the numerical aliases of the first few parameters.
		
		Generally, simple image drawing is more of a use case for the image's draw method.
		
		::
		
			graphics.drawImage{myImage, 50, 50,
			    alpha = 0.5,
			    color = Color.RED,
			    xscale = 1.5
			}
		
		Draws ``myImage`` at 50% opacity, tinted red, and stretched by 50% horizontally.

Functions (Setters)
===================

``graphics.alpha(value)``
------------------------------

	Sets the transparency which shapes will be drawn at.
	A ``value`` of 1 is fully opaque, while 0 is fully transparent.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       number      The new alpha value, from 0 to 1.
		===========     ======      ===========
	
	**Examples:**
		::
			
			graphics.alpha(0.3)
			graphics.rectangle(x, y, x + 50, y + 50, false)
			graphics.alpha(1)
			
		Draws a 50x50 square with 30% opacity at the coordinates stored in ``x`` and ``y``, and then resets the alpha back to 1 when finished.
		
``graphics.getAlpha()``
------------------------------
	
	**Returns:**
	
		The drawing alpha as set by ``graphics.alpha``.

``graphics.color(value)``
------------------------------

	Alias: ``graphics.colour(value)``

	Sets the :doc:`color </class/color>` which shapes will be drawn with.
	
	This should always be set when you begin drawing shapes, as the initial color may vary from call to call.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       Color       The color to begin drawing in.
		===========     ======      ===========
	
	**Examples:**
		::
			
			graphics.color(Color.BLUE)
			graphics.rectangle(x, y, x + 50, y + 50, false)
			
		Draws a 50x50 blue square at the coordinates stored in ``x`` and ``y``.

``graphics.getColor()``
------------------------------
	
	Alias: ``graphics.getColour()``

	**Returns:**
	
		| The drawing alpha as set by ``graphics.color``.
		| Note that this will not be the same color *object*, but it will have the same RGB values as the set color.

``graphics.setTarget(target)``
-------------------------------
	
	Changes the render target to a :doc:`Surface </class/surface>`.
	
	**Parameters:**
	
		===========     =======      ===========
		Parameter       Type         Description
		===========     =======      ===========
		``target``      Surface      The surface to set the render target to.
		===========     =======      ===========
	
	**Example:**
		::
			
			graphics.setTarget(vine)
			
		Sets the render target to the surface called ``vine``.
	
``graphics.resetTarget()``
--------------------------

	Resets the render target to go back to drawing as normal.
	
	**Example:**
		::
		
			graphics.setTarget(vine)
			-- Some drawing functions
			graphics.resetTarget()
			
		After drawing to a surface it is important to reset the drawing target so that drawing can return to functioning as usual.

``graphics.getTarget()``
------------------------------
	
	**Returns:**
	
		| The current render target as set by ``graphics.setTarget`` and ``graphics.resetTarget``. 
		| Returns a :doc:`Surface </class/surface>` when one is being drawn to or nil if just drawing to the screen.

``graphics.setBlendMode(mode)``
-------------------------------

	Sets the current drawing blend mode.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``mode``     string   The blend mode to begin using.
		===========  =======  ===========
		
		All accepted blend modes:

		===========  =======
		Name         Description
		===========  =======
		normal       The default blend mode.
		additive     Luminosity values of light areas are added.
		subtract     Luminosity values of light areas are subtracted.
		max          Similar to additive blending.
		===========  =======
		
		*(Descriptions taken from the GMS2 documentation for gpu_set_blendmode)*

``graphics.setBlendModeAdvanced(source, dest)``
-----------------------------------------------

	Sets the current drawing blend mode.

	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``mode``     string   The blend mode to begin using.
		===========  =======  ===========
		
		All accepted blend modes:

		=====================  =======
		Name                   Blend Factor (R, G, B, A)
		=====================  =======
		zero                   (0, 0, 0, 0)
		one                    (1, 1, 1, 1)
		sourceColour           (Rs, Gs, Bs, As)
		sourceColourInv        (1-Rs, 1-Gs, 1-Bs, 1-As)
		sourceAlpha            (As, As, As, As)
		sourceAlphaInv         (1-As, 1-As, 1-As, 1-As)
		destColour             (Rd, Gd, Bd, Ad)
		destColourInv          (1-Rd, 1-Gd, 1-Bd, 1-Ad)
		destAlpha              (Ad, Ad, Ad, Ad)
		destAlphaInv           (1-Ad, 1-Ad, 1-Ad, 1-Ad)
		sourceAlphaSaturation  | (f, f, f, 1)
		                       | where f = min(As, 1-Ad)
		=====================  =======
		
		*(Blend factors taken from the GMS2 documentation for gpu_set_blendmode_ext)*

		The blend modes of ``graphics.setBlendMode`` can be replicated using the following combinations:

		===========  =====================  =====================
		Name         ``source``             ``dest``
		===========  =====================  =====================
		normal       sourceAlpha            sourceAlphaInv
		additive     sourceAlpha            one
		subtract     zero                   sourceColourInv
		max          sourceAlpha            sourceColourInv
		===========  =====================  =====================


Functions (Shapes)
==================

``graphics.rectangle(x1, y1, x2, y2, outline)``
------------------------------------------------

	Draws a rectangle between the coordinates ``x1``, ``y1`` and ``x2``, ``y2``.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x1``       number   The X position of the first corner of the rectangle.
		``y1``       number   The Y position of the first corner of the rectangle.
		``x2``       number   The X position of the second corner of the rectangle.
		``y2``       number   The Y position of the second corner of the rectangle.
		``outline``  boolean  *Optional:* Whether the rectangle should be drawn as an outline. Defaults to false.
		===========  =======  ===========
			
``graphics.circle(x, y, radius, outline)``
------------------------------------------------

	Draws a circle of specified radius centered on the coordinates ``x``, ``y``.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x``        number   The X position of the center of the circle.
		``y``        number   The Y position of the center of the circle.
		``radius``   number   The radius of the circle.
		``outline``  boolean  *Optional:* Whether the circle should be drawn as an outline. Defaults to false.
		===========  =======  ===========
		
``graphics.ellipse(x1, y1, x2, y2, outline)``
------------------------------------------------

	Draws an ellipse between the coordinates ``x1``, ``y1`` and ``x2``, ``y2``.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x1``       number   The X position of the first corner of the rectangle.
		``y1``       number   The Y position of the first corner of the rectangle.
		``x2``       number   The X position of the second corner of the rectangle.
		``y2``       number   The Y position of the second corner of the rectangle.
		``outline``  boolean  *Optional:* Whether the ellipse should be drawn as an outline. Defaults to false.
		===========  =======  ===========
		
``graphics.triangle(x1, y1, x2, y2, x3, y3, outline)``
-------------------------------------------------------

	Draws a triangle with a point landing on each of the 3 provided pairs of coordinates.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x1``       number   The X position of the first point of the triangle.
		``y1``       number   The Y position of the first point of the triangle.
		``x2``       number   The X position of the second point of the triangle.
		``y2``       number   The Y position of the second point of the triangle.
		``x3``       number   The X position of the third point of the triangle.
		``y3``       number   The Y position of the third point of the triangle.
		``outline``  boolean  *Optional:* Whether the triangle should be drawn as an outline. Defaults to false.
		===========  =======  ===========

``graphics.line(x1, y1, x2, y2, width)``
------------------------------------------------

	Draws a line between the points ``x1``, ``y1`` and ``x2``, ``y2``.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x1``       number   The X position of the first point of the line.
		``y1``       number   The Y position of the first point of the line.
		``x2``       number   The X position of the second point of the line.
		``y2``       number   The Y position of the second point of the line.
		``width``    number   *Optional:* The width of the line in pixels. Defaults to 1.
		===========  =======  ===========
		

``graphics.pixel(x, y)``
------------------------------------------------

	Draws a single pixel at the provided coordinates.

	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``x``        number   The X position of the pixel.
		``y``        number   The Y position of the pixel.
		===========  =======  ===========

Functions (Text)
==================

.. _text-constants:

Text Drawing Constants
----------------------

	When using the ``graphics.print`` or ``graphics.printColor`` functions, there are a number of constants used when picking certain settings.
	Here you can find a list of all the text drawing related constants.

	All built-in fonts are as follows:

	==========================    ====
	Name                          Description
	==========================    ====
	graphics.FONT_DEFAULT         The most common font used for drawing.
	graphics.FONT_LARGE           Used for things such as the level name when entering a stage and the number of minutes on the in-game timer.
	graphics.FONT_SMALL           Used by RoRML in the command console.
	graphics.FONT_DAMAGE          Used by damage indicators and the player's health bar. Only contains numbers and a few miscellaneous characters.
	graphics.FONT_CRITICAL        Same as ``FONT_DAMAGE`` but has a red outline rather than a black one.
	graphics.FONT_MONEY           Used by the player's current gold display at the top left of the HUD.
	==========================    ====

	The following is a list of all halign settings:

	========================== =
	graphics.ALIGN_LEFT
	graphics.ALIGN_MIDDLE
	graphics.ALIGN_RIGHT
	========================== =

	The following is a list of all valign settings:

	========================== =
	graphics.ALIGN_TOP
	graphics.ALIGN_CENTER
	graphics.ALIGN_CENTRE
	graphics.ALIGN_BOTTOM
	========================== =

``graphics.print(text, x, y, [font, halign, valign])``
------------------------------------------------------

	Draws a string.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``text``     any      The text to be drawn. If the value is not a string then it will be converted to a human readable format. 
		``x``        number   The X coordinate to draw the text at.
		``y``        number   The Y coordinate to draw the text at.
		``font``     number   *Optional:* The ID of the font to use. Defaults to ``graphics.FONT_DEFAULT``. [#f1]_
		``halign``   number   *Optional:* Decides where the text is drawn relative to the X coordinate. Defaults to ``graphics.ALIGN_LEFT``. [#f1]_
		``valign``   number   *Optional:* Decides where the text is drawn relative to the Y coordinate. Defaults to ``graphics.ALIGN_TOP``. [#f1]_
		===========  =======  ===========
		
	.. [#f1] See :ref:`above <text-constants>` for all valid values.
	
	**Examples:**
		::
		
			graphics.print("Bonus: " .. tostring(bonus), 16, 128)
			
		| Displays a counter for the variable ``bonus`` at the coordinates (16, 128).
		| The font and alignment of the text is never specified, so it will be drawn using the standard font with the top left of the text at the specified coordinates.
		
		::
		
			graphics.print("[Target]", target.x, target.y - 32, graphics.FONT_SMALL, graphics.ALIGN_MIDDLE, graphics.ALIGN_BOTTOM)
			
		| Displays an indicator above the instance ``target``.
		| The text will be drawn with the small font, drawn above the Y coordinate, and centered around the X coordinate.

``graphics.printColor(text, x, y, [font])``
------------------------------------------------------

	Draws a string. Additionally supports Risk of Rain's Supports :doc:`colored text formatting </misc/coloredTextFormatting>`.
	
	When using this function, keep in mind that it is slower than using ``graphics.print`` and that the alignment of the text cannot be changed from top left.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``text``     string   The text to be drawn.
		``x``        number   The X coordinate to draw the text at.
		``y``        number   The Y coordinate to draw the text at.
		``font``     number   *Optional:* The ID of the font to use. Defaults to ``graphics.FONT_DEFAULT``. See :ref:`above <text-constants>` for all fonts.
		===========  =======  ===========
	
	**Example:**
		::
		
			graphics.printColor("&y&[New Record]&!& Enemies defeated: &b&" .. tostring(enemy_tally) .. "&!&", x, y)
			
		| Displays a message containing 3 parts:
		| The text ``[New Record]`` which will be drawn in yellow as indicated by the ``&y&`` tag.
		| The text ``Enemies Defeated:`` which will be drawn in the current drawing colour.
		| The value of variable ``enemy_tally`` which will be drawn in blue as indicated by the ``&b&`` tag.

``graphics.textWidth(text, font)``
------------------------------------------------------

	Returns the width of a string in a specific font as pixels.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``text``     any      The text to get the width of. 
		``font``     number   The ID of the font to use.
		===========  =======  ===========
	
	**Examples:**
		::
		
			local str = "Hello!"
			graphics.color(Color.BLACK)
			graphics.rectangle(16, 16, 16 + graphics.textWidth(str, graphics.FONT_DEFAULT), 16 + graphics.textHeight(graphics.FONT_DEFAULT))
			graphics.color(Color.WHITE)
			graphics.print(str, 16, 16)
			
		Draws the string ``str`` with a black rectangle behind it.
		
	**Returns:**

		The width of the text in pixels.
		
``graphics.textHeight(text, font)``
------------------------------------------------------

	Returns the height of a string in a specific font as pixels.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``text``     any      The text to get the width of. 
		``font``     number   The ID of the font to use.
		===========  =======  ===========

	**Returns:**

		The height of the text in pixels.
		
Functions (Fonts)
=================

``graphics.fontFromFile(fname, size, [bold, italic])``
------------------------------------------------------

	Loads a font from a file to be used with other drawing.

	Note that this will not work well with pixel fonts due to anti-aliasing.
	
	**Parameters:**
	
		===========  =======  ===========
		Parameter    Type     Description
		===========  =======  ===========
		``fname``    string   The name of a ``ttf`` file relative to the mod's directory. The file extension is optional. 
		``size``     number   The font size in pixels.
		``bold``     boolean  *Optional:* If true the font will be bold.
		``italic``   boolean  *Optional:* If true the font will be italic.
		===========  =======  ===========

	**Returns:**

		Returns the ID of the new font.

``graphics.fontFromSprite(sprite, characters, [separation, monospace])``
------------------------------------------------------------------------

	Creates a font from an existing sprite.

	**Parameters:**
	
		==============  =======  ===========
		Parameter       Type     Description
		==============  =======  ===========
		``sprite``      Sprite   The sprite with each character of the font as a subimage. 
		``characters``  string   A string containing all the characters in the font in order of subimage.
		``separation``  number   *Optional:* The distance in pixels between each character when rendered. Defaults to 1.
		``monospace``   boolean  *Optional:* If true then each character of the font will take the font's width, rather than being based on the width of the subimage. Defaults to false.
		==============  =======  ===========

	**Returns:**

		Returns the ID of the new font.

``graphics.fontDelete(font)``
-----------------------------

	Deletes a previously created font.
	Built-in fonts cannot be deleted and will cause an error if attempting to delete them.

	**Parameters:**
	
		==============  =======  ===========
		Parameter       Type     Description
		==============  =======  ===========
		``font``        number   The ID of the font to delete. 
		==============  =======  ===========

``graphics.fontIsValid(font)``
------------------------------

	Checks whether a font exists.
	Always returns false for non-numerical values.

	**Parameters:**
	
		==============  =======  ===========
		Parameter       Type     Description
		==============  =======  ===========
		``font``        any      The value to check as a valid font ID. 
		==============  =======  ===========

	**Returns:**

		``true`` or ``false`` depending on whether the font exists.
		
``graphics.fontReplace(original, new)``
---------------------------------------

	Replaces an existing font with another one.
	Can be used to overwrite built-in fonts.

	**Parameters:**
	
		==============  =======  ===========
		Parameter       Type     Description
		==============  =======  ===========
		``original``    number   The ID of the font to be overwritten.
		``new``         number   The ID of the font to replace ``original`` with.
		==============  =======  ===========		
		
Functions (Misc)
================

``graphics.getGameResolution()``
--------------------------------

	Returns the width and height of the game camera.
	
	**Returns:**
	
		Returns the width followed by the height.
		
	**Examples:**
		::
			
			callback.register("onDraw", function()
			    local w, h = graphics.getGameResolution()
			    graphics.color(Color.DARK_RED)
			    graphics.rectangle(w * 0.25, w * 0.25, w * 0.75, h * 0.75)
			end)

		Draws a dark red rectangle over the middle of the camera.

``graphics.getHUDResolution()``
--------------------------------

	Returns the width and height of the HUD layer.

	Useful for positioning UI elements correctly.

	This will be the same as ``getGameResolution`` unless the ``"hud_scale"`` option in prefs.json is modified.
	
	**Returns:**
	
		Returns the width followed by the height.
		
	**Examples:**
		::
			
			callback.register("onHUDDraw", function()
			    local w, h = graphics.getHUDResolution()
			    graphics.color(Color.DARK_RED)
			    graphics.rectangle(w * 0.25, w * 0.25, w * 0.75, h * 0.75)
			end)

		The same as the above example, but drawn to the HUD layer instead.

``graphics.bindDepth(depth, bind)``
-------------------------------------

	Used to draw things below or above other instances.
	
	Calls the specified function when the game is drawing at the specified depth.
	
	As the object controlling the function calls is an :doc:`Instance </class/instance>`, it can be modified and destroyed at any time to store the state of drawing or stop it.
	
	**Parameters:**
	
		===========  ========  ===========
		Parameter    Type      Description
		===========  ========  ===========
		``depth``    number    The depth the function will be called at.
		``bind``     function  The function to be called.
		===========  ========  ===========
	
	**Returns:**
	
		Returns the instance which controls the function calls.
		
	**Examples:**
		::
			
			local drawables = {}
			local function myFunction()
			    for _, v in ipairs(drawables) do
			        graphics.drawImage(v)
			    end
			end
			
			callback.register("onStageEntry", function()
			    graphics.bindDepth(-7, myFunction)
			end)
		
		Binds the function ``myFunction`` to be called each frame at depth -7, just behind players.
		
		We call ``graphics.bindDepth`` at the start of every stage since the instance is removed between stages.

		::
		
			local function drawEffect(handler, frame)
			    if frame >= 45 then
			        handler:destroy()
			    else
			        graphics.color(Color.LIGHT_BLUE)
			        graphics.alpha(1 - frame / 45)
			        graphics.circle(handler.x, handler.y, frame * 3 + 12, true)
			        graphics.alpha(1)
			    end
			end
			
			-- Later in our code...
			local handler = graphics.bindDepth(-9, drawEffect)
			handler.x = x
			handler.y = y
		
		Binds the function ``drawEffect`` to be called each frame at depth -9, just above players.
		
		The function ``drawEffect`` itself draws a ring which grows over time and then is destroyed after 45 frames.
		
		| This example uses the fact that the object controlling function calls is an instance to contain a state and destroy the instance when the effect is done.
		| The number of frames the instance has been around is also passed to the function, and is used here to control the timing of instance destruction and the rate of circle growth.

``graphics.setChannels(red, green, blue, alpha)``
-------------------------------------------------

	| Can be used to disable drawing to specific color channels.
	| Each parameter is a boolean indicating whether that channel should be drawn to.

	**Parameters:**
	
		===========  ========  ===========
		Parameter    Type      Description
		===========  ========  ===========
		``red``      boolean   Whether to draw to the red channel.
		``green``    boolean   Whether to draw to the green channel.
		``blue``     boolean   Whether to draw to the blue channel.
		``alpha``    boolean   Whether to draw to the alpha channel.
		===========  ========  ===========
		
``graphics.resetChannels(red, green, blue, alpha)``
---------------------------------------------------

	| Enables drawing to all color channels again.
	| Effectively the same as calling ``graphics.setChannels`` with all parameters set to true.
	| This should always be called after using ``graphics.setChannels`` as the active color channels are not automatically reset.

************
Input Module
************

The ``input`` module is used to check and manage player input.

.. contents:: Table of Contents
   :local:

.. _keypressstatus:

Press Statuses
==============

Input checking functions all return one of the press status constants depending on the state of the checked input.

These numbers are consistent across all functions and are as follows:
 
==============    ======
Number            Status
==============    ======
input.NEUTRAL     Not pressed
input.PRESSED     Just pressed
input.HELD        Being held
input.RELEASED    Just released
==============    ======
   
Functions (Controls)
====================

The module provides functionality for checking both bound controls and for raw input, the following section concerns specifically the handling of bound controls.

.. _input-check-control:	

``input.checkControl(control, [player])``
-----------------------------------------
	
	Used to check the press status of one of the player's inputs.
	
	Functionally the same as :ref:`PlayerInstance:control <player-instance-control>`.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``control``     string              The control to check. Case insensitive.
		``player``      PlayerInstance      *Optional:* The player whose controls to check. Defaults to player 1.
		===========     ==============      ===========
		
		.. _playerinputnames:

		The following is a list of all control names:
		
		========    ========    ========
		left        right       up
		down        jump        ability1
		ability2    ability3    ability4
		use         enter       swap
		========    ========    ========

	**Returns:**
	
		Returns the press status of the control. (:ref:`See above <keypressstatus>`)
	
	**Example:**
		::
			
			if input.checkControl("down", player) == input.HELD then
			    superjumpcharge = superjumpcharge + 1
			end
			
		Checks whenever ``player`` is holding the down key and increases a counter continuously.
   
``input.getControlString(control, [player])``
---------------------------------------------
	
	Gets a string representation of one of the player's inputs.

	.. note:: Strings of gamepad controls use special characters and will only display correctly on the ``graphics.FONT_DEFAULT`` and ``graphics.FONT_LARGE`` fonts.

	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``control``     string              The control to get a string of. Case insensitive.
		``player``      PlayerInstance      *Optional:* Used to determine gamepad type. Defaults to player 1.
		===========     ==============      ===========
		
		:ref:`See above <playerinputnames>` for a list of controls.	

	**Returns:**
	
		Returns the string representation of the control.
	

Functions (Keyboard)
====================

``input.checkKeyboard(key)``
-------------------------------
	
	Gets the status of a keyboard key.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``key``         string      String representation of the keyboard key to check for.
		===========     ======      ===========
		
		| Button names should be mostly intuitive, for example ``"a"`` meaning the A button, ``"6"`` meaning the number 6 key.
		| Other keys are represented as a single string without spaces or punctuation, for example ``"numpad4"`` and ``"pageup"``.
		| Also included is the ``"anykey"`` key, which can be used to check if any keyboard key is pressed.

	**Returns:**
	
		Returns the press status of the keynumber. (:ref:`See above <keypressstatus>`)
	
	**Example:**
		::
			
			if input.checkKeyboard("numpad5") == input.PRESSED then
			    eggplant_item:create(player.x, player.y)
			end
			
		Creates an instance of the item ``eggplant_item`` on the ``player`` when the 5 key on the numpad is pressed; Useful for testing.

Functions (Gamepad)
===================

``input.getPlayerGamepad([player])``
------------------------------------
	
	Gets the ID of a player's currently active gamepad.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``player``      PlayerInstance      *Optional:* The player to get the controller ID of. Defaults to player 1.
		===========     ==============      ===========

	**Returns:**
	
		Returns the controller index as a number, or nil if the keyboard is in use. 
	
	**Example:**
		::
			
			local gamepad = input.getPlayerGamepad(player)
			
		Gets the gamepad index of ``player``.

``input.checkGamepad(button, gamepad)``
---------------------------------------
	
	Gets the status of a button on a gamepad.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``button``      string      String representation of the keyboard key to check for.
		``gamepad``     number      The ID of the controller to check button presses on.
		===========     ======      ===========
		
		The following buttons can be checked for:
		
		==========    ===========
		Button        Description
		==========    ===========
		face1         Face button 1 (A or Cross)
		face2         Face button 2, (B or Circle)
		face3         Face button 3 (X or Square)
		face4         Face button 4 (Y or Triangle)
		shoulderl     Left Shoulder Button (Left Bumper or L1)
		shoulderlb    Left Shoulder Trigger (Left Trigger or L2)
		shoulderr     Right Shoulder Button (Right Bumper or R1)
		shoulderrb    Right Shoulder Trigger (Right Trigger or R2)
		select        The select button (Touch-Pad press on a PS4 controller)
		start         The start button ("Options" button on a PS4 controller)
		stickl        The left stick pressed (as a button)
		stickr        The right stick pressed (as a button)
		padu          Up on the D-pad
		padd          Down on the D-pad
		padl          Left on the D-pad
		padr          Right on the D-pad
		==========    ===========

	**Returns:**
	
		Returns the press status of the button. (:ref:`See above <keypressstatus>`)
	
	**Example:**
		::
			
			if input.checkGamepad("stickl", gamepad) then
			    -- Do Something
			end
			
		Does something when the left stick is pressed on the controller with the ID ``gamepad``.
		
``input.getGamepadAxis(axis, gamepad)``
---------------------------------------
	
	Gets the status of a gamepad control stick axis.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``axis``        string      String representation of the axis to check.
		``gamepad``     number      The ID of the controller to check axis movement on.
		===========     ======      ===========
		
		The following axes can be checked for:
		
		====    ===========
		Axis    Description
		====    ===========
		lh      Left stick horizontal
		lv      Left stick vertical
		rh      Right stick horizontal
		rv      Right stick vertical
		====    ===========

	**Returns:**
	
		Returns a number between -1 and 1, depending on how far the stick is tilted in the checked direction.
	
	**Example:**
		::
			
			if input.getGamepadAxis("lv", gamepad) < -0.2 then 
			    -- Do something
			end
			
		Does something when the left stick on ``gamepad`` is tilted upwards at least 20%.
		
Functions (Mouse)
=================
		
``input.getMousePos([screen])``
-------------------------------
	
	Gets the position of the mouse either on screen or in the game world.
	
	**Parameters:**
	
		===========     =======      ===========
		Parameter       Type         Description
		===========     =======      ===========
		``screen``      boolean      | *Optional:* If set to true, returns the position of the mouse in the game window.
		                             | If false, returns the position of the mouse in the world. Defaults to false.
		===========     =======      ===========

	**Returns:**
	
		Returns two numbers; the horizontal position of the mouse and the vertical position.
	
	**Example:**
		::
			
			local x, y = input.getMousePos()
			eggplant_sprite:draw(x, y)
			
		Draws the sprite ``eggplant_sprite`` at the mouse's game world coordinates.
		
``input.checkMouse(button)``
-------------------------------
	
	Gets the status of a mouse button.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``button``      string      String representation of the mouse button to check for; can be "left", "right", "middle", or "none".
		===========     ======      ===========

	**Returns:**
	
		Returns the press status of the button. (:ref:`See above <keypressstatus>`)
	
	**Example:**
		::
			
			if input.checkMouse("middle") == input.PRESSED then
				eggplant_item:create(input.getMousePos())
			end
			
		Spawns the item ``eggplant_item`` at the cursor position when the middle mouse button is pressed.
		
``input.getMouseScroll()``
-------------------------------

	**Returns:**
	
		Returns -1 when the mouse wheel was scrolled up and 1 when the mouse wheel was scrolled down. Returns 0 if the mouse wheel wasn't moved.

	**Example:**
		::
			
			scroll = scroll + input.getMouseScroll()
			eggplant_sprite:draw(player.x, player.y + scroll)
		
		Draws the sprite ``eggplant_sprite`` at a position relative to ``player`` which can be controlled by moving the mouse wheel up and down.
		
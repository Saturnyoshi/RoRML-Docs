*********
Callbacks
*********

Callbacks are your main method of getting the game to call your code.

.. contents:: Table of Contents
   :local:
   
Functions
=========

.. _register-callback:

.. warning:: Callbacks exist forever (even across runs) no matter when they are defined. If you think you need to define a callback after load, then you're probably doing something wrong.

``callback.register(callback, bind, [priority])``
-------------------------------------------------

	Alias: ``callback(callback, bind, [priority])``

	Adds a function to be called whenever the specified callback is fired.
	
	**Parameters:**
	
		============    ========    ===========
		Parameter       Type        Description
		============    ========    ===========
		``callback``    string      The name of the callback to add a function to.
		``bind``        function    The function to add to the callback. The arguments fed to this function will depend on what callback is being fired, as detailed :ref:`below <list-of-callbacks>`.
		``priority``    integer     *Optional:* Defaults to 10. A priority can be set to decide in which order callbacks will be run. A higher priority means the function is called earlier. This value can be negative.
		============    ========    ===========
		
	**Example:**
		::
			
			local function foo(player)
			    -- Do something
			end
			
			callback.register("onPlayerStep", foo)
		
		::
			
			callback.register("onPlayerStep", function(player)
			    -- Do something
			end)
		
		These two examples are functionally identical ways of assigning a function to a callback. In this case we're *doing something* on the player step callback.

		::
			
			callback.register("onNPCDeath", function(npc)
			    -- Do something
			end, 200)

		Here is a very similar example, except here we're registering to the NPC death callback, and registering at a priority of 200. The higher priority means this callback will be called before most others.

		::
			
			callback("onStageEntry", function(npc)
			    -- Do something
			end)

		This is a final simple example, except here we use the shortcut to ``callback.register`` rather than typing out the whole thing.




``callback.create(name)``
-------------------------

	Creates a new callback that all mods can register to.
	
	If multiple mods try to create the same callback an error will be thrown, so be sure to use unique names.
	
	**Parameters:**
	
		============    ========    ===========
		Parameter       Type        Description
		============    ========    ===========
		``name``        string      The name that will be used to add to this callback with ``callback.register``.
		============    ========    ===========
		
	**Returns:**
	
		Returns a new function used to invoke the callback.
		
		Arguments passed to this function will also be passed to all called functions.
		
	**Example:**
		::
			
			-- Create a callback
			local customCallback = callback.create("exampleCustomCallback")
			
			-- Add a function to it
			local function myFunc(number)
			    print("The number is: " .. tostring(number))
			end
			callback.register("exampleCustomCallback", myFunc)
			
			-- Call it
			customCallback(math.random(100))
		
		The above example would create a new callback, add a function to it, and call it with a random number.
		
.. _list-of-callbacks:
	
Supported Callbacks
===================

General Callbacks
-----------------

	These are callbacks related to the running of the game. None of these come with any parameters.

	+-------------------------+----------------------------------------------------------------------------------+
	|Name                     |Description                                                                       |
	+=========================+==================================================================================+
	|``"onStep"``             |Fired every game tick.                                                            |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"preStep"``            |Fired at the start of every game tick.                                            |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"postStep"``           |Fired at the end of every game tick.                                              |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"onDraw"``             |Fired after most vanilla rendering. Custom drawing should be done here.           |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"onHUDDraw"``          |Fired after most vanilla HUD rendering. Drawing coordinates are screen based.     |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"preHUDDraw"``         |Fired before most vanilla HUD rendering. Drawing coordinates are screen based.    |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"onLoad"``             |Fired immediately after all mods are initialized.                                 |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"postLoad"``           |Fired immediately after ``onLoad``.                                               |
	+-------------------------+----------------------------------------------------------------------------------+
	|``"onCameraUpdate"``     |Fired immediately after the base game's camera movement.                          |
	+-------------------------+----------------------------------------------------------------------------------+

Game Callbacks
--------------

	These are callbacks related to game events. None of these come with any parameters

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onStageEntry"``       |                                                                  |Fired at the start of every stage.                                                |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onMinute"``           |``minute:number``, ``second:number``                              |Fired whenever the timer minute changes.                                          |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onSecond"``           |``minute:number``, ``second:number``                              |Fired whenever the timer second changes.                                          |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onGameStart"``        |                                                                  |Fired at the very start of the game.                                              |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onGameEnd"``          |                                                                  |Fired at the end of the game.                                                     |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	
Actor Callbacks
---------------

	These are callbacks related to any actor instance.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onActorInit"``        |``actor:ActorInstance``                                           |Called whenever a new actor spawns.                                               |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

Player Callbacks
----------------

	These are callbacks related to the player and are fired for each player object in the game.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onPlayerInit"``       |``player:PlayerInstance``                                         |Fired at the start of the run for every player.                                   |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerStep"``       |``player:PlayerInstance``                                         |Fired each tick for every player.                                                 |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerDraw"``       |``player:PlayerInstance``                                         |Fired after drawing the player's sprite.                                          |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerDrawBelow"``  |``player:PlayerInstance``                                         |Fired when beginning to draw the player, before any part of them is drawn.        |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerDrawAbove"``  |``player:PlayerInstance``                                         |Fired after drawing all parts of the player.                                      |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerHUDDraw"``    |``player:PlayerInstance, x:number, y:number``                     |The x and y coordinates are the position of the player's first skill on screen.   |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerLevelUp"``    |``player:PlayerInstance``                                         |Fired whenever the player levels up.                                              |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onPlayerDeath"``      |``player:PlayerInstance``                                         |Fired whenever a player dies.                                                     |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

Damager Callbacks
-----------------

	These are callbacks related to damagers, i.e. bullets and explosions.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onFire"``             |``damager:DamagerInstance``                                       |Called each time a bullet or explosion is fired.                                  |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onFireSetProcs"``     |``damager:DamagerInstance, parent:ActorInstance``                 |Called when setting item procs when a bullet or explosion is fired.               |
	|                         |                                                                  |Not called when the damager is set not to proc.                                   |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onHit"``              |``damager:DamagerInstance, hit:ActorInstance, x:number, y:number``|Fired whenever a damager hits an actor, including the precise coordinates hit at. |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"preHit"``             |``damager:DamagerInstance, hit:ActorInstance``                    |Fired whenever a damager hits an actor, before dealing damage or most item procs. |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"postHit"``            |``damager:DamagerInstance``                                       |Fired after hitting an actor but only once per damager.                           |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onImpact"``           |``damager:DamagerInstance, x:number, y:number``                   |Fired when any damager hits an actor and when bullet damagers hit a wall.         |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

NPC Callbacks
-------------

	These are callbacks related to NPCs and bosses.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onNPCDeath"``         |``npc:ActorInstance``                                             |Fired whenever an NPC dies.                                                       |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onNPCDeathProc"``     |``npc:ActorInstance, player:PlayerInstance``                      |Fired for each player whenever an NPC dies. Handle death item procs here.         |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

MapObject Callbacks
-------------------

	These are callbacks related to map objects.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onMapObjectActivate"``|``mapObject:Instance, activator:PlayerInstance``                  |Fired whenever a map object is activated by a player.                             |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

Item Callbacks
--------------

	These are callbacks related to item objects.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"onItemRoll"``         |``pool:ItemPool, item:Item``                                      |Fired when rolling an item from a pool. The rolled item can be overwritten by     |
	|                         |                                                                  |returning an item object within the callback.                                     |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onItemInit"``         |``item:Instance``                                                 |Fired whenever an instance of an item is spawned.                                 |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onItemPickup"``       |``item:Instance, player:PlayerInstance``                          |Fired whenever an item gets picked up.                                            |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"onUseItemUse"``       |``player:PlayerInstance, item:Item``                              |Fired whenever a player uses a use item.                                          |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"postUseItemUse"``     |``player:PlayerInstance, item:Item``                              |Fired after ``onUseItemUse`` and vanilla use item logic.                          |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

Global Callbacks
----------------

	.. warning:: The API was not really designed to be used outside of runs. Running code outside of the game holds a much higher risk of errors.

	These are callbacks that can be executed both in and outside of gameplay, such as during menus or cutscenes.
	
	Each callback receives the current :doc:`Room </class/room>` as parameter.

	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|Name                     |Parameters                                                        |Description                                                                       |
	+=========================+==================================================================+==================================================================================+
	|``"globalStep"``         |``room:Room``                                                     |Global equivalent to the ``"onStep"`` callback.                                   | 
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"globalPreStep"``      |``room:Room``                                                     |Global equivalent to the ``"preStep"`` callback.                                  |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"globalPostStep"``     |``room:Room``                                                     |Global equivalent to the ``"postStep"`` callback.                                 |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"globalRoomStart"``    |``room:Room``                                                     |Called any time a new room is loaded.                                             |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+
	|``"globalRoomEnd"``      |``room:Room``                                                     |Called when the current room is being unloaded.                                   |
	+-------------------------+------------------------------------------------------------------+----------------------------------------------------------------------------------+

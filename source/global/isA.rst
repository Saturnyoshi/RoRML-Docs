***
IsA
***

The ``isa`` function is used to check the type of values.

``isa(value, type)``
--------------------
	
	Checks whether any value is an instance of any class or any of its descendants.

	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``value``       any         The value to check the type of.
		``type``        string      The type to check for.
		===========     ======      ===========
	
	**Returns:**
	
		A boolean. True if the type of ``value`` is ``type`` or any descendant of ``type``.

	**Examples:**

		::
			
			print(isa("Hello!", "string"))
			print(isa("Hello!", "number"))
		
		| Checks the type of a string value.
		| The first print statement will display true, as the value is a string; The second will display false as it is not a number.

		::
			
			local player = misc.players[1]
			print("Player is a PlayerInstance?", isa(player, "PlayerInstance"))
			print("Player is a ActorInstance?", isa(player, "ActorInstance"))
			print("Player is an Instance?", isa(player, "Instance"))
			print("Player is a GMObject?", isa(player, "GMObject"))
		
		Checks whether the object ``player`` inherits from several different classes.

		| The first 3 print statements will display true, as the type of ``player`` is ``PlayerInstance``, which inherits from both ``ActorInstance`` and ``Instance``.
		| The final print statement will display false as the ``GMObject`` class has no relation to ``PlayerInstance``.

		
******
Export
******

The ``export`` function is used to expose functions and values defined in one mod to any other mod with it added as a dependency.

``export(name, [value])``
-------------------------
	
	Inserts the provided value into the environment of all dependent mods.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The key the value will be assigned to. Also supports table paths (see examples).
		``value``       any         *Optional:* The value to be exported. Defaults to ``_G[name] or {}`` when nil; 
		===========     ======      ===========
	
	**Examples:**
		
		There are a multitude of ways the export function can be used.
		
		::
			
			Fruits = {"banana", "apple", "pear"}
			export("Fruits")
			
		::
			
			export("Fruits", {"banana", "apple", "pear"})
			
		The above code snippets both have the same effect; a table of strings is created and added to all relevant mod environments under the key ``Fruits``. 
		
		Creating custom libraries is simple with this function:
		
		::
			
			export("MyLibrary")
			MyLibrary.happyPrint = function(str)
			    print(str .. " :)")
			end
		
		You can also utilize this function to modify or expand built-in modules:
		
		::
			
			function math.add(n1, n2)
			    return n1 + n2
			end
			export("math.add")
			
		This would create a new ``add`` function in the ``math`` library.
		
		::
			
			function math.random()
			    return 1
			end
			export("math.random")
			
		This would overwrite the ``math.random`` function with a replacement which always returns 1.
		
		When using table paths, all missing steps will automatically be created:
		
		::
			
			export("very.long.string.of.nested.tables.number", 1)
			
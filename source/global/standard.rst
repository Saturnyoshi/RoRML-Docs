**************************
Standard Library Additions
**************************

In addition to most of the Lua standard library functions, RoRML contains some useful functions not found in the standard library.

.. contents:: Table of Contents
   :local:

Table Functions
===============

``table.random(t)``
-------------------

	Used when you want to get a random element from a table with non-numerical keys.

	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``t``           table               The table to get a random element from.
		===========     ==============      ===========

	**Returns:**
	
		A random value from the table.

``table.irandom(t)``
--------------------

	Used when you want to get a random element from a table with strictly numerical keys.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``t``           table               The table to get a random element from.
		===========     ==============      ===========
		
	**Returns:**
	
		A random value from the table.
		
Math Functions
==============

``math.chance(percent)``
------------------------
	
	| Returns a random boolean. The chance that the function will return true is specified by the user.
	| Useful for simple random item or visual effects.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``percent``     number              The chance that the function will return true, from 0 to 100.
		===========     ==============      ===========
		
	**Returns:**
	
		Returns true the specified percent of times and false every other time.
		
``math.approach(value, target, [step])``
----------------------------------------
	
	| Moves the parameter ``value`` towards the target by the amount specified by the parameter ``step``.
	| The returned value never overshoots the target and the absolute value of ``step`` is used. 
	| Works with both positive and negative values for all parameters.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``value``       number              The starting value.
		``target``      number              The value to move towards
		``step``        number              *Optional:* The amount to move the ``value`` towards ``target`` by. Defaults to 1.
		===========     ==============      ===========
		
	**Returns:**
	
		Returns the parameter ``value`` moved towards the target by the value ``step``.
		
	**Examples:**
		::
			
			timer = math.approach(timer, 0)
			if timer == 0 then
			    -- Do something
			end
			
		Makes the variable ``timer`` approach 0 by 1 each time the code is ran and does something when it reaches 0.

		::
			
			exp = math.approach(exp, max_exp, bonus)
			
		Moves the value of ``exp`` towards the value of ``max_exp`` by the value of ``bonus``, but never overshoots.		
		
``math.sign(n)``
----------------
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``n``           number              The value to get the sign of.
		===========     ==============      ===========
		
	**Returns:**
	
		Returns 1 if ``n`` is positive, -1 if ``n`` is negative, and 0 if ``n`` equals 0.
				
		
``math.round(n)``
-----------------
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``n``           number              The value to round.
		===========     ==============      ===========
		
	**Returns:**
	
		Returns the value rounded to the nearest whole value, whether it's up or down.
		
``math.clamp(value, lower, upper)``
-----------------------------------

	Used to make a number stay within a certain range.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``value``       number              The value to clamp.
		``lower``       number              The lower bounds of the clamp.
		``upper``       number              The upper bounds of the clamp.
		===========     ==============      ===========
		
	**Returns:**
		
		| If ``value`` is within ``upper`` and ``lower`` then ``value`` is returned.
		| If ``value`` is greater than ``upper`` then it is returned.
		| If ``value`` is less than ``lower`` then it is returned.
	
		
``math.lerp(value, lower, upper)``
-----------------------------------

	Used to calculate the value between 2 values by a specific percent.
	
	**Parameters:**
	
		===========     ==============      ===========
		Parameter       Type                Description
		===========     ==============      ===========
		``from``        number              The value to lerp from.
		``to``          number              The value to lerp to.
		``amount``      number              The amount to lerp by.
		===========     ==============      ===========
		
	**Returns:**
		
		``from`` interpolated to ``to`` by ``amount``.

	**Example:**
		::

			print(math.lerp(10, 30, 0.5)

		Prints 20.
		
Misc
====

``log(...)``
------------

	The same as ``print`` but the output is written to the error log as well.

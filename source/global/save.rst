***********
Save Module
***********

The ``save`` module provides a simple way of writing custom values to the save file.

.. contents:: Table of Contents
   :local:

Functions
=========

``save.write(key, value)``
-------------------------------
	
	Writes a value to the user's save file.

	This is limited in scope to the current mod and will not overwrite values saved by other mods.

	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``key``         string                             The name of the save key to write. This must be a string.
		``value``       number, string, boolean, or nil    The value that will be written to the save. This can be any of the supported value types.
		============    ===============================    ===========
		
	**Example:**
		::
			
			local runCount = save.read("runCount")
			if runCount == nil then
			    runCount = 0
			end

			callback.register("onGameStart", function()
			    runCount = runCount + 1
			    save.write("runCount", runCount)
			end)
			
		This example keeps track of the total number of runs the player has had with the mod active.

``save.read(key)``
-------------------------------
	
	Reads a value from the user's save file.

	This is limited in scope to the current mod and will not return values saved by other mods.

	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``key``         string                             The name of the save key to read.
		============    ===============================    ===========
		
	**Returns:**
	
		Returns the value written to the save file.
		
		If no value exists with the key, nil is returned.
	
``save.writeMod(mod, key, value)``
--------------------------------------
	
	Writes a value to the user's save file.

	This is used to write to the save data of other mods. The mod's internal name is used to identify it.

	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``mod``         string                             The internal name of the mod to write to. Case insensitive.
		``key``         string                             The name of the save key to write. This must be a string.
		``value``       number, string, boolean, or nil    The value that will be written to the save. This can be any of the supported value types.
		============    ===============================    ===========

``save.readMod(mod, key)``
-------------------------------
	
	Reads a value from the user's save file.

	This is used to read from the save data of other mods. The mod's internal name is used to identify it.

	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``mod``         string                             The internal name of the mod to read from. Case insensitive.
		``key``         string                             The name of the save key to read.
		============    ===============================    ===========
		
	**Returns:**
	
		Returns the value written to the save file.
		
		If no value exists with the key, nil is returned.
	
``save.getKeys([mod])``
-------------------------------
	
	Returns a list of all of the keys in a mod's save data.

	**Parameters:**
	
		============    ===============================    ===========
		Parameter       Type                               Description
		============    ===============================    ===========
		``mod``         string                             *Optional:* The internal name of the mod to read keys from. Case insensitive. If omitted, the current mod is used.
		============    ===============================    ===========
		
	**Returns:**
	
		Returns a numerically indexed table of all the keys.

	**Example:**
		::
			
			local keys = save.getKeys()
			for _, v in ipairs(keys) do
			    print(v, save.read(v))
			end
			
		Prints all save keys of the current mod, alongside their values.
	
``save.getMods()``
-------------------------------
	
	Returns a list of all mods with save data in lowercase.

	**Returns:**
	
		Returns a numerically indexed table of lowercase mod internal names.

	**Example:**
		::
			
			local mods = save.getMods()
			local data = {}
			for _, m in ipairs(mods) do
			    data[m] = {}
			    local keys = save.getKeys(m)
			    for _, v in ipairs(keys) do
			        data[m][v] = save.readMod(m, v)
			    end
			end
			print(data)
			
		Reads the entire mod save data to a table and then prints it to the console.
	
	
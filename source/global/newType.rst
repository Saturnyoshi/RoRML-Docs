*******
NewType
*******

The ``newtype`` function is used to create new datatypes and classes.

``newtype(name)``
-----------------
	
	Used to create a type or class.
	
	The current namespace followed by a colon will be prepended to the type name to prevent naming collisions.
	
	**Parameters:**
	
		===========     ======      ===========
		Parameter       Type        Description
		===========     ======      ===========
		``name``        string      The name of the type, such as ``"number"``, ``"vector"``, or ``"item"``.
		===========     ======      ===========
	
	**Returns:**
	
		===============     ========      ===========
		Return              Type          Description
		===============     ========      ===========
		``constructor``     function      A function which returns a new object of the type.
		``metatable``       table         The metatable used by all objects of the type.
		===============     ========      ===========
	
	**Examples:**
		
		::
			
			local MyClass, classMT = newtype("MyClass")
			
		Sets up a new type called ``MyClass`` and stores the type constructor and metatable in the ``MyClass`` and ``classMT`` variables respectively.
		
		::
			
			local obj = MyClass()
			print(type(obj))
			
		An example of a custom defined type interacting with built-in functions.
		Creates a new object and then prints its type to the console.
		
		::
			
			classMT.__index = {}
			function classMT.__index:greet()
			    print("Howdy!")
			end
			
			obj:greet()
			
		This example uses standard Lua metatable features to add a method named ``greet`` to our custom type, and then calls it on our previously created object.
		
		::
			
			local obj_name = {}
			    
			function classMT:__init(name)
			   obj_name[self] = name
			end
		    
			function classMT.__index:getName()
			    return obj_name[self]
			end
			
			local obj2 = MyClass("Apple")
			print(obj2:getName())
			
		An example of using the special ``__init`` metamethod.
		
**********
Net Module
**********

The ``net`` module is used when writing online co-op compatible code.

.. contents:: Table of Contents
   :local:

Fields
======

``net.online``
-----------------------------------------
	
	Boolean indicating whether the game is currently in online co-op.
	

``net.host``
-----------------------------------------
	
	Boolean indicating whether the user is the host of the game when ``net.online`` is true.
	If ``net.online`` is false, this value is always true.

``net.localPlayer``
-----------------------------------------

	The :doc:`PlayerInstance </class/playerInstance>` of the local player.

``net.Packet``
-----------------------------------------

	The packet class, used to transfer information between clients.
	
	See the page on the :doc:`net.Packet </class/net.packet>` class for info.

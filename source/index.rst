.. Risk of Rain ModLoader documentation master file, created by
   sphinx-quickstart on Thu Feb 22 16:06:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

####################################
Risk of Rain ModLoader Documentation
####################################
*The documentation is currently up to date to version 0.4.2*

| This is the documentation for RoRML, the unofficial update for Risk of Rain which primarily focuses on adding mod support.
| As this is a relatively early version of the modding API, several important features are still missing. 
| As the mod continues to be updated, the mod support will be improved to allow for more of the game to be modded.

If you need a hand, have a feature suggestion, or have a bug report then visit our `Discord server <https://discord.gg/ajsGTdN>`_.

Completed mods can be uploaded to and downloaded from `Rainfusion <https://rainfusion.ml>`_.

API Documentation
=================

.. toctree::
	:maxdepth: 1
	:glob:
	:caption: Global Functions:
	
	global/registerCallback
	global/export
	global/newType
	global/isA
	global/*

.. toctree::
	:maxdepth: 1
	:glob:
	:caption: Classes:
	
	class/*

.. toctree::
	:maxdepth: 1
	:glob:
	:caption: Misc:
	
	misc/*
	
Tutorials
=========

.. toctree::
    :maxdepth: 1
    :glob:
    :caption: Tutorials:
    
    tutorials/*
	